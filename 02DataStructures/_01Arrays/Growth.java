package _01Arrays;

import java.util.ArrayList;

public class Growth {

	public static void main(String[] args) {
		ArrayList<Integer>numbers=new ArrayList<>(); //Don't need anything inside <> when creating the object, if it is being assigned to a variable which has already specified the types
		ArrayList<Integer>numbers2=new ArrayList<>(); //Don't need anything inside <> when creating the object, if it is being assigned to a variable which has already specified the types
		//myInstance.myMethod(new ArrayList<>()); would not compile
		System.out.println("measuring minimum recordable time");
		long before,after;
		for(int i=0;i<100;++i){
			before=System.nanoTime();
			after=System.nanoTime();
			System.out.println("i is "+i+" time is "+((after-before)/1000.)+"�s");
		} //Quiz: what is happening at i is 58?
		//Window>Preferences>Search"Console">Run/Debug Console>Console buffer size(characters)/800000
		System.out.println("measuring .add() times");
		for(int j=0;j<10000;++j){
			before=System.nanoTime();
			numbers.add(j);
			after=System.nanoTime();
			System.out.println("j is "+j+" time is "+((after-before)/1000.)+"�s");
		}
		System.out.println("measuring .add() times - condensed");
		for(int k=0;k<58000000;++k){
			before=System.nanoTime();
			numbers2.add(k);
			after=System.nanoTime();
			double delta=(after-before)/1000000.;
			if(delta>10){
				System.out.println("k is "+k+" time is "+delta+"ms");
			}
		}		
	}
}