package _01Arrays;

import java.util.ArrayList;
import java.util.stream.Stream;

public class Array {
	public static void main(String[]args){
		//DEBUG ME

		//HOMOGENEOUS FIXED WIDTH ARRAYS
			int[]ia=new int[5]; //look at initial values
			short[]shortsEatMy=new short[5];
			byte[]b=new byte[3];
			System.out.println(ia.getClass().getName()); //[ means array I means int
			System.out.println(shortsEatMy.getClass().getName());
			System.out.println(b.getClass().getName());
			System.out.println(shortsEatMy.length);
			ia[0]=42; //see the value change
			ia[1]=123456;
			
			shortsEatMy[0]=42;
			shortsEatMy[1]=1234;
			//show that can change value of i[1]  -> click and add 1
			//for h have to (short)123 to change
			System.out.println(ia);
			for(int i:ia){
				System.out.println(i+' ');
			}
			for(int i=0;i<ia.length;++i){
				System.out.print(ia[i]+" "+shortsEatMy[i]+"|");
			}
			//change me, save me, and choose the restart option
		
		//HOMOGENEOUS VARIABLE WIDTH ARRAYS
			String[]s={"David","Hodgins","MThree"};
			//show 0 1 2 of array, and then the 3 values, with 0-4, 0-6 and 0-5
		//HETEROGENEOUS ARRAYS
			Object[]a={42,4.2,"Dave"};
			//show 0,1,2 3 types 2->value, 0->autoboxing
		//VARIABLE LENGTH FIXED WIDTH ARRAY
			ArrayList<Integer>al=new ArrayList<Integer>();
			for(int counter=0;counter<=10;++counter){
				al.add(42-counter);
				//watch it populate elementData
				//when we get to counter=10 watch it grow
			}
			al.remove(10);
		//MULTI-DIMENSIONAL FIXED WIDTHS ARRAY
			int[][]flightPath=new int[5][3]; //5 points of height,latitude,longitude
			flightPath[0][0]=0;
			flightPath[0][1]=51;
			flightPath[0][2]=0;

			flightPath[1][0]=100;
			flightPath[1][1]=51;
			flightPath[1][2]=0;

			flightPath[2][0]=10000;
			flightPath[2][1]=52;
			flightPath[2][2]=1;

			flightPath[3][0]=20000;
			flightPath[3][1]=53;
			flightPath[3][2]=3;

			flightPath[4][0]=40000;
			flightPath[4][1]=56;
			flightPath[4][2]=7;
	}
}
