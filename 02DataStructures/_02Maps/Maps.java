package _02Maps;

import java.util.HashMap;
import java.util.Map;

public class Maps {

	public static void main(String[] args) {
		//don't want to explain interfaces yet
		HashMap<String,String>shakeColours=new HashMap<String,String>();
		shakeColours.put("Banana", "Yellow");
		//shakeColours/table/ whichever one is populated
		shakeColours.put("Chocolate", "Brown");
		//hopefully the values are not adjacent
		shakeColours.put("Vanilla", "White");
		shakeColours.put("Skittles", "Rainbow");
		
		System.out.println("We serve the following milkshake flavours:");
		//loop over keys
		for(String key:shakeColours.keySet()){
			System.out.println("\t"+key);
		}
		System.out.println("We serve the following milkshake colours:");
		//loop over values
		for(String value:shakeColours.values()){
			System.out.println("\t"+value);
		}
		//loop over both
		for(Map.Entry<String, String> entry:shakeColours.entrySet()){
			System.out.println(entry.getKey()+" milkshakes are "+entry.getValue());
		}
		
		shakeColours.remove("Skittles");
		shakeColours.put("Chocolate", "Black");
		//hash for Chocolate is -1602936215 and is entry 9
		for(int i=0;i<6;i++){
			shakeColours.put(i+"", ""+i);
		}
		//Quiz
		shakeColours.put(6+"", ""+6); //Talk about collisions, demo shakeColours.table[6].next
		for(int i=7;i<9;i++){
		    shakeColours.put(i+"", ""+i);
		}
		//this will cause it to grow and rehash
		shakeColours.put("Oreo", "Light Brown");
		
		
		HashMap<Integer,String>clients=new HashMap<>(); //Java 7 
		clients.put(0, "bob"); 
		//avoid the Java 8 examples as they require understanding of interfaces and are no longer relevant in Java 10
		var clients2=new HashMap<>(); //Java 10
		clients2.put(0,"bob");		  //Java 10
		clients2.put("bob",2);	//More dynamic than expected
		String k=(String)clients2.get(0); //Regression to Java 4, even if we comment out 54
	}
}
