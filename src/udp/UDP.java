package udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class UDP{

	public static void main(String[] args){
		FeedHandler f=new FeedHandler();
		f.start();
//		while(!f.bound) {
//			try{
//				Thread.sleep(1);
//			}catch(InterruptedException e1){
//				e1.printStackTrace();
//			}
//		}
		Exchange e=new Exchange();
		e.start();

		FeedHandler2 f2=new FeedHandler2();
		Exchange2 e2=new Exchange2();
		f2.start();
		e2.start();
}

}

class Exchange extends Thread{
	public void run() {
		try {
			String msg="Here is a trade message.";
			DatagramChannel channel = DatagramChannel.open().connect(new InetSocketAddress("localhost",4000));
			ByteBuffer buf = ByteBuffer.allocate(42*42);
			channel.write(buf.put(msg.getBytes()).flip());
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}

class FeedHandler extends Thread{
	boolean bound;
	public void run() {
		try {
			DatagramChannel channel=DatagramChannel.open();
			channel.socket().bind(new InetSocketAddress(4000));
			bound=true;
			ByteBuffer buf = ByteBuffer.allocate(42*42);
			SocketAddress originAddress = channel.receive(buf);
			System.out.println(originAddress);
			System.out.println(new String(buf.array()));
		}catch(IOException e) {
			e.printStackTrace();
		}
	}	
}

class Exchange2 extends Thread{
	public void run() {
		try {
			String msg="Here is a trade message.";
			
			DatagramSocket socket=new DatagramSocket(4445);
			
			byte[] buf = new byte[256];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			socket.receive(packet);
			String received=new String(packet.getData(), 0, packet.getLength());
			System.out.println(received);
			InetAddress address = packet.getAddress();
         int port=packet.getPort();
         buf=msg.getBytes();
         
         packet=new DatagramPacket(buf, buf.length, address, port);
         socket.send(packet);

         socket.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}

class FeedHandler2 extends Thread{
	boolean bound;
	public void run() {
		try {
			DatagramSocket socket=new DatagramSocket();
			byte[] buf="message from FH".getBytes();
			InetAddress address = InetAddress.getByName("localhost");
			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 4445);
			socket.send(packet);
			packet = new DatagramPacket(buf, buf.length);
			socket.receive(packet);
			String received = new String(packet.getData(), 0, packet.getLength());
			System.out.println(received);
			socket.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}	
}