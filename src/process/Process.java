package process;

import java.io.IOException;
import java.io.InputStreamReader;

public class Process {
	public static void main(String[] args) throws InterruptedException, IOException {
		System.out.println("Test output");
		// Get runtime
		java.lang.Runtime rt = java.lang.Runtime.getRuntime();
		// String[] cmd={"cmd","/c","java","-version"};
		// String[] cmd={"cmd","/c","dir bin\\process"};
		String[] cmd = { "java", "-cp", "bin", "process.Yo" };
		java.lang.Process p = rt.exec(cmd);
		// Get process' output: its InputStream
		java.io.InputStream os = p.getInputStream();
		java.io.InputStream es = p.getErrorStream();
		java.io.BufferedReader stdout = new java.io.BufferedReader(new InputStreamReader(os));
		java.io.BufferedReader stderr = new java.io.BufferedReader(new InputStreamReader(es));
		// And print each line
		String out, err = null;

		// And print each line
		do {
			out = stdout.readLine();
			if (out != null)
				System.out.println("out " + out);
			
			err = stderr.readLine();
			if (err != null)
				System.out.println("err " + err);
			
			Thread.sleep(100);
		} while (p.isAlive() || out != null || err != null);
		os.close();
		es.close();
	}
}
