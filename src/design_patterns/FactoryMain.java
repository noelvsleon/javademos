package design_patterns;

public class FactoryMain {

    public static void main(String[] args) {
	Object o=new Object();
	//comment/uncomment
	//Factory f=new Factory();
	
	Factory f2=Factory.createObject();
	f2.setA(1);
	f2.setW(2);
	f2.setX(3);
    }

}

class Factory {
    private int x;
    private int y;
    private int z;
    private int w;
    private int a;
    private Factory(){
    }
    static Factory createObject() {
	return new Factory();
    }
    public int getX() {
	return x;
    }
    public void setX(int x) {
	this.x = x;
    }
    public int getY() {
	return y;
    }
    public void setY(int y) {
	this.y = y;
    }
    public int getZ() {
	return z;
    }
    public void setZ(int z) {
	this.z = z;
    }
    public int getW() {
	return w;
    }
    public void setW(int w) {
	this.w = w;
    }
    public int getA() {
	return a;
    }
    public void setA(int a) {
	this.a = a;
    }
}
