package design_patterns;

public class SingletonMain{

	public static void main(String[] args){
		assert(Singleton.getInstance()==Singleton.getInstance());
	}

}

class Singleton{
	private static Singleton instance;
	private Singleton() {
	}

	public static Singleton getInstance() {
		synchronized(instance){
			if(instance==null) {
				instance=new Singleton();
			}
		}
		return instance;
	}
}