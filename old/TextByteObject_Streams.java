package io;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
public class TextByteObject_Streams {
	public static void main(String[] args) {
		Server2 s=new Server2(3000);
		s.start();
		s.setName("server");
		Client4 c=new Client4();
		c.start();
		c.setName("client");
	}
}

class Client4 extends Thread{
	@SuppressWarnings("resource")
	@Override
	public void run(){
		p("client connecting");
		try {
			p("connecting to text");
			Socket text=new Socket("localhost",3000);
			p("connecting to bytes");
			Socket bytes=new Socket("localhost",3001);
			p("connecting to objects");
			Socket objects=new Socket("localhost",3002);

			p("send text");
			PrintWriter textout=new PrintWriter(text.getOutputStream(),true);
			textout.println("i am client");
			
			p("send bytes");
			DataOutputStream bytesout=new DataOutputStream(bytes.getOutputStream());
			for(int i=0;i<256;i++){
				bytesout.write(i);
			}
			p("close bytes");
			bytesout.close();
			
			p("send object");
			ObjectOutputStream objectsout=new ObjectOutputStream(objects.getOutputStream());
			objectsout.writeObject(new Fruit("apple"));
			objectsout.writeObject(new Fruit("orange"));
			
			p("read text");
 			BufferedReader textin=new BufferedReader(new InputStreamReader(text.getInputStream()));
			p("server sent:"+textin.readLine());

			p("read object");
			ObjectInputStream objectsin=new ObjectInputStream(objects.getInputStream());
			p("Dog is "+((Dog)objectsin.readObject()).size());
			p("Dog is "+((Dog)objectsin.readObject()).size());
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		p("done");
	}
	private static void p(String message){
		System.out.println("CLIENT: "+message);
	}
}

class Server2 extends Thread{
	private int port;
	private static void p(String message){
		System.out.println("SERVER: "+message);
	}
	Server2(int port){
		this.port=port;
	}
	@SuppressWarnings("resource")
	@Override
	public void run(){
		p("server listening");
		try {
			//Socket conn=ServerSocketFactory.getDefault().createServerSocket(port).accept();
			Socket text=new ServerSocket(port).accept();
			p("client connected to text");
			Socket bytes=new ServerSocket(port+1).accept();
			p("client connected to bytes");
			Socket objects=new ServerSocket(port+2).accept();
			p("client connected to objects");

			p("read text");
			BufferedReader in=new BufferedReader(new InputStreamReader(text.getInputStream()));
			p("client sent:"+in.readLine());
			
			p("read bytes");
			DataInputStream bytesin=new DataInputStream(bytes.getInputStream());
			int current;
			int i=0;
			while(-1!=(current=bytesin.read())){
				System.out.print(current+" is "+(char)current+"\t");
				if(i==10){
					i=0;
					System.out.println();
				}
				i++;
			}
			p("received end of byte stream, got -1");
			
			p("read object");
			ObjectInputStream objectsin=new ObjectInputStream(objects.getInputStream());
			p(((Fruit)objectsin.readObject()).toString());
			p(((Fruit)objectsin.readObject()).toString());

			p("send text");
			PrintWriter textout=new PrintWriter(text.getOutputStream(),true);
			textout.println("i am server");
			
			p("try to send bytes");
			DataOutputStream bytesout=new DataOutputStream(bytes.getOutputStream());
			try{
				bytesout.writeBytes("The meaning of life, the universe and everything is:");
				bytesout.write(42);
				bytesout.writeChar('\n');
			}catch(SocketException e){
				p("got error, as expected as should be closed");
				e.printStackTrace();
			}
			
			p("send object");
			ObjectOutputStream objectsout=new ObjectOutputStream(objects.getOutputStream());
			objectsout.writeObject(new Dog("German"));
			objectsout.writeObject(new Dog("Yorkshire"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
class Fruit implements Serializable{
	private String type;
	private boolean hasStem;
	Fruit(String type){
		this.type=type;
		if(type=="apple"){
			hasStem=true;
		}
	}
	public String toString(){
		String result=super.toString()+"\n"
				+"\tClass:"+this.getClass().getName()+"\n"
				+"\tParent:"+this.getClass().getSuperclass().getName()+"\n"
				+"\tFields\n"
				+"\t\tString type=\""+type+"\"\n"
				+"\t\tboolean hasStem="+hasStem+"\n\tReflection:\n";
		for(Field f:this.getClass().getDeclaredFields()){
			result+="\t"+f.getType().getName()+" "+f.getName()+"\n";
		}
		return result;
	}
}
class Dog implements Serializable{
	private String breed;
	Dog(String breed){
		this.breed=breed;
	}
	public String size(){
		if(breed=="German"){
			return "big";
		}else if(breed.equals("Alsation")){
			return "Large";
		}else{
			return "small";
		}
	}	
}
