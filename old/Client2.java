package io;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import _60Logging.M3Log;
public class Client2 {
	public static void main(String[] args) {
		//We can't introduce threads until we have done networking, we can't fully explain networking until we have done threads.
		//So use separate processes manually for initial networking
		M3Log.lg.info("Connect to server on port 3000");
		Socket text = null;
		while(text==null){
			try {
				text=new Socket("localhost",3000);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		M3Log.lg.info("our port is "+text.getLocalPort());
		
		try {
			PrintWriter textout=new PrintWriter(text.getOutputStream(),true);
			String msg="client says hello";
			M3Log.lg.info("send a message to the server:"+msg);
			textout.println(msg);

			BufferedReader textin=new BufferedReader(new InputStreamReader(text.getInputStream()));
			M3Log.lg.info("got message from server:"+textin.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
