package io;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import _60Logging.M3Log;
public class Client3 {
	public static void main(String[] args) {
		//We can't introduce threads until we have done networking, we can't fully explain networking until we have done threads.
		//So use separate processes manually for initial networking
		M3Log.lg.info("Connect to server on port 3000");
		Socket text = new Socket();
		SocketAddress a=new InetSocketAddress("localhost",3000);
		int timeout[]={1000, 2000, 5000, 10000}; //exponential backoff
		int timeoutIndex=0;
		int counter=0;
		int maxRetries=10;
		while(counter<maxRetries && !text.isConnected()){
			try {
				text.connect(a, timeout[timeoutIndex]);;
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(timeoutIndex<3)timeoutIndex++;
			counter++;
		}
		if(!text.isConnected()){
			M3Log.lg.info("could not connect after "+maxRetries+". Oh dear!! Exiting.");
			System.exit(1);
		}
		M3Log.lg.info("our port is "+text.getLocalPort());

		PrintWriter textout;
		try {
			textout=new PrintWriter(text.getOutputStream(),true);
			String msg="i am client";
			M3Log.lg.info("send a message to the server:"+msg);
			textout.println(msg);

			BufferedReader textin=new BufferedReader(new InputStreamReader(text.getInputStream()));
			M3Log.lg.info("got message from server:"+textin.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
