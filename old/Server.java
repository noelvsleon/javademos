package io;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import _60Logging.M3Log;
public class Server {
	public static void main(String[] args) {
		try {
			M3Log.lg.info("Listen on port 3000 for a client to connect");
			Socket text;
			text = new ServerSocket(3000).accept();
			M3Log.lg.info("Client port is:"+text.getPort());
			BufferedReader in=new BufferedReader(new InputStreamReader(text.getInputStream()));
			M3Log.lg.info("Message from Client:"+in.readLine());
			
			PrintWriter textout=new PrintWriter(text.getOutputStream(),true);
			M3Log.lg.info("Send a message to client");
			textout.println("server says goodbye");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}