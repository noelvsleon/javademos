package _14ThreadPools;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
//(FH,FH,FH)->Buffer->(Repeater,Repeater,Repeater)
//Want to separate the FH from the Repeater because capturing the data is more important than disseminating it
//TODO Do we need a connection handler?
public class Pool {
	public static void main(String[]args){
		//DEBUG ME. There is a deliberate bug that forms part of an exercise. the bug is that the last messages don't get recieved by the clients.
		FH fh1=new FH(FH.Exchanges.BATE,Ports.exchangeBATE);
		FH fh2=new FH(FH.Exchanges.LSE,Ports.exchangeLSE);
		FH fh3=new FH(FH.Exchanges.XETR,Ports.exchangeXETR);

		//Pretend this is a 4 core machine, so have 3 threds for FH, and a pool of 1 for the Repeaters 
		fh1.start();
		fh2.start();
		fh3.start();
		
		MarketDataRepeater3 mdr1=new MarketDataRepeater3(MarketDataRepeater3.Exchanges.BATE,Ports.MDRBATE,fh1);
		MarketDataRepeater3 mdr2=new MarketDataRepeater3(MarketDataRepeater3.Exchanges.LSE,Ports.MDRBATE,fh2);
		MarketDataRepeater3 mdr3=new MarketDataRepeater3(MarketDataRepeater3.Exchanges.XETR,Ports.MDRBATE,fh3);

		ExecutorService pool=Executors.newFixedThreadPool(1);
		pool.execute(mdr1);
		pool.execute(mdr2);
		pool.execute(mdr3);
		
		test();
		//Let main thread die
		//TODO have the main thread monitor the state of the other threads and log load metrics
	}

	static void test(){
		//use a pool so it doesn't take over my machine
		ExecutorService pool=Executors.newFixedThreadPool(1);
		pool.execute(new testExchange(testExchange.Exchanges.BATE,Ports.exchangeBATE));
		pool.execute(new testExchange(testExchange.Exchanges.LSE,Ports.exchangeLSE));
		pool.execute(new testExchange(testExchange.Exchanges.XETR,Ports.exchangeXETR));

		pool.execute(new testClient(Ports.MDRBATE));
		pool.execute(new testClient(Ports.MDRLSE));
		/* TODO have 1 client listen to 2 MD ports*/
	}
}

class Ports{
	static final int exchangeLSE=3000;
	static final int exchangeBATE=3001;
	static final int exchangeXETR=3002;
	static final int MDRLSE=3003;
	static final int MDRBATE=3004;
	static final int MDRXETR=3005;
}

class testExchange extends Thread{
	enum Exchanges{LSE,BATE,XETR};
	Exchanges exchange;
	int port;
	testExchange(Exchanges exchange,int port){
		this.exchange=exchange;
		this.port=port;
	}
	public void run(){
		try {
			//Make sure have talked about method returning an object, and then chaining with methods that re-return it.
			DatagramChannel channel = DatagramChannel.open().connect(new InetSocketAddress("localhost",port));
			ByteBuffer buf = ByteBuffer.allocate(42*42);

			int msgId=0;
			String msg=msgId+++","+exchange+",VOD,194.25,1000,B";
			System.out.println("Exchange sending "+msg);
			
			channel.write(buf.put(msg.getBytes()).flip());
			
			//TODO doesn't work
			//SocketAddress originAddress=channel.receive((ByteBuffer)buf.clear());
			//buf.flip();
			//System.out.println("testExchange "+exchange+" Got "+buf.limit()+" bytes from "+originAddress+new String(buf.array()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
class FH extends Thread{
	enum Exchanges{LSE,BATE,XETR};
	Exchanges exchange;
	int port;
	boolean haveMsgFlag=false;
	String msg;
	FH(Exchanges exchange,int port){
		this.exchange=exchange;
		this.port=port;
	}
	public void run(){
		try {
			DatagramChannel channel=DatagramChannel.open();
			//TODO why can't we use connect here? gives an exception or just hangs.
			channel.socket().bind(new InetSocketAddress(port));
			ByteBuffer buf = ByteBuffer.allocate(42*42);

			SocketAddress originAddress = channel.receive(buf);
			buf.flip();
			msg=new String(Arrays.copyOfRange(buf.array(),buf.position(),buf.limit()));
			haveMsgFlag=true;
			System.out.println("FH "+exchange+" Got "+buf.limit()+" bytes from "+originAddress+" "+msg);
			
			while(true){
				sleep(1000);
			}
			//TODO this doesn't work, probably for the same reason we can't call connect, which is probably because we are trying to send/recv on the same UDP on the same machine, and it doesn't like it for some reason.
			//byte[]reply=("Ta vury much "+msg).getBytes();
			//channel.send(((ByteBuffer)buf.clear()).put(reply),new InetSocketAddress("localhost",port));
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class MarketDataRepeater3 extends Thread{
	enum Exchanges{LSE,BATE,XETR};
	Exchanges exchange;
	int port;
	FH fh;
	MarketDataRepeater3(Exchanges exchange,int port,FH fh){
		this.exchange=exchange;
		this.port=port;
		this.fh=fh;
	}
	public void run(){
		try {
			DatagramChannel channel = DatagramChannel.open().connect(new InetSocketAddress("localhost",port)); //not a real connection, just a shorthand so that don't have to specify the address for every message
        		ByteBuffer buf = ByteBuffer.allocate(42*42);
        		while(!fh.haveMsgFlag){
        			sleep(200);
        		}
        		System.out.println("MDR forwarding message "+fh.msg);
			channel.write((ByteBuffer)buf.put(fh.msg.getBytes()).flip());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
class testClient extends Thread{
	int port;
	testClient(int port){
		this.port=port;
	}
	public void run(){
		try {
			DatagramChannel channel = DatagramChannel.open();
			channel.socket().bind(new InetSocketAddress(port));
			ByteBuffer buf = ByteBuffer.allocate(42*42);
			channel.receive(buf);
			buf.flip();
			System.out.println("Client "+getName()+" got message "+new String(buf.array()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

/*
LEAVE BELOW AS TODO UNTIL END/ANOTHER COURSE

processes
linux shell

syncrhonise cache flush example

wait()
notify()

lock starvation while(true) synchronized(lock){System.out.println(Thread.currentThread().getName()); //do 2 threads

		
deadlock
livelock
daemon threads

semaphore

Exchanger
Buffer
Futures
ReentrantLock

setPriority(4) default value is that of the parent, what is the default value of main, in eclipse?
apparently in Linux setPriority has no effect

*/
