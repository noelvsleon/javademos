package _09Exchanger;

import java.util.ArrayList;
import java.util.concurrent.Exchanger;
import _01Logging.M3Log;

public class Exchangers{
	public static void main(String[] args){
		//DEBUGME
		Exchanger<ArrayList<Trade>>exchanger=new Exchanger<>();
		new Thread(new Reader(exchanger)).start();
		new Thread(new Processor(exchanger)).start();
	}
}
class Trade{
	int id;
	Trade(int id){
		this.id=id;
	}
}

abstract class Parent implements Runnable{ //abstract classes can implement interfaces without doing so, leaving it to their children to do this
	Exchanger<ArrayList<Trade>> exchanger;
	ArrayList<Trade>trades=new ArrayList<>();
	public Parent(Exchanger<ArrayList<Trade>> exchanger){
		this.exchanger=exchanger;
	}
}
class Reader extends Parent{
	public Reader(Exchanger<ArrayList<Trade>> exchanger){
		super(exchanger);
	}
	@Override public void run(){
		Thread.currentThread().setName("Reader");
		int id=0;
		M3Log.lg.info("Generate first batch");
		while(true) {
			//Pretend we get the trade data from a file or network connection
			//...
			for(int i=0;i<10;++i) {
				trades.add(new Trade(++id));
			}
			try{
				M3Log.lg.info("Reader exchanging");
				trades=exchanger.exchange(trades);
				M3Log.lg.info("Reader exchanged");
			}catch(InterruptedException e){
			}
		}
	}
}
class Processor extends Parent{
	public Processor(Exchanger<ArrayList<Trade>> exchanger){
		super(exchanger);
	}
	@Override public void run(){
		Thread.currentThread().setName("Processor");
		while(true) {
			try{
				M3Log.lg.info("Processor exchanging");
				trades=exchanger.exchange(trades);
				M3Log.lg.info("Processor exchanged");
			}catch(InterruptedException e){
			}
			//Pretend we do something more exciting with them
			for(Trade t:trades) {
				System.out.println(t.id);
			}
			trades.clear();
		}
	}
}
