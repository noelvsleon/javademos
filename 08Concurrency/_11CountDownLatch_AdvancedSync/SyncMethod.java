package _11CountDownLatch_AdvancedSync;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
public class SyncMethod {
	static Order o1=new Order(10_000,"VOD");
	static Order o2=new Order(10_000,"BT");
	static Order o3=new Order(10_000,"HSBA");
	static CyclicBarrier cb=new CyclicBarrier(4);
	public static void main(String[] args) throws InterruptedException {
		Thread t =new OrderRouter();
		Thread t2=new OrderRouter();
		Thread t3=new OrderRouter();
		Thread t4=new OrderRouter();

		t.start();
		Thread.sleep(3000, 123_456);
		t2.start();
		t3.start();
		t4.start();
		//Run a few times and see the different outputs
		//Will see, 0/-1, 0/-1/-2, 10000
	}
}

class Order{
	/*bank balance
	sync changeBalance
	notsync getBalance
	sync calcOverdraftCharge
	
	change this to IB
	*/
	
	/*Order and fill 
	problem with threads trying to fill an order on different exchanges
	might end up short selling if order is to sell
	both threads will have to check the remaining shares and then send a slice to market
	explain  that in a real system, they would have to reverse this position within a fixed time to comply with exchange rules
	talk about compliance reports that look at the DB T+1 and check how long each short was for, eg any shor that lasted longer than X seconds, in a liquid market break the exchange rules and exchange can "fine" or revoke membership
	
	talk about not wanted to solve this problem with synchronisation due to the performance penalty/lack of scalability
	also, often this is a problem across machines/DataCenters, rather than across threads, where the sync penalty is even worse
	*/
	int OrderQty;
	int LeavesQty;
	int ReservedQty=0;
	String instrument;
	Order(int OrderQty,String instrument){
		this.OrderQty=OrderQty;
		this.LeavesQty=OrderQty;
		this.instrument=instrument;
	}
	//dangerous way
	int Fill(int fillSize){
		return LeavesQty-=fillSize;
	}
	//safe way
	//need another sync method otherwise this sync method pointless
	synchronized int SafeFill(int fillSize){
		return LeavesQty-=fillSize;
	}
	synchronized int ReservedFill(int fillSize){
		ReservedQty-=fillSize;
		return LeavesQty-=fillSize;
	}
	synchronized int GetLeavesQty(){
		return LeavesQty;
	}
	synchronized boolean ReserveFill(int qty){
		if((LeavesQty-ReservedQty)>=qty){
			ReservedQty+=qty;
			return true;
		} //TODO what is the name of this 'no else' pattern
		return false;
	}
	//need a non sync method for my example
	//TODO getters are lame childish syntax when no sync is needed, see c# for how it should be done
	String getInstrument(){
		return instrument;
	}
}

class OrderRouter extends Thread{
	@Override public void run(){
		//both threads wait to actually run until both are here
		try {
			System.out.println("Thread "+getId()+" Current :"+(System.currentTimeMillis()/1000)+" Waiting");
			SyncMethod.cb.await();
			System.out.println("Thread "+getId()+" Current :"+(System.currentTimeMillis()/1000));
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
		Order o1=SyncMethod.o1;
		Order o2=SyncMethod.o2;
		Order o3=SyncMethod.o3;
		//use cyclic barrier here//want to encourage badness, so sync
		try {
			SyncMethod.cb.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
		while(o1.LeavesQty>0){
			o1.Fill(1); //Here we don't even attempt to be safe
		}
		System.out.println("Final LeavesQty o1 "+o1.LeavesQty);

		try {
			SyncMethod.cb.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
		while(o2.GetLeavesQty()>0){
			o2.SafeFill(1); //Here we try to be safe, but have a race condition
		}
		System.out.println("Final LeavesQty o2 "+o2.GetLeavesQty());

		try {
			SyncMethod.cb.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
		while(o3.ReserveFill(1)){
			//If we use Fill() we could corrupt the value
			//Highlight the 1 letter method name difference, to avoid confusion
			//Pretend we go and get the shares over the network, and that getting them is not guaranteed
			o3.ReservedFill(1); //Use SafeFill to avoid that.
		}
		System.out.println("Final LeavesQty o3 "+o3.GetLeavesQty());
	}
}

