package _02Symmetrical;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SymmetricalAddition{
	public static List<Integer>numbers=new ArrayList<>();
	public static final int n=10_000_000;
	public static int[]answers;
	static {
		Random rand=new Random();
		for(int i=0;i<n;++i) {
			numbers.add(rand.nextInt(100));
		}
	}
	public static void main(String[] args){
		final int numSummers=4;
		answers=new int[numSummers];
		Thread[] summers=new Thread[numSummers];
		final int window=n/numSummers;
		for(int i=0;i<numSummers;++i) {
			summers[i]=new Thread(new SumR(i,i*window,(i+1)*window));
			summers[i].start();
		}
		for(int i=0;i<summers.length;++i) {
			try{
				summers[i].join();
			}catch(InterruptedException e){ } //not possible
		}
		int total=0;
		for(int subtotal:answers) {
			total+=subtotal;
		}
		System.out.println("Total "+total);
	}
}

class SumR implements Runnable{
	int id,start,end;
	SumR(int id,int start,int end){
		this.id=id;
		this.start=start;
		this.end=end;
	}
	@Override public void run() {
		for(int i=start;i<end;++i) {
			SymmetricalAddition.answers[id]+=SymmetricalAddition.numbers.get(i);
		}
	}
}
