package _89Concurrency_misc;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Interrupt{

	public static void main(String[] args){
		System.exit(1); //not finished
		System.out.println("main");
		Thread main=Thread.currentThread();
		Thread a=new A(main);
		Thread b=new B(main);
		a.start();
		b.start();
//		LinkedBlockingQueue a=new LinkedBlockingQueue<E>(1024);
		for(int i=0;i<2;++i) {
			try{
				synchronized(main) {
					main.wait();
				}
			}catch(InterruptedException e){
//				main.
				e.printStackTrace();
			}
		}
		
	}

}

class A extends Thread{
	Thread main;
	A(Thread main){
		this.main=main;
	}
	public void run() {
		System.out.println("A");
		main.interrupt();
		try{
			wait();
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
}
class B extends Thread{
	Thread main;
	B(Thread main){
		this.main=main;
	}
	public void run() {
		System.out.println("B");
		main.interrupt();
		try{
			wait();
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
}