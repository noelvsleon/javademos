package _03Synchronization;

import java.util.ArrayList;
import java.util.List;

public class SynchronizedMethod{
	public static List<Integer>numbers=new ArrayList<>();
	public static final int n=8;
	static {
		for(int i=0;i<n;++i) {
			numbers.add(i);
		}
	}
	public static void main(String[] args){ //DEBUGME and show locks
		final int numSummers=4;
		Thread[] summers=new Thread[numSummers];
		final int window=n/numSummers;
		PlaceToStoreAnswers answers=new PlaceToStoreAnswers();
		for(int i=0;i<numSummers;++i) {
			summers[i]=new Thread(new SumSM(i*window,(i+1)*window,answers));
			summers[i].start();
		}
		for(int i=0;i<summers.length;++i) {
			try{
				summers[i].join();
			}catch(InterruptedException e){ } //not possible
		}
		System.out.println("Total "+answers.grandTotal());
	}
}

class SumSM implements Runnable{
	int start,end;
	PlaceToStoreAnswers answers;
	SumSM(int start,int end,PlaceToStoreAnswers data){
		this.start=start;
		this.end=end;
		this.answers=data;
	}
	@Override
	public void run() {
		int subtotal=0;
		for(int i=start;i<end;++i) {
			subtotal+=SynchronizedMethod.numbers.get(i);
		}
		answers.saveAnswer(subtotal);
	}
}

class PlaceToStoreAnswers{
	private List<Integer>answers=new ArrayList<Integer>();
	
	synchronized void saveAnswer(int subtotal) {
		answers.add(subtotal);
	}
	public int grandTotal() {
		int total=0;
		for(int subtotal:answers) {
			total+=subtotal;
		}
		return total;
	}
}
