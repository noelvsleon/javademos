package _03Synchronization;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SynchronizedObject{
	public static List<Integer>numbers=new ArrayList<>();
	public static final int n=10_000_000;
	public static List<Integer>answers=new ArrayList<Integer>();
	static {
		Random rand=new Random();
		for(int i=0;i<n;++i) {
			numbers.add(rand.nextInt(100));
		}
	}
	public static void main(String[] args){ //DEBUGME and show locks
		final int numSummers=4;
		Thread[] summers=new Thread[numSummers];
		final int window=n/numSummers;
		for(int i=0;i<numSummers;++i) {
			summers[i]=new Thread(new SumSO(i*window,(i+1)*window));
			summers[i].start();
		}
		for(int i=0;i<summers.length;++i) {
			try{
				summers[i].join();
			}catch(InterruptedException e){ } //not possible
		}
		int total=0;
		for(int subtotal:answers) {
			total+=subtotal;
		}
		System.out.println("Total "+total);
	}

}

class SumSO implements Runnable{
	int start,end;
	SumSO(int start,int end){
		this.start=start;
		this.end=end;
	}
	@Override public void run() {
		int subtotal=0;
		for(int i=start;i<end;++i) {
			subtotal+=SynchronizedObject.numbers.get(i);
		}
		synchronized(SynchronizedObject.answers) {
			SynchronizedObject.answers.add(subtotal);
		}
	}
}
