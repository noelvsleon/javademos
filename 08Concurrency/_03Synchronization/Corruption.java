package _03Synchronization;

public class Corruption{
	static int x=0;
	public static void main(String[] args) throws InterruptedException{
		Thread thread1=new MyThread();
		Thread thread2=new MyThread();
		thread1.start();
		thread2.start();
		thread1.join();
		thread2.join();
		System.out.println("0+(2*100000)="+x);
		System.out.println("We lost at least "+(200_000-x)+" writes "+(100*(200_000-x)/200_000)+"%");
	}

}

class MyThread extends Thread{
	@Override public void run() {
		for(int i=0;i<100_000;++i) {
			++Corruption.x;
		}
	}
}