package _03Synchronization;

class ii{
	static int staticI=0;
	static int j=0;
	//TODO learn about Lock object
	static Object lock=new Object();
}

public class MultipleCorruption {
	public static void main(String[] args) throws InterruptedException {
		Integer i=0;
		Thread[]exchanges={
				//more hoops when implements Runnable, instead of extends Thread
				new Thread(new MarketDataRepeater2(MarketDataRepeater2.Exchanges.BATE,i)),
				new Thread(new MarketDataRepeater2(MarketDataRepeater2.Exchanges.LSE,i)),
				new Thread(new MarketDataRepeater2(MarketDataRepeater2.Exchanges.XETR,i))};
		for(Thread exchange:exchanges)
			exchange.start();
		for(Thread exchange:exchanges){
			System.out.println("waiting for thread to finish:"+exchange.getName());
			exchange.join();
		}
		//There is a chance that it will run fine, but run it again, about 9/10 chance it will corrupt as expected
	}
}

class MarketDataRepeater2 implements Runnable{ //implement Runnable instead of extend Thread as this is best practice
	enum Exchanges{LSE,BATE,XETR};
	Exchanges exchange;
	Integer immutableI;
	MarketDataRepeater2(Exchanges exchange,Integer i){
		this.exchange=exchange;
		this.immutableI=i;
	}
	public void run(){
		String threadName=Thread.currentThread().getName(); //Need to use Thread. when implements Runnable, no necessary when extends Thread.
		System.out.println("Own Stacks");
		System.out.println("I am "+this.getClass().getName()+" class for exchange "+exchange);
		for(int localI=0;localI<10_000;++localI)
			++this.immutableI;
		System.out.println("Shared Heap - but Integer is immutable");
		System.out.println("Final value of immutableI is "+this.immutableI+" for thread "+threadName);
		for(int i=0;i<10_000;++i)
			++ii.staticI;
		System.out.println("Cooperation"); //Interspersed logging statements
		System.out.println("Final value of ii.staticI is "+ii.staticI+" for thread "+threadName);
		synchronized(ii.lock){
			for(int i=0;i<10_000;++i)
				++ii.j;
			System.out.println("Final value of ii.j is "+ii.j+" for thread "+threadName);			
		}
	}
}