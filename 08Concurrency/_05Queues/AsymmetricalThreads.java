package _05Queues;

import java.util.concurrent.LinkedBlockingQueue;
import _01Logging.M3Log;

public class AsymmetricalThreads{
	public static void main(String[] args){
		StreamProducer s=new StreamProducer();
		(new Thread(s)).start();
		(new Thread(new TradeConsumer(s.trades))).start();
		(new Thread(new QuoteConsumer(s.quotes))).start();
	}
}

class StreamProducer implements Runnable{
	LinkedBlockingQueue<Transaction> trades=new LinkedBlockingQueue<>(10);
	LinkedBlockingQueue<Transaction> quotes=new LinkedBlockingQueue<>(10);
	@Override public void run(){
		int id=-1;
		while(true){
			Trade t=new Trade(++id);
			sendMessage(trades,t);
			Quote q=new Quote(++id);
			sendMessage(quotes,q);
		}
	}
	void sendMessage(LinkedBlockingQueue<Transaction> queue,Transaction t){
		try{
			Thread.sleep(1000); //DEMO with and without this sleep
			while(!queue.offer(t)){
				Thread.sleep(100); // TODO maybe then add wait notify here
			}
		}catch(InterruptedException e){
		}
	}
}

abstract class Consumer_ implements Runnable{
	LinkedBlockingQueue<Transaction> queue;
	Consumer_(LinkedBlockingQueue<Transaction> q){
		queue=q;
	}
	@Override public void run(){
		String className=this.getClass().getSimpleName();
		Thread.currentThread().setName(className+"Thread");
		while(true){
			Transaction t;
			try{
				t=queue.take(); // blocks
				M3Log.lg.info(className+" id:"+t.id);
				process(t);
			}catch(InterruptedException e){
			}
		}
	}
	public abstract void process(Transaction tr);
}

class TradeConsumer extends Consumer_{
	TradeConsumer(LinkedBlockingQueue<Transaction> q){
		super(q);
	}
	@Override public void process(Transaction tr){
		Trade t=(Trade)tr;
		System.out.println(t.id);
	}
}

class QuoteConsumer extends Consumer_{
	QuoteConsumer(LinkedBlockingQueue<Transaction> q){
		super(q);
	}
	@Override public void process(Transaction tr){
		Quote q=(Quote)tr;
		System.out.println(q.id);
	}
}