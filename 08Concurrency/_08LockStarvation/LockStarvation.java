package _08LockStarvation;

public class LockStarvation{
	public static void main(String[] args){
		//Quiz: what will get printed, what will it look like?
		Data d;
		new Thread(new MyThread(d=new Data())).start();
		new Thread(new MyThread(d)).start();
	}
}
class Data{
	int i=0;
}
class MyThread implements Runnable{
	Data d;
	MyThread(Data d){
		this.d=d;
	}
	@Override public void run() {
		String name=Thread.currentThread().getName();
		int i=0;
		while(true) {
			synchronized(d) {
				System.out.println((name.equals("Thread-1")?"\t":"")+"i="+ (++i)+" d.i="+ ++d.i);
				if(d.i>100)return;
			}
		}
	}
}