package _08LockStarvation;

public class Yield{
	public static void main(String[] args){
		//Quiz: what will get printed, what will it look like?
		Data2 d;
		new Thread(new MyThread2(d=new Data2())).start();
		new Thread(new MyThread2(d)).start();
	}
}
class Data2{
	int i=0;
}
class MyThread2 implements Runnable{
	Data2 d;
	MyThread2(Data2 d){
		this.d=d;
	}
	@Override
	public void run() {
		String name=Thread.currentThread().getName();
		int i=0;
		while(true) {
			synchronized(d) {
				System.out.println((name.equals("Thread-1")?"\t":"")+"i="+ (++i)+" d.i="+ ++d.i);
				if(d.i>100)return;
			}
			Thread.yield();
		}
	}
}