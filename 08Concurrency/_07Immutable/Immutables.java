package _07Immutable;

public class Immutables{
	public static void main(String[] args){
		//DEBUGME
		Integer i1=Integer.valueOf(42);
		Integer i2=i1;
		i1++;
		Integer i3=Integer.sum(i1,3); //can't use +, have to use sum. See also divideUnsigned
		
		String s1="dave";
		String s2=s1;
		s1+="hodgins";
	}
}
