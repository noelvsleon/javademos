package _13Futures;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class Futures{
	public static void main(String[] args){
		FutureTask<Integer>answer1=new FutureTask<>(new MyClass());
		FutureTask<Integer>answer2=new FutureTask<>(new MyClass());
		Executors.newSingleThreadExecutor().submit(answer1);
		Executors.newSingleThreadExecutor().submit(answer2);
		
		for(int i=0;i<3;++i) {
			System.out.println("something");
		}
		try{
			int result=answer1.get()+answer2.get();
			System.out.println(result);
		}catch(InterruptedException e){
		}catch(ExecutionException e){
			e.printStackTrace();
		}
	}
}
class MyClass implements Callable<Integer>{
	public Integer call() {
		return 42;
	}
}