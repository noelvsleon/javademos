package _01implementVSextends;
public class Main {
	public static void main(String[] args) {
		Thread a=new MyThread();
		a.start();
		
		Runnable b=new MyRunnable();
		Thread c=new Thread(b);
		c.start();

		try{
			a.join();
			c.join();
		}catch(InterruptedException e){
		}
	}
}

class MyThread extends Thread{
	@Override public void run() {
		System.out.println("hello");
		try{
			sleep(3000);
		}catch(InterruptedException e){
		}
		System.out.println("there");
	}
}
class MyRunnable implements Runnable{
	@Override public void run() {
		System.out.println("goodbye");
		try{
			Thread.sleep(3000);
		}catch(InterruptedException e){
		}
		System.out.println("my friend");
	}
}