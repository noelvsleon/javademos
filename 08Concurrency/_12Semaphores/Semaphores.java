package _12Semaphores;

import java.util.concurrent.Semaphore;
import _01Logging.M3Log;

public class Semaphores{

	public static void main(String[] args){
		Semaphore s=new Semaphore(2,true); //true gives FIFO to requests
		new Thread(new MockDiskWriter(s)).start();
		new Thread(new MockDiskWriter(s)).start();
		new Thread(new MockDiskWriter(s)).start();
	}
}

class MockDiskWriter implements Runnable{
	private Semaphore s;
	MockDiskWriter(Semaphore s){
		this.s=s;
	}
	@Override public void run() {
		M3Log.lg.info("Acquiring semaphore");
		try{
			s.acquire();
			M3Log.lg.info("Acquired");
			Thread.sleep(5000);
		}catch(InterruptedException e){
		}
		M3Log.lg.info("Releasing");
		s.release();
	}
}
