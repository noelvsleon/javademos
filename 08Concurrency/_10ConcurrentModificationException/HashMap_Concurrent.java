package _10ConcurrentModificationException;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HashMap_Concurrent{

	public static void main(String[] args){
		//DEBUGME
		Map<String,Integer>m=new HashMap<>();
		m.put("a",1);
		m.put("b",2);
		m.put("c",3);
		m.put("d",4);
		printAndRemoveC(m);
		
		Map<String,Integer>cm=new ConcurrentHashMap<>();
		cm.put("a",1);
		cm.put("b",2);
		cm.put("c",3);
		cm.put("d",4);
		printAndRemoveC(cm);
	}
	
	static void printAndRemoveC(Map<String,Integer>hm) {
		System.out.println("\n"+hm.getClass().getName());
		try{
			for(var entry:hm.entrySet()) {
				if(entry.getKey().equals("c")) {
					System.out.println("Removing c");
					hm.remove("c");
				}
				System.out.println(entry.getKey()+" "+entry.getValue());
			}
		}catch(ConcurrentModificationException e) {
			e.printStackTrace();
		}
	}
}
