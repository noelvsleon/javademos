package _06Deadlock;

import _01Logging.M3Log;

public class Deadlock{
	public static void main(String[] args){
		new Thread(new A()).start();
		new Thread(new B()).start();
	}
	static Object resource1=new Object();
	static Object resource2=new Object();
}

// need 2 locks, and two different methods that acquire the locks in reverse
// order
// socket, order

class A implements Runnable{
	@Override public void run(){
		while(true){
			M3Log.lg.info("acquiring resource1");
			synchronized(Deadlock.resource1){
				M3Log.lg.info("acquiring resource2");
				synchronized(Deadlock.resource2){
					M3Log.lg.info("acquired both\n");
				}
			}
		}
	}
}

class B implements Runnable{
	@Override public void run(){
		while(true){
			M3Log.lg.info("acquiring resource2");
			synchronized(Deadlock.resource2){
				M3Log.lg.info("acquiring resource1");
				synchronized(Deadlock.resource1){
					M3Log.lg.info("acquired both\n");
				}
			}
		}
	}
}
