package _04Asymmetrical;

public class AsymmetricalThreads{
	public static void main(String[] args){
		StreamHandler s=new StreamHandler();
		new Thread(s).start();
		TradeHandler t=new TradeHandler(s);
		new Thread(t).start();
		QuoteHandler q=new QuoteHandler(s);
		new Thread(q).start();
	}
}

class Transaction{
	int id;
	Transaction(int id){
		this.id=id;
	}
	@Override public String toString() {
		return this.getClass().getSimpleName()+"="+id;
	}
}
class Trade extends Transaction{
	Trade(int id){
		super(id);
	}
}
class Quote extends Transaction{
	Quote(int id){
		super(id);
	}
}
class StreamHandler implements Runnable{
	Transaction nextMessage=new Trade(-1); //CHEAT!! 
	@Override public void run() {
		int id=-1;
		while(true) {
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){}
			synchronized(nextMessage) {
				nextMessage=new Trade(++id);
			}
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){}
			synchronized(nextMessage) {
				nextMessage=new Quote(++id);
			}
		}
	}
}

class TradeHandler implements Runnable{
	private StreamHandler s;
	TradeHandler(StreamHandler s){
		this.s=s;
	}
	@Override public void run() {
		while(true) {
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){}
			synchronized(s.nextMessage) {
				if(s.nextMessage instanceof Trade) {
					System.out.println(s.nextMessage);
				}
			}
		}
	}
}

class QuoteHandler implements Runnable{
	private StreamHandler s;
	QuoteHandler(StreamHandler s){
		this.s=s;
	}
	@Override public void run() {
		while(true) {
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){}
			synchronized(s.nextMessage) {
				if(s.nextMessage instanceof Quote) {
					System.out.println(s.nextMessage);
				}
			}
		}
	}
}