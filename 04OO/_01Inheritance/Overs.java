package _01Inheritance;
public class Overs {
	public static void main(String[] args) { //DEBUGME
		Child c=new Child();
		System.out.println(c.method());
		System.out.println(c.method(4321));
		System.out.println(c.method(69,96));
		int[]tmp={1,1,2,3,5,8,13};
		System.out.println(c.method(tmp));
		System.out.println(c.method2(42));
		System.out.println(c.method2(42,42));
		System.out.println(c.method2(2,6,7,21,42));}}
class Parent{
	int method() {
		return 21;
}}
class Child extends Parent{
	@Override
	int method(){
		return method(new int[]{super.method(),42});}
	int method(int x){ //Overload
		return method(new int[]{x});}
	int method(int x,int y){ //Overload
		return method(new int[]{x,y});} //anonymous array
	int method(int[]l){ //main one //Overload
		int res=0;
		for(int e:l)
			res+=e;
		return res;}
	//have to name it differently, otherwise it overlaps with int method
	int method2(int... values){
		return method(values);}
}