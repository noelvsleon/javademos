package _01Inheritance;

import java.io.Serializable;
import java.time.ZonedDateTime;
import Ref.Ric;

@SuppressWarnings("serial") public class Instrument implements Serializable{
	long id;
	String name;
	Ric ric;
	String isin;
	String sedol;
	String bbid;
	public Instrument(String name){
		this.name=name;
	}
	public String toString(){
		return name;
	}
}
@SuppressWarnings("serial") class EqInstrument extends Instrument{
   Dividend nextDiv;

	public EqInstrument(String name){
		super(name);
	}
}
@SuppressWarnings("serial") class FutInstrument extends Instrument{
   ZonedDateTime expiry;
	Instrument underlier;
	boolean physicalSettlement;
	public FutInstrument(String name){
		super(name);
	}
}
class Dividend{
	ZonedDateTime announce;
	ZonedDateTime confirm;
	ZonedDateTime ex;
	ZonedDateTime paid;
	double amtPerShare;
	Dividend(ZonedDateTime announce,double amtPerShare){
		this.announce=announce;
		this.amtPerShare=amtPerShare;
	}
}
/*TODO
Index
bond
methods
*/