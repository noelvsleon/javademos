package _01Inheritance;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import Ref.Ric;

public class Inheritance{

	public static void main(String[] args){
		Instrument[] portfolio=new Instrument[5];
		portfolio[0]=new Instrument("TSCO Bond");
		portfolio[0].isin="XS0289810318";
		
		portfolio[1]=new EqInstrument("Tesco Ord");
		portfolio[1].ric=new Ric("TSCO.L");
		//https://uk.advfn.com/stock-market/london/tesco-TSCO/dividends
		Dividend d=new Dividend(ZonedDateTime.of(2018,10,03,8,0,0,0,ZoneId.of("GMT")),1.67);
		d.ex=ZonedDateTime.of(2018,11,11,8,0,0,0,ZoneId.of("GMT"));
		d.paid=ZonedDateTime.of(2018,11,23,8,0,0,0,ZoneId.of("GMT"));
		((EqInstrument)portfolio[1]).nextDiv=d;
		
		portfolio[2]=new FutInstrument("Gold future Dec23");
		if(portfolio[2]instanceof FutInstrument) {
			((FutInstrument)portfolio[2]).expiry=ZonedDateTime.of(2023,12,27,8,0,0,0,ZoneId.of("GMT")); //https://www.theice.com/products/31500921/Mini-Gold-Future/expiry
		}
		
		for(Instrument currentInstrument:portfolio) {
			System.out.print(currentInstrument);
			if(currentInstrument instanceof EqInstrument) {
				System.out.println("=Div Announce Date:"+((EqInstrument)currentInstrument).nextDiv.announce);
			}else if(currentInstrument instanceof FutInstrument){
				System.out.println("=Underlier:"+((FutInstrument)currentInstrument).underlier);
			}else {
				System.out.println("=Just a generic instrument");
			}
		}
	}
}
