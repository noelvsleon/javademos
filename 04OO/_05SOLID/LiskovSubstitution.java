package _05SOLID;

public class LiskovSubstitution{
	public static void main(String[] args){
		 Print(new Parent()); //original - works
		 Print(new Child()); //refactored to use child instead - broken. Unless this was deliberate.
	}
	static void Print(Parent p) {
		System.out.println(p.m(6));
	}
}

class Parent{
	int m(int x) {
		if(x>10) {
			return 42;
		}else {
			return ++x;
		}
	}
}
class Child extends Parent{
	@Override
	int m(int x) {
		if(x>5) {
			System.exit(1); 
			return -1;
		}else {
			return super.m(x);
		}
	}
}