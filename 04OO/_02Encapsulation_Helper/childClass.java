package _02Encapsulation_Helper;

public class childClass extends parentClass{
	//override the public constructor
	childClass(){
		b=100;
	}
	int getA(){
		//don't have access to super.a, parent has a b, and child has a b
		return super.getA()*super.b*b;
	}
}
