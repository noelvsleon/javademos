package _02Encapsulation;

import _02Encapsulation_Helper.childClass;
import _02Encapsulation_Helper.packageClass;
import _02Encapsulation_Helper.parentClass;

public class world {
	public static void main(String[] args) {
		//open panes, this one, (parent+child+package from encapsulation_helper)
		//copy to deleteme_world.java and then FIX ME using comments
		parentClass po=new parentClass();
		childClass co=new childClass();	//can't create child because not public constructor
		packageClass pack=new packageClass();
		childClass poc=pack.c; //c is not public

		int la=po.a; //a is not public
		int lb=po.b; //b is not public
		int lc=po.c; //c is not public
		int ld=po.d;

		nonPubClass npc=new nonPubClass(); //class is not public
		nonPubClass2 npc2=new nonPubClass2(); //class is not public, so constructor definition is superfluous
		
		//now look at packageClass in more detail
		pack.demo();
		//now look at parentClass in more detail
		
		//now look at childClass in more detail
		
	}

	
/*
 * private
 * 
 * protected
 * public
 * 
 * class
 * subclass
 * package
 * world
 * 
 * field
 * method
 * class
 * constructor
 * 
 * TODO:
 * static
 * 
 */
}
