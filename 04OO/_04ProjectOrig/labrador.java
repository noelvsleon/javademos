package _04ProjectOrig;
/**Labradors are gun dogs, and like to retrieve.
 * https://en.wikipedia.org/wiki/Labrador_Retriever
 * They make good family pets, assistance dogs, but poor guard dogs.
 * @author david.hodgins
 */
public class labrador extends dog{
	/**The colours a lab can be*/
	String[] colours={"Brown","Black"};
	/**The colour of this lab*/
	String colour;
	/**When constructing lab objects, we need to specify their colour and how many limbs they have - in case they have had an amputation
	 * @param colour of this lab
	 * @param limbs how many limbs this lab has*/
	labrador(String colour,byte limbs){
		this.colour=colour;
		if(limbs==3){ //If dog was unlucky when crossing the road
			this.limbs=limbs;
			System.out.println("3 legged dogs are 50% off -- Today only!!");
		}
		boolean found=false;
		for(int i=0;i<colours.length;i++)
			if(colours[i].equals(colour))
				found=true;
		if(!found)
			System.out.println("Invalid Colour");
		
		System.out.println("You have bought a "
				+colour
				+" labrador which is "
				+(isNice?"nice":"not nice")
				+" and has "
				+(hasWarmBlood?"warm":"cold")
				+" blood with "
				+limbs
				+" limbs. A dog is for life, not just for xmas!");
	}
	/**Polymorphic constructor
	 * Most dogs have 4 legs, so allow the user to construct 'normal' labs.
	 * This constructor calls the other constructor and passes in the default number of limbs(4).
	 * 
	 * @param colour the colour of the lab
	 */
	labrador(String colour){
		this(colour,(byte)4);
	}
}
