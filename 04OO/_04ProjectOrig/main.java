package _04ProjectOrig;
/**Run this class to see the demo. As with most of the examples, debugging it is more interesting
 * 
 * Showcases inheritance,statics,interfaces, etc using animal examples*/
public class main {
	/**The main method, you would not normally document the fact that main is the entry point to your program
	 * 
	 * Create some dog subclasses, showcase equality of objects.
	 * Create an array of animals of different classes, because this is interesting to debug the various constructors, super constructors, inheritance etc.
	 * @param args the cmdline args, wouldn't normally document this, unless we were going to specify what the valid argument are, eg it takes 3 arguments an address a user and a pass.*/
	public static void main(String[] args) {
		labrador mydog=new labrador("Black");
		labrador yourdog=new labrador("Black");
		System.out.println(mydog==yourdog);
		System.out.println(mydog.equals(yourdog)); //would need to implement the equals method
		//open all the classes and drag to corners
		//main+animal->reptile+mammal->cat+dog->alsation+labrador
		//minimise side and bottom panes
		animal[]petShop=new animal[8];
		petShop[0]=new labrador("Blue");
		petShop[1]=new labrador("Blue",(byte)3);
		petShop[2]=new labrador("Black");
		petShop[3]=new alsation();
		petShop[4]=new dog();
		petShop[5]=new mammal();
		petShop[6]=new cat();
		petShop[7]=new reptile();
	}

}
