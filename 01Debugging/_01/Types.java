package _01;

public class Types {

	public static void main(String[] args) {
		//DEBUG ME
			//hover over margin
			//double click margin to add breakpoint, right click too, hover to check due to unused var messages.
			//fix warnings by clicking one and choosing supress to main,
			//then show breakpoints again
			//debug perspective, debug ant icons (next to play, and perspective one)
			//resume
			//debug again
			//step into all the way to .getClass method, explaining the types and vars as go along
			//step into vs step over for getClass, step return
			//step over for the rest
		byte b=0x1a;
		short h=42;
		int i=65000;
		int anotherWholeNumber=42;
		short myFirstShort=(short)anotherWholeNumber;  //cast from 0 0 0 42 to 0 42
		short h2=(short)65000; //overflow
		long j=2147483648L;
		float f=4.2f;
		float f2=1/3;
		double d=4.2;
		double d2=1/3.;
		boolean l=true;
		char c='d';
		String s="life";
		String className=s.getClass().getName(); //look at 'value' in the debugger
		int len=s.length();
		int twentysix_a=26;
		int twentysix_b=0x1a;
		int twentysix_c=0b11010; //Java 7
		String escapes="\t\n\"\'\\";
		long creditCardNumber = 1234_5678_9012_3456L; //Java 7 allows random _ in number literals
	}
}
