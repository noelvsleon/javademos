package _02Methods;

//template for create name objects
public class Name {
	//each name object gets a different first and last name
	/*this is
	 * called object state
	 */
	String first;
	String last;
	//classes have a function/method/constructor inside them with the same name, this is called/invoked when creating the object
	//objects are created with the new keywor
	//....new name(...,...)
	//to create a name, you have to pass 2 strings
	Name(String first_,String last){
		//c style is to use different names
		first=first_;
		//java style is to use this. to distinguish between the method property and the argument/parameter
		this.last=last;
	}
}
