package _02Methods;

//can't write java code outside classes so need a class
//its confusing to have the main class do other things, especially when teaching, so here it is just a wrapper for our main program code
public class StartingPoint {
	//can't write any real code in java outside of a block
	//the static main method is a block of code for the class, no objects are involved. this is overly complicated, but that's java for you
	//java likes things to be explicit, so args is the list of parameters passed to the program, from wherever it was invoked, eg the command line
	public static void main(String[] args) {
		//finally! the real code starts
		System.out.println("What is the meaning of life, the universe and everything?");
		//create a name object to represent me
		Name me=new Name("david","hodgins");
		//instantiate the name class to represent not me
		Name default_=new Name("AN","Other");
		//create a list of names
		//here new applies to the [] array, and does not create 3 name objects, simply a place to store such objects i.e. an array of 3 null pointers
		Name[]listofnames=new Name[3];
		//populate the array
		listofnames[0]=me; //change the first null pointer to point at the me object, which is an instance of the name class
		listofnames[1]=default_; //put the object reference into the second position in the array
		listofnames[2]=new Name("harvey","specter"); //theres no need to name the objects we put into the array
		System.out.println(listofnames[2].first+" is awesome"); //print out harvey, because he is cool
	}	
}
