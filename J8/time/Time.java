package time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class Time{

	public static void main(String[] args){
		LocalDate today=LocalDate.now();
		LocalDate yesterday=today.minusDays(1);
		Period oneday=Period.between(yesterday,today);
		LocalDate startOfMillenium=LocalDate.of(2000,1,1);
		LocalDate nextYear=today.plus(1,ChronoUnit.YEARS);
		LocalTime sequential=LocalTime.ofNanoOfDay(5544333222111L); //01:32:24.333222111
		LocalDateTime jobStart=LocalDateTime.of(today,sequential);
	}

}
