package lambdas;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.function.Function;

interface mmethod{
	void mmmethod(Object[] args);
}
interface mmethod2{
	void mmmethod(Object... args);
}
interface mmethod3{
	int mmmethod(Object... args);
}
public class main{
	int life=4242;
	main(){
		//lambdas
		Runnable f1=()->{System.out.println("f1");};
		f1.run();	
		int a=6;
		int b=7;
		int c=42;
		mmethod f2=(Object[] o)->{System.out.println((Integer)o[0]*(Integer)o[1]);};
		f2.mmmethod(new Object[] {a,b});
		
		mmethod2 f3=(Object... o)->{System.out.println((Integer)o[0]*(Integer)o[1]);};
		f3.mmmethod(a,c);
		
		mmethod3 f4=(Object... o)->{return gymnastics(o);};
		System.out.println(f4.mmmethod(a,b,c));
		
	}
	public static void main(String[] args) {
		new main(); //Java scoping sucks, so test everything as locals to an object rather than to a static
	}
	int gymnastics(Object... o) {
		int result=life;
		for(var e:o) {
			result+=(Integer)e;
		}
		return 42;
	}
}
