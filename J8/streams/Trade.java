package streams;

import java.time.LocalTime;
import java.util.Random;

public class Trade{
	public LocalTime time;
	public String sym;
	public double price;
	public int size;
	public char side;

	public Trade(String sym,LocalTime time,double price,int size,char side) {
		this.time=time;
		this.sym=sym;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public LocalTime getTime() {
		return time;
	}
}

class GenTrades{
	public static Trade[] gen(int count) {
		Random r=new Random();
		Trade[]t=new Trade[count];
		String[]syms= {"VOD.L","BT.L","VOD.O","GOOG.N"}; //TODO check .O/.N
		double[]egPrices= {165,221,2345,34567};
		LocalTime now=LocalTime.now();
		for(int i=0;i<count;i++) {
			int index=r.nextInt(syms.length);
			t[i]=new Trade(
					syms[index],
					now.plusSeconds(i*10),
					egPrices[index]+r.nextDouble()*15,
					r.nextInt(2000),
					r.nextBoolean()?'S':'B');
		}
		return t;
	}
}
