package jit;

import java.io.IOException;

public class JIT4{
	static void myMethod(int numIterations) {
		long result=0;
		for(int i=0;i<numIterations;++i) {
			result+=2*i;
		}
	}
	public static void main(String[] args) throws IOException, InterruptedException{
		System.out.println("\n\n\n\n\n\nHello World");
		long before,after;
		before=System.nanoTime();
		myMethod(60415);
		after=System.nanoTime();
		long duration=after-before;
		System.out.println("#########60415========"+duration);

		before=System.nanoTime();
		myMethod(10);
		after=System.nanoTime();
		System.out.println("#########10========"+(after-before));
		
		before=System.nanoTime();
		myMethod(60415);
		after=System.nanoTime();
		System.out.println("#########60415========"+(after-before));
		System.out.println("#########Pre comp vs after comp "+duration/(double)(after-before));

		Thread.sleep(1000);
		
		before=System.nanoTime();
		myMethod(60415);
		after=System.nanoTime();
		System.out.println("#########60415========"+(after-before));
		System.out.println("#########Pre comp vs after comp "+duration/(double)(after-before));
	}
}
