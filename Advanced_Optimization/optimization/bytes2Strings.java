package optimization;

public class bytes2Strings{
	static String withoutIntern(byte[]b) {
		return new String(b);
	}
	static String withIntern(byte[]b) {
		return (new String(b)).intern();
	}
	public static void main(String[] args){
		//PROFILE ME>Heap
		int max=5000000;
		String[]a1=new String[max];
		String[]a2=new String[max];
		byte[] msg="VOD.L,1234,4,56,B".getBytes();
		long before,after;
		before=System.nanoTime();
		for(int i=0;i<max;++i) {
			a1[i]=withoutIntern(msg);
		}
		after=System.nanoTime();
		long duration=after-before;
		System.out.println(duration);

		before=System.nanoTime();
		for(int i=0;i<max;++i) {
			a2[i]=withIntern(msg);
		}
		after=System.nanoTime();
		System.out.println(after-before);
		System.out.println((after-before)/(double)duration);
	}

}
