package optimization.maps06;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import optimization.maps03.DateTime;
import optimization.maps03.Trade;

public class Maps6{
	static int numMsgs=4000000; //Increase for VisualVM 400000
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		//Debug(VisualVM)
		/*
		 * Sampler>CPU>Hotspots, Choose Threads main
		 * Resume
		 */
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* 
		 * iterate from the end of the trades AL, and remove as we go .remove() will return the object, check if the HandleTable still holds the references and if so, how do we cause unshared to be true?
		 * further isolate the client from the server by replacing readAllBytes with a less leaky version.
		 * ObjectOutputStream.defaultWriteFields is dirty
		 * send bytes instead of objects
		 * 
		 * This version seems much slower, if you suspend the client thread then the sent trades get GC'd
		 * Server and client are at 32/30MB and 290K have been sent
		 *
		 * Next we figure out why this version is 3x slower
		 * 
		 */
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Client "+(endClient-startClient)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps6.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		for(int i=0;i<Maps6.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps6.startClient=System.nanoTime();
			DataOutputStream server=new DataOutputStream(serverSock.getOutputStream());
			for(int i=Maps6.numMsgs-1;i>=0;i--) {
				Trade t=trades.remove(i);
				server.writeLong(t.exchTime.nanoseconds);
				server.writeLong(t.time.nanoseconds);
				server.writeBytes(t.ticker); //test grabbing the bytes once instead of letting it do it repeatedly
				server.writeDouble(t.price);
				server.writeInt(t.size);
				server.writeChar(t.side);
			}
			server.flush();
			Maps6.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
//	ArrayList<Trade>trades=new ArrayList<>(Maps.numMsgs);;
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			@SuppressWarnings("resource") ServerSocket listenSock=new ServerSocket(3000);
			Maps6.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			DataInputStream client=new DataInputStream(clientSock.getInputStream());
			byte[]b=new byte[1024];
			while(client.read(b)!=-1) {
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}