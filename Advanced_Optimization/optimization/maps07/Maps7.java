package optimization.maps07;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import optimization.maps03.DateTime;

public class Maps7{
	static int numMsgs=63000; //for 1s
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* 
		 * Maps6 was 3x slower than Maps3
		 * This version we avoid the multiple writes and write methods that are inefficient, 
		 * we establish a proper protocol and recreate the Trades on the server. 
		 * This version is 2x as fast as Maps3. 
		 *  
		 * Next version will increase numMsgs so we can profile it.
		 */
		c.join();
		s.join();
		double toSeconds=1000000000.;
		System.out.println("Client "+(endClient-startClient)/toSeconds);
		System.out.println("Server "+(endServer-startServer)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps7.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		for(int i=0;i<Maps7.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				123.45+i,
				1000+i,
				0==i/2?'B':'S',
				"vod"));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps7.startClient=System.nanoTime();
			DataOutputStream server=new DataOutputStream(serverSock.getOutputStream());
			byte[]writeBuffer=new byte[30];
			long price;
			for(int i=Maps7.numMsgs-1;i>=0;i--) {
				Trade t=trades.remove(i);
//multiple writes are really slow
//				server.writeLong(t.exchTime.nanoseconds);
//				server.writeLong(t.time.nanoseconds);
//				server.writeBytes(t.ticker); //test grabbing the bytes once instead of letting it do it repeatedly
//				server.writeDouble(t.price);
//				server.writeInt(t.size);
//				server.writeChar(t.side);
				
		      writeBuffer[0] = (byte)(t.exchTime.nanoseconds >>> 56);
		      writeBuffer[1] = (byte)(t.exchTime.nanoseconds >>> 48);
		      writeBuffer[2] = (byte)(t.exchTime.nanoseconds >>> 40);
		      writeBuffer[3] = (byte)(t.exchTime.nanoseconds >>> 32);
		      writeBuffer[4] = (byte)(t.exchTime.nanoseconds >>> 24);
		      writeBuffer[5] = (byte)(t.exchTime.nanoseconds >>> 16);
		      writeBuffer[6] = (byte)(t.exchTime.nanoseconds >>>  8);
		      writeBuffer[7] = (byte)(t.exchTime.nanoseconds >>>  0);
		      writeBuffer[8] = (byte)(t.time.nanoseconds >>> 56);
		      writeBuffer[9] = (byte)(t.time.nanoseconds >>> 48);
		      writeBuffer[10] = (byte)(t.time.nanoseconds >>> 40);
		      writeBuffer[11] = (byte)(t.time.nanoseconds >>> 32);
		      writeBuffer[12] = (byte)(t.time.nanoseconds >>> 24);
		      writeBuffer[13] = (byte)(t.time.nanoseconds >>> 16);
		      writeBuffer[14] = (byte)(t.time.nanoseconds >>>  8);
		      writeBuffer[15] = (byte)(t.time.nanoseconds >>>  0);
		      price=Double.doubleToLongBits(t.price);
		      writeBuffer[16] = (byte)(price >>> 56);
		      writeBuffer[17] = (byte)(price >>> 48);
		      writeBuffer[18] = (byte)(price >>> 40);
		      writeBuffer[19] = (byte)(price >>> 32);
		      writeBuffer[20] = (byte)(price >>> 24);
		      writeBuffer[21] = (byte)(price >>> 16);
		      writeBuffer[22] = (byte)(price >>>  8);
		      writeBuffer[23] = (byte)(price >>>  0);
		      writeBuffer[24]=(byte)((t.size >>> 24) & 0xFF);
		      writeBuffer[25]=(byte)((t.size >>> 16) & 0xFF);
		      writeBuffer[26]=(byte)((t.size >>> 8) & 0xFF);
		      writeBuffer[27]=(byte)((t.size >>> 0) & 0xFF);
		      writeBuffer[28]=(byte)t.side;
		      writeBuffer[29]=(byte)t.ticker.length();
		      
		      server.write(writeBuffer);
		      server.write(t.ticker.getBytes()); //we could get the bytes once instead of letting it do it repeatedly if we had order to the trades
			}
			server.flush();
			Maps7.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	@SuppressWarnings("static-access") public void run() {
		try{
			@SuppressWarnings("resource") ServerSocket listenSock=new ServerSocket(3000);
			Maps7.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			DataInputStream client=new DataInputStream(clientSock.getInputStream());
			byte[]b=new byte[1024];
			long exchTimeLong,timeLong;
			double priceDouble;
			int sizeInt;
			char sideChar;
			byte tickerLengthByte;
			String tickerString;
			for(int i=0;i<Maps7.numMsgs;i++) {
				new Trade(new DateTime(client.readLong()),
						new DateTime(client.readLong()),
						client.readDouble(),
						client.readInt(),
						(char)client.readByte(),
						new String(client.readNBytes(client.readByte())));
			}
			Maps7.endServer=System.nanoTime();
			clientSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}