package optimization.maps04;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Comparator;
import optimization.maps03.DateTime;
import optimization.maps03.Trade;

public class Maps4{
	static int numMsgs=4000000; //Increase for VisualVM
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		//Debug(VisualVM)
		/*
		 * Sampler>CPU>Hotspots, Choose Threads main
		 * Resume
		 */
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* We removed the gc call.
		* Created our own datetime class
		* 
		* Sampler>CPU>Hotspots, Choose Threads main, Serverr, Client
		 *
		 * Now plusSeconds() is replaced by plus() and doesn't make it to the hotspot list
		 * Client constructor is now even faster
		 * 
		 * Sampler>Memory>Per thread allocations>right click>filter>main,Client,Serverr>Enter
		 * 	Trade we have just shy of 8M Trade objects 319M despite only generating 4M on client and not keeping them on the server, 
		 * 	Object[], int[], HandleList are still large
		 * 	LocalDateTime is gone, If we filter by DateTime we only have 4 instances (2 for client, 2 for server) 
		 * 
		 * Next we will isolate the client by not reading objects on the server, but just reading bytes
		 * 
		 */
		s.join();
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Server "+(endServer-startServer)/toSeconds);
		System.out.println("Client "+(endClient-startClient)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps4.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		for(int i=0;i<Maps4.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps4.startClient=System.nanoTime();
			ObjectOutputStream server=new ObjectOutputStream(serverSock.getOutputStream());
			for(int i=0;i<Maps4.numMsgs;i++) {
				server.writeObject(trades.get(i));
			}
			server.flush();
			Maps4.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			ServerSocket listenSock=new ServerSocket(3000);
			Maps4.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			ObjectInputStream client=new ObjectInputStream(clientSock.getInputStream());
			for(int i=0;i<Maps4.numMsgs;i++) {
				client.readObject();
			}
			Maps4.endServer=System.nanoTime();
			listenSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
}