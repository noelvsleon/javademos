package optimization.maps11;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import optimization.maps03.DateTime;
import optimization.maps10.Trade;

public class Maps11{
	static int numMsgs=4000000; //120000; //4000000; //for 31s
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		Client c=new Client();
		Serverr s=new Serverr();

		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/*
		 * This time: non blocking client
		 * bytebuffer, use wrap, and manually set the position, limit
		 * client buffer never filled, unless cause it to in debug mode
		 * 
		 * Next times
			 * non blocking server
			 * Could explore TCP buffer size
			 * UDP vs TCP
			 * Last trade for vod
			 * Last trade table
			 * Keeping all trades on the server ArrayList of Objects, Object of ArrayLists
			 * Multiple instruments, HashMap, then EnumMap
			 * OrderBook
			 * ticker -> test grabbing the bytes once instead of letting it do it repeatedly
		 * 
		 */
		c.join();
		s.join();
		double toSeconds=1000000000.;
		System.out.println("Client "+(endClient-startClient)/toSeconds);
		System.out.println("Server "+(endServer-startServer)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps11.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		byte[]vod=new byte[] {'v','o','d'};
		for(int i=0;i<Maps11.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				123.45+i,
				1000+i,
				0==i/2?'B':'S',
				vod));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			SocketAddress a=new InetSocketAddress("localhost",3000);
			SocketChannel server=SocketChannel.open();
			server.connect(a);
			Maps11.startClient=System.nanoTime();
			server.configureBlocking(false);
			final byte[]writeBufferA=new byte[40];
			final ByteBuffer writeBuffer=ByteBuffer.wrap(writeBufferA);
			
			long price;
			for(int i=Maps11.numMsgs-1;i>=0;i--) {
				Trade t=trades.remove(i);
		      writeBufferA[0] = (byte)(t.exchTime.nanoseconds >>> 56);
		      writeBufferA[1] = (byte)(t.exchTime.nanoseconds >>> 48);
		      writeBufferA[2] = (byte)(t.exchTime.nanoseconds >>> 40);
		      writeBufferA[3] = (byte)(t.exchTime.nanoseconds >>> 32);
		      writeBufferA[4] = (byte)(t.exchTime.nanoseconds >>> 24);
		      writeBufferA[5] = (byte)(t.exchTime.nanoseconds >>> 16);
		      writeBufferA[6] = (byte)(t.exchTime.nanoseconds >>>  8);
		      writeBufferA[7] = (byte)(t.exchTime.nanoseconds >>>  0);
		      writeBufferA[8] = (byte)(t.time.nanoseconds >>> 56);
		      writeBufferA[9] = (byte)(t.time.nanoseconds >>> 48);
		      writeBufferA[10] = (byte)(t.time.nanoseconds >>> 40);
		      writeBufferA[11] = (byte)(t.time.nanoseconds >>> 32);
		      writeBufferA[12] = (byte)(t.time.nanoseconds >>> 24);
		      writeBufferA[13] = (byte)(t.time.nanoseconds >>> 16);
		      writeBufferA[14] = (byte)(t.time.nanoseconds >>>  8);
		      writeBufferA[15] = (byte)(t.time.nanoseconds >>>  0);
		      price=Double.doubleToLongBits(t.price);
		      writeBufferA[16] = (byte)(price >>> 56);
		      writeBufferA[17] = (byte)(price >>> 48);
		      writeBufferA[18] = (byte)(price >>> 40);
		      writeBufferA[19] = (byte)(price >>> 32);
		      writeBufferA[20] = (byte)(price >>> 24);
		      writeBufferA[21] = (byte)(price >>> 16);
		      writeBufferA[22] = (byte)(price >>>  8);
		      writeBufferA[23] = (byte)(price >>>  0);
		      writeBufferA[24]=(byte)((t.size >>> 24) & 0xFF);
		      writeBufferA[25]=(byte)((t.size >>> 16) & 0xFF);
		      writeBufferA[26]=(byte)((t.size >>> 8) & 0xFF);
		      writeBufferA[27]=(byte)((t.size >>> 0) & 0xFF);
		      writeBufferA[28]=(byte)t.side;
		      writeBufferA[29]=(byte)t.ticker.length;
		      writeBuffer.position(30); //some annoying checks, could subclass it and break the encapsulation
				writeBuffer.put(t.ticker); 
		      server.write(writeBuffer.flip());
		      while(0!=writeBuffer.remaining()) {
		      	System.out.println("Client buffer full");
		      	server.write(writeBuffer);
		      }
		      writeBuffer.clear();
			}
			Maps11.endClient=System.nanoTime();
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
interface fi{
	byte[] m();
}
class Serverr extends Thread{
//	ArrayList<Trade>trades=new ArrayList<>(Maps.numMsgs);;
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	@SuppressWarnings("static-access") public void run() {
		try{
			ServerSocketChannel listenSock = ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000));
			Maps11.startServer=System.nanoTime();
			SocketChannel client=listenSock.accept();
			final ByteBuffer b=ByteBuffer.allocate(40);
			final byte[]ticker=new byte[10];
			for(int i=0;i<Maps11.numMsgs;i++) {
				client.read(b); //TODO what if we don't have the whole message?
				b.flip();
				
				new Trade(
						new DateTime(b.getLong()),
						new DateTime(b.getLong()),
						b.getDouble(),
						b.getInt(),
						(char)b.get(),
						(new fi(){public byte[] m(){byte tickerLengthByte=b.get();b.get(ticker,0,(int)tickerLengthByte);return ticker;}}).m());
				b.compact();
			}
			Maps11.endServer=System.nanoTime();
			client.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}