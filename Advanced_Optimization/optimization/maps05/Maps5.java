package optimization.maps05;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Comparator;
import optimization.maps03.DateTime;
import optimization.maps03.Trade;

public class Maps5{
	static int numMsgs=4000000; //Increase for VisualVM
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		//Debug(VisualVM)
		/*
		 * Sampler>CPU>Hotspots, Choose Threads main
		 * Resume
		 */
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* 
		 * Isolate the client by not reading objects on the server, but just reading bytes
		* 
		 * Sampler>Memory>Per thread allocations>right click>filter>main,Client,Serverr>Enter
		 * 	Trade we have just shy of 8M Trade objects 319M despite only generating 4M on client and not keeping them on the server, 
		 * 	Object[], int[], HandleList are still large
		 * 	LocalDateTime is gone, If we filter by DateTime we only have 4 instances (2 for client, 2 for server) 
		 * 
		 * Heap histogram shows that it was the server creating the new Trade objects.
		 * TODO why is client growing to 100's MB, Heap Dump.
		 * HeapDump>Objects>expand the references on a trade, there is a reference in the ArrayList, and another in the HandleTable
		 *		byte[] is 111MB so investigate -> ignore the first few, all the 8192's are referenced from object[], so look at those, not much help
		 * Looking inside the methods we are calling:
		 * ObjectOutputStream.defaultWriteFields is dirty
				new byte[]
				new Object[]
			Servers readAllBytes is also dirty, will further isolate in the next version.
		 * 
		 * Visual GC -> pretty
		 * 
		 * Next Time:
		 * 	further isolate the client from the server by replacing readAllBytes with a less leaky version.
		 * 	iterate from the end of the trades AL, and remove as we go .remove() will return the object, check if the HandleTable still holds the references and if so, how do we cause unshared to be true?
		 * 	ObjectOutputStream.defaultWriteFields is dirty ->fix
		 */
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Client "+(endClient-startClient)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps5.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		for(int i=0;i<Maps5.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps5.startClient=System.nanoTime();
			ObjectOutputStream server=new ObjectOutputStream(serverSock.getOutputStream());
			for(int i=0;i<Maps5.numMsgs;i++) {
				server.writeObject(trades.get(i));
			}
			server.flush();
			Maps5.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			@SuppressWarnings("resource") ServerSocket listenSock=new ServerSocket(3000);
			Maps5.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			InputStream is=clientSock.getInputStream();
			while(true) {
				is.readAllBytes();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}