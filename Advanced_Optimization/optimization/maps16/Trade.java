package optimization.maps16;

public class Trade{
	long exchTime;
	long time;
	double price;
	int size;
	char side;
	public Trade(long exchTime,long time, double price, int size, char side){
		this.exchTime=exchTime;
		this.time=time;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public String toString(){
		return DateTime.toString(exchTime)+","+DateTime.toString(time)+","+price+","+size+","+side;
	}
}