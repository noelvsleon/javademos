package optimization.maps16;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import optimization.maps15.RefData;

class ExchangeConnection{
        final ByteBuffer[] ba=new ByteBuffer[] {ByteBuffer.allocate(30),ByteBuffer.allocate(32)}; //TODO benchmark a bigger buffer
        final ByteBuffer bm=ByteBuffer.allocate(1); //TODO benchmark a bigger buffer
	int numBytesRead=0;
	byte tickerByte;
	String ticker;
	Trade t;
	Fill f;
	byte msgType=0x02;
	int[]msgSizes=new int[] {31,33}; //we added 1 to optimise code later
	final SocketChannel[]exchanges=new SocketChannel[Maps16.numClients];
	void addExchange(SocketChannel exch,byte id){
		exchanges[id]=exch;
	}
	void receiveMsg(SocketChannel client,byte exchId) throws IOException{
			if(msgType==0x02) {
				numBytesRead=client.read(bm);
				if(numBytesRead==0) {
					++Serverr.timesServerBufferEmpty;
					return;
				}else {
					msgType=bm.array()[0];
					bm.clear();
				}
			}
			numBytesRead+=client.read(ba[msgType]);
			if(numBytesRead<msgSizes[msgType]) {
				++Serverr.timesServerBufferEmpty;
				return;
			}
			ByteBuffer b=ba[msgType];
			b.flip();
			if(msgType==0x00) {
				t=new Trade(
						b.getLong(),
						b.getLong(),
						b.getDouble(),
						b.getInt(),
						(char)b.get());
				tickerByte=b.get();
				Serverr.trades[tickerByte].newTrade(t,exchId);
			}else {
				f=new Fill(
						b.getLong(),
						b.getLong(),
						b.getDouble(),
						b.getInt(),
						(char)b.get(),
						b.getShort());
				tickerByte=b.get(); //This fixes a bug from the past few versions
				Serverr.trades[tickerByte].newFill(f,exchId);
			}
			numBytesRead=0;
			msgType=0x02;
			b.clear();
			++Serverr.numMsgsRecv;
	}
	void receive() {
		final int numMsgs=Maps16.numMsgsIncFills*Maps16.numClients;
		while(Serverr.numMsgsRecv<numMsgs){
			for(int i=0;i<exchanges.length;++i){
				try{
					receiveMsg(exchanges[i],(byte)i);
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}

}
class Serverr extends Thread{
 	static final MarketData[]trades=new MarketData[RefData.syms.length];
 	static {
 		for(int i=0;i<trades.length;++i) {
 			trades[i]=new MarketData(RefData.syms[i]);
 		}
 	}
	static long timesServerBufferEmpty=0;
	static int numMsgsRecv=0;
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			Maps16.startServer=System.nanoTime();
			SocketChannel[]client=new SocketChannel[Maps16.numClients];
			ExchangeConnection exs=new ExchangeConnection();
			for(int i=0;i<Maps16.numClients;++i) {
				client[i]=SocketChannel.open();
				client[i].connect(new InetSocketAddress("localhost",3000+i));
				client[i].configureBlocking(false);
				exs.addExchange(client[i],(byte)i);
			}
			exs.receive();
			Maps16.endServer=System.nanoTime();
			System.out.println(Now.n()+"Server buffer empty "+timesServerBufferEmpty+" Num trades "+trades.length);
			for(int i=0;i<5;++i) {
				MarketData md=trades[i];
				if(md==null) {
					System.out.println(Now.n()+" "+RefData.syms[i]+" No trades");
				}else {
					for(int j=0;j<Maps16.numClients;++j) {
						System.out.printf("%s: %s Count %6d Last %91s High %91s Low %91s\n",Client.exchangeNames[j],RefData.syms[i],md.trades[j].size(),md.trades[j].get(md.trades[j].size()-1),md.high,md.low);
					}
				}
			}
			for(SocketChannel s:client) {
				s.configureBlocking(true);
				s.close();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}