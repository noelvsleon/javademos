package optimization.maps16;

import java.time.LocalDateTime;

public class Maps16{
	static int numMsgs=2500000; //2500000; for 64s //120000 for 2s
	static int numMsgsIncFills=numMsgs; //2625000 //
	static int numClients=3;
	static int numServerMsgs=numClients*numMsgs;
	static Long startServer,endServer;
	public static void main(String[] args) throws InterruptedException{
		System.out.println(numMsgsIncFills);
		Client[] c=new Client[numClients];
		Serverr s=new Serverr();
		for(int i=0;i<c.length;++i){
			c[i]=new Client(i);
			c[i].start();
		}
		System.out.println(numMsgsIncFills);
		int numClientsReady=0;
		while(numClientsReady!=c.length) {
			numClientsReady=0;
			for(Client cl:c) {
				if(cl.startClient!=0) {
					++numClientsReady;
				}
			}
			Thread.sleep(1);
		}
		System.out.println(Now.n()+"Choose Thread Server, Client too");
		s.start();
		/*
		 * Prev Prev:
			 * DateTime 24M objects consuming 576MB, there are 12M Trade objects and 4M TradeC. Each DateTime is 16byte header+8bytes long=24bytes
		 * Prev:
		 	* Server detect pump and dump keep track of sequential buy Fills followed by a sell where the Trade price moved progressively higher(min increase of 5%) (70% of Fills at a price higher than the average of the last 3), the sell is then for at least 70% of the sum of the buys, and the Trades show the price deflating over the next 10 trades to at least 2.5% of its peak   
		 		* Currently just reporting indices of sequences of B or S >=10
	 		* now slower so reduced numMsgs from 4M to 2.5M. 4M used to take 51s, now 2.5M takes 64s. But we are doing more, so this is expected
	 		* Server buffer is normally not empty, 0 or 3, Client buffers are full alot ~35M
	 		* Profile:
		 		*  Serverr Hotspots
		 		*  	newFill 55ms out of 22s
		 		*  	receiveMsg 94ms out of 59s
		 		*  	receive 4.8s out of 64s
		 		*  	pumpAndDumpCheck 22s
		 		*  	SocketChannelImpl.read 37s
		 		*  So significant time being spent on things other than reading.
		 		*  Client memory is KBs Serverr is 940MB
	 	* This Time
	 		* Print first 5 instruments in server
	 		* Made Server Trade class independent of TradeC as it had both versions of ticker, and remove DateTime objects and store only the longs, then modify Trade toString method to use DateTime toString
	 		* If we have 8,000,000 trades and ~100 syms that thats roughly 80K trades per sym. So we could presize the Server ArrayLists at 100K as a reasonable guess, but it can always grow.
	 		* We could drop the ticker component of Servers Trade class as it could be added to the MarketData class(1 instead of N). Still need to recieve it, and use it, just not store it. 
	 		* We could remove the if else by prepopulating trades with MarketData objects containing fake high and low Trades with 0/MAX_VALUE prices
	 		* Profile:
	 			* Serverr memory reduced from 940MB to 388MB
		 * Next times
		 	 * UDP vs TCP
		 	 * 
		 	 * We could change Serverrs Trade class to have ArrayLists of values instead of having a List of Trades. We'd want to diagram this in Java due to autoboxing, before trying it out
		 	 * Change how Serverr keeps track of Fills, currently stores lots of nulls.
		 	 * We could play with the ByteBuffer size, and the TCP buffer sizes.
		 	 * Server detect pump and dump keep track of sequential buy Fills followed by a sell where the Trade price moved progressively higher(min increase of 5%) (70% of Fills at a price higher than the average of the last 3), the sell is then for at least 70% of the sum of the buys, and the Trades show the price deflating over the next 10 trades to at least 2.5% of its peak   
		 	 * We could switch the order of the if else branches in server
		 	 *  	
		 	 * Server now handles arbitrary number of clients
		 	 * Selector
		 	 * Server now handles reconnection
		 	 * 
		 	 * ...
			 * 
			 * HashMap, then EnumMap
			 * OrderBook
		 */
		for(int i=0;i<c.length;++i) {
			c[i].join();
		}
		s.join();
		double toSeconds=1000000000.;
		System.out.println(Now.n()+"Server "+(endServer-startServer)/toSeconds);
	}
}

interface fi{
	byte[] m();
}


class Now{
	static String n() {
		return LocalDateTime.now()+" ";
	}
}
