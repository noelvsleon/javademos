package optimization;

import java.lang.reflect.Field;

public class Strings{
	public static void main(String[] args){
		//Debug me
		System.out.println("literal "+System.identityHashCode("str1"));
		String s1="str1";
		System.out.println("s1      "+System.identityHashCode(s1));
		String s2=s1;
		System.out.println("s2      "+System.identityHashCode(s2));
		String s3="str1";
		System.out.println("s3      "+System.identityHashCode(s3));
		String s4=new String("str1");
		System.out.println("s4      "+System.identityHashCode(s4));
		String s5=new String(s1.getBytes());
		System.out.println("s5      "+System.identityHashCode(s5));
		String s6=new String(s1.getBytes());
		System.out.println("s6      "+System.identityHashCode(s6));
		String s7=s1.substring(1);
		System.out.println("s7      "+System.identityHashCode(s7));
		String s8=s1.toLowerCase();
		System.out.println("s8      "+System.identityHashCode(s8));
		String s9="str";
		System.out.println("s9      "+System.identityHashCode(s9));
		String s10=s4.intern();
		System.out.println("s10     "+System.identityHashCode(s10));
		try{
			Field field=s1.getClass().getDeclaredField("value");
			field.setAccessible(true);
			System.out.println("Reflect s1 "+System.identityHashCode(field.get(s1))+" "+field.get(s1));
			System.out.println("Reflect s2 "+System.identityHashCode(field.get(s2))+" "+field.get(s2));
			System.out.println("Reflect s3 "+System.identityHashCode(field.get(s3))+" "+field.get(s3));
			System.out.println("Reflect s4 "+System.identityHashCode(field.get(s4))+" "+field.get(s4));
			System.out.println("Reflect s5 "+System.identityHashCode(field.get(s5))+" "+field.get(s5));
			System.out.println("Reflect s6 "+System.identityHashCode(field.get(s6))+" "+field.get(s6));
			System.out.println("Reflect s7 "+System.identityHashCode(field.get(s7))+" "+field.get(s7));
			System.out.println("Reflect s9 "+System.identityHashCode(field.get(s9))+" "+field.get(s9));
		}catch(NoSuchFieldException e){
			e.printStackTrace();
		}catch(SecurityException e){
			e.printStackTrace();
		}catch(IllegalArgumentException e){
			e.printStackTrace();
		}catch(IllegalAccessException e){
			e.printStackTrace();
		}
		
		long before,after;
		int max=10000;
		before=System.nanoTime();
		for(int i=0;i<max;++i) {
			builderLoop();
		}
		after=System.nanoTime();
		long duration=after-before;
		System.out.println("builderLoop "+duration);
		
		before=System.nanoTime();
		for(int i=0;i<max;++i) {
			concatLoop();
		}
		after=System.nanoTime();
		System.out.println("concatLoop "+(after-before));
		System.out.println("concat/builder "+(after-before)/(double)duration);

		//PROFILE ME, look at VisualGC GC Time, 2ms, 110ms, 440ms
		max=100000000;
		before=System.nanoTime();
		for(int i=0;i<max;++i) {
			builderLoop();
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.println("builderLoop "+duration);
		
		before=System.nanoTime();
		for(int i=0;i<max;++i) {
			concatLoop();
		}
		after=System.nanoTime();
		System.out.println("concatLoop "+(after-before));
		System.out.println("concat/builder "+(after-before)/(double)duration);
	}
	static String builderLoop() {
		int n=1;
		StringBuilder b=new StringBuilder();
		for(int i=0;i<10;++i) {
			b.append(n*i);
		}
		return b.toString();
	}
	static String concatLoop() {
		int n=1;
		String res="";
		for(int i=0;i<10;++i) {
			res+=n*i;
		}
		return res;
	}

}
