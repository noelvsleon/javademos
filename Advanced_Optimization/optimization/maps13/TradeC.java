package optimization.maps13;

import optimization.maps03.DateTime;

public class TradeC{
	public DateTime exchTime;
	public DateTime time;
	public double price;
	public int size;
	public char side;
	public byte ticker;
	public TradeC(){
		
	}
	public TradeC(DateTime exchTime,DateTime time, double price, int size, char side,byte ticker){
		this.exchTime=exchTime;
		this.time=time;
		this.price=price;
		this.size=size;
		this.side=side;
		this.ticker=ticker;
	}
}