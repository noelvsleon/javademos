package optimization.maps13;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Random;

import optimization.maps03.DateTime;

public class Maps13{
	static int numMsgs=8000000; //8000000; //for 59s //120000
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		Client c=new Client();
		Server s=new Server();

		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/*
		 * Prev: non blocking server
			 * Client and Server are equal with 20s, after doubling input to 8000000, it's 43s each with the only hotspots being read and write
			 * Server buffer was empty 36972768 times whereas Client buffer was never full
		 *This:
			 * Added the FTSE100 to the client instead of just vod called RefData.syms
			 * Both Client and Serverr share this array, which enables us to send instruments as an index into this array, which makes it fixed width and simplifies the protocol. Stock Exchanges use the same system, where they disseminate the mappings from number to ticker/ISIN/etc every morning
			 * This required having 2 Trade classes, one for sending(index) and the other for receiving(String) 
			 * The sym and price now have a random element when we generate the data in the client
			 * Server now stores all the trades in an array of new MarketData objects.
			 * Can't use byte[] as key in HashMap as it will do .contains() on the object id, instead of the contents
			 * The MarketData class has an ArrayList to store trades, and keeps track of the trade with the highest price so far, and the one with the lowest price, under the premise that the systems purpose might be to disseminate this info in RT.
			 * Messages are now a static 30 bytes, so the Serverr ByteBuffer is set to that size so we don't have to deal with message fragments and compacting it.
			 * When the Serverr thread finishes it prints the last prices and the high and low trades per instrument(101 in FTSE100 today)
			 * Stripped out unnecessary stuff in Trade class leftover when when we were doing other things with it.
			 * 
			 * For 8000000 now a bit slower 59s vs 48s. Buffer issues C/S 0/47785195
			 * Seems 1% faster for 120000, the obvious discrepancy being we are keeping more data now.
			 * 
			 * 
			 * 
		 * Next times
		 	 * Multiple clients and switch focus to server optimisation
		 	 * If we have 8,000,000 trades and ~100 syms that thats roughly 80K trades per sym. So we could presize the Server ArrayLists at 100K as a reasonable guess, but it can always grow.
		 	 * We could switch the order of the if else branches in server
		 	 * We could remove the if else by prepopulating trades with MarketData objects containing fake high and low Trades with 0/MAX_VALUE prices
		 	 * We could drop the ticker component of Servers Trade class as it could be added to the MarketData class(1 instead of N). Still need to recieve it, and use it, just not store it. 
		 	 * We could play with the ByteBuffer size, and the TCP buffer sizes.
		 	 * We could change Serverrs Trade class to have ArrayLists of values instead of having a List of Trades.	
		 	 * ...
		 	 * UDP vs TCP
			 * 
			 * ???Multiple instruments, HashMap, then EnumMap
			 * OrderBook
		 * 
		 */
		c.join();
		s.join();
		double toSeconds=1000000000.;
		System.out.println("Client "+(endClient-startClient)/toSeconds);
		System.out.println("Server "+(endServer-startServer)/toSeconds);
	}
}

class RefData{
	static String[]syms=new String[]{"AAL","ABF","ADM","AHT","ANTO","AV.","AZN","BA.","BARC","BATS","BDEV","BKG","BLND","BLT","BNZL","BP.","BRBY","BT.A","CCH","CCL","CNA","CPG","CRDA","CRH","DCC","DGE","DLG","EVR","EXPN","EZJ","FERG","FRES","GLEN","GSK","GVC","HL.","HLMA","HSBA","IAG","IHG","III","IMB","INF","ITRK","ITV","JE.","JMAT","KGF","LAND","LGEN","LLOY","LSE","MCRO","MKS","MNDI","MRO","MRW","NG.","NMC","NXT","OCDO","PPB","PRU","PSN","PSON","RB.","RBS","RDSA","RDSB","REL","RIO","RMG","RMV","RR.","RRS","RSA","RTO","SBRY","SDR","SGE","SGRO","SHP","SKG","SKY","SLA","SMDS","SMIN","SMT","SN.","SSE","STAN","STJ","SVT","TSCO","TUI","TW.","ULVR","UU.","VOD","WPP","WTB"};
}

class Client extends Thread{
	ArrayList<TradeC>trades=new ArrayList<>(Maps13.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		Random rand=new Random();
		for(int i=0;i<Maps13.numMsgs;i++) {
			trades.add(new TradeC(
				now.plus(1),
				now2.plus(1),
				123.45+rand.nextInt(20),
				1000+i,
				0==i/2?'B':'S',
				(byte)rand.nextInt(RefData.syms.length)));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			SocketAddress a=new InetSocketAddress("localhost",3000);
			SocketChannel server=SocketChannel.open();
			server.connect(a);
			Maps13.startClient=System.nanoTime();
			server.configureBlocking(false);
			byte[]writeBufferA=new byte[40];
			ByteBuffer writeBuffer=ByteBuffer.wrap(writeBufferA);
			long timesClientBufferFull=0;
			long price;
			for(int i=Maps13.numMsgs-1;i>=0;i--) {
				TradeC t=trades.remove(i);
		      writeBufferA[0] = (byte)(t.exchTime.nanoseconds >>> 56);
		      writeBufferA[1] = (byte)(t.exchTime.nanoseconds >>> 48);
		      writeBufferA[2] = (byte)(t.exchTime.nanoseconds >>> 40);
		      writeBufferA[3] = (byte)(t.exchTime.nanoseconds >>> 32);
		      writeBufferA[4] = (byte)(t.exchTime.nanoseconds >>> 24);
		      writeBufferA[5] = (byte)(t.exchTime.nanoseconds >>> 16);
		      writeBufferA[6] = (byte)(t.exchTime.nanoseconds >>>  8);
		      writeBufferA[7] = (byte)(t.exchTime.nanoseconds >>>  0);
		      writeBufferA[8] = (byte)(t.time.nanoseconds >>> 56);
		      writeBufferA[9] = (byte)(t.time.nanoseconds >>> 48);
		      writeBufferA[10] = (byte)(t.time.nanoseconds >>> 40);
		      writeBufferA[11] = (byte)(t.time.nanoseconds >>> 32);
		      writeBufferA[12] = (byte)(t.time.nanoseconds >>> 24);
		      writeBufferA[13] = (byte)(t.time.nanoseconds >>> 16);
		      writeBufferA[14] = (byte)(t.time.nanoseconds >>>  8);
		      writeBufferA[15] = (byte)(t.time.nanoseconds >>>  0);
		      price=Double.doubleToLongBits(t.price);
		      writeBufferA[16] = (byte)(price >>> 56);
		      writeBufferA[17] = (byte)(price >>> 48);
		      writeBufferA[18] = (byte)(price >>> 40);
		      writeBufferA[19] = (byte)(price >>> 32);
		      writeBufferA[20] = (byte)(price >>> 24);
		      writeBufferA[21] = (byte)(price >>> 16);
		      writeBufferA[22] = (byte)(price >>>  8);
		      writeBufferA[23] = (byte)(price >>>  0);
		      writeBufferA[24]=(byte)((t.size >>> 24) & 0xFF);
		      writeBufferA[25]=(byte)((t.size >>> 16) & 0xFF);
		      writeBufferA[26]=(byte)((t.size >>> 8) & 0xFF);
		      writeBufferA[27]=(byte)((t.size >>> 0) & 0xFF);
		      writeBufferA[28]=(byte)t.side;
		      writeBufferA[29]=t.ticker;
		      writeBuffer.limit(30); //some annoying checks, could subclass it and break the encapsulation
		      server.write(writeBuffer);
		      while(0!=writeBuffer.remaining()) {
		      	++timesClientBufferFull;
		      	server.write(writeBuffer);
		      }
		      writeBuffer.clear();
			}
			Maps13.endClient=System.nanoTime();
			System.out.println("Client buffer full "+timesClientBufferFull);
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
interface fi{
	byte[] m();
}
class MarketData{
	final ArrayList<Trade> trades=new ArrayList<>();
	Trade high;
	Trade low;
	MarketData(Trade t){
		trades.add(t);
		high=t;
		low=t;
	}
	void newTrade(Trade t) {
		trades.add(t);
		if(t.price>high.price) {
			high=t;
		}else if (t.price<low.price) {
			low=t;
		}
	}
}
class Server extends Thread{
	final MarketData[]trades=new MarketData[RefData.syms.length];
	Server(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	@SuppressWarnings("static-access") public void run() {
		try{
			ServerSocketChannel listenSock = ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000));
			Maps13.startServer=System.nanoTime();
			SocketChannel client=listenSock.accept();
			client.configureBlocking(false);
			final ByteBuffer b=ByteBuffer.allocate(30); //TODO benchmark a bigger buffer
			int numMsgsRecv=0;
			int numBytesRead=0;
			byte tickerByte;
			Trade t;
			long timesServerBufferEmpty=0;
			while(numMsgsRecv<Maps13.numMsgs) {
				numBytesRead+=client.read(b);
				while(numBytesRead<30) { //messages are fixed size of 30
					++timesServerBufferEmpty;
					numBytesRead+=client.read(b);
				}
				b.flip();
				
				t=new Trade(
						new DateTime(b.getLong()),
						new DateTime(b.getLong()),
						b.getDouble(),
						b.getInt(),
						(char)b.get(),
						RefData.syms[tickerByte=b.get()]);
				if(trades[tickerByte]==null) { //TODO test branch ordering
					trades[tickerByte]=new MarketData(t);
				}else {
					trades[tickerByte].newTrade(t);
				}
				numBytesRead=0;
				++numMsgsRecv;
				b.clear();
			}
			Maps13.endServer=System.nanoTime();
			System.out.println("Server buffer empty "+timesServerBufferEmpty);
			for(int i=0;i<trades.length;++i) {
				MarketData md=trades[i];
				if(md==null) {
					System.out.println(RefData.syms[i]+" No trades");
				}else {
					System.out.printf("%s Count %6d Last %91s High %91s Low %91s\n",RefData.syms[i],md.trades.size(),md.trades.get(md.trades.size()-1),md.high,md.low);
				}
			}
			client.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}