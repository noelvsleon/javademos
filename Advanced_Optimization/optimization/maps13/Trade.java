package optimization.maps13;

import optimization.maps03.DateTime;

public class Trade extends TradeC{
	String ticker;
	public Trade(DateTime exchTime,DateTime time, double price, int size, char side, String ticker){
		this.exchTime=exchTime;
		this.time=time;
		this.price=price;
		this.size=size;
		this.side=side;
		this.ticker=ticker;
	}
	public String toString(){
		return ticker+","+exchTime+","+time+","+price+","+size+","+side;
	}
}