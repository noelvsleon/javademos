package optimization;
class C1{
	final int i=42;
}
class C2{
	int i=42;
}

public class Final_{
	public static long finalPrimitives(int iterations) {
		C1 c=new C1();
		int r=0;
		long before=System.nanoTime();
		for(int i=0;i<iterations;i++) {
			r+=c.i;
		}
		long after=System.nanoTime();
		return after-before;
	}
	public static long notFinalPrimitives(int iterations) {
		C2 c=new C2();
		int r=0;
		long before=System.nanoTime();
		for(int i=0;i<iterations;i++) {
			r+=c.i;
		}
		long after=System.nanoTime();
		return after-before;
	}
	public static void main(String[] args){
		//final vs not
		int iterations=2000000000;
		long time=0,time2=0;
		for(int i=0;i<5;i++) {
			time2+=notFinalPrimitives(iterations);
		}
		System.out.printf("Normal       %11d\n",time2);
		for(int i=0;i<5;i++) {
			time+=finalPrimitives(iterations);
		}
		System.out.printf("Final        %11d\n",time);
		System.out.printf("Normal/Final %11f\n",time2/(double)time);

	}

}
