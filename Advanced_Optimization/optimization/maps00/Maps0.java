package optimization.maps00;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Maps0{
	static int numMsgs=10000; // for 1.0x seconds
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");

		s.join();
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Server "+(endServer-startServer)/toSeconds);
		System.out.println("Client "+(endClient-startClient)/toSeconds);
	}
}
class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps0.numMsgs);;
	Client(){
		this.setName("Client");
		for(int i=0;i<Maps0.numMsgs;i++) {
			trades.add(new Trade(
				LocalDateTime.now().plusSeconds(i),
				LocalDateTime.now().plusSeconds(i+60),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps0.startClient=System.nanoTime();
			ObjectOutputStream server=new ObjectOutputStream(serverSock.getOutputStream());
			for(int i=0;i<Maps0.numMsgs;i++) {
				server.writeObject(trades.get(i));
			}
			server.flush();
			Maps0.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			ServerSocket listenSock=new ServerSocket(3000);
			Maps0.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			ObjectInputStream client=new ObjectInputStream(clientSock.getInputStream());
			for(int i=0;i<Maps0.numMsgs;i++) {
				client.readObject();
			}
			Maps0.endServer=System.nanoTime();
			listenSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
}