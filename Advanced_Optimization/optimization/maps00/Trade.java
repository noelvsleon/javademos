package optimization.maps00;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings("serial") public class Trade implements Comparable<Trade>,Serializable{
	LocalDateTime exchTime;
	LocalDateTime time;
	String ticker;
	double price;
	int size;
	char side;
	public Trade(LocalDateTime exchTime,LocalDateTime time, String ticker, double price, int size, char side){
		this.exchTime=exchTime;
		this.time=time;
		this.ticker=ticker;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public int compareTo(Trade t) {
		return time.compareTo(t.time);
	}
}