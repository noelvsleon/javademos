package optimization.maps09;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import optimization.maps03.DateTime;
import optimization.maps07.Trade;

public class Maps9{
	static int numMsgs=115000; //115000 for 1s //4000000; //for 24s
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/*
		 * Prev: 
			 * Client and Serverr take similar CPU time, Server takes 48s vs Client 26s. Although this was predictable from the fact the server is creating objects. We'll look at that as part of an expansion in functionality.
			 * Serverr gets to 1.2GB, Client gets to 95MB
			 * Main culprit is still byte[] which is 191MB for 1.7M
		 * 
		 * This time: blocking nio
		 * A lot faster ~2x
		 * 
		 * Next times
		 * 	Optimise
			 * non blocking
			 * look at enhancing bytebuffer, use wrap, and manually set the position, limit
			 * Could explore TCP buffer size
			 * UDP vs TCP
			 * Last trade for vod
			 * Last trade table
			 * Keeping all trades on the server ArrayList of Objects, Object of ArrayLists
			 * Multiple instruments, HashMap, then EnumMap
			 * OrderBook
			 * ticker -> test grabbing the bytes once instead of letting it do it repeatedly
		 * 
		 */
		c.join();
		s.join();
		double toSeconds=1000000000.;
		System.out.println("Client "+(endClient-startClient)/toSeconds);
		System.out.println("Server "+(endServer-startServer)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps9.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		for(int i=0;i<Maps9.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				123.45+i,
				1000+i,
				0==i/2?'B':'S',
				"vod"));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			SocketAddress a=new InetSocketAddress("localhost",3000);
			SocketChannel server=SocketChannel.open();
			server.connect(a);
			Maps9.startClient=System.nanoTime();
			ByteBuffer writeBuffer=ByteBuffer.allocate(40);
			
//			byte[]writeBuffer=new byte[30];
//			long price;
			for(int i=Maps9.numMsgs-1;i>=0;i--) {
				Trade t=trades.remove(i);
				writeBuffer.putLong(t.exchTime.nanoseconds);
				writeBuffer.putLong(t.time.nanoseconds);
				writeBuffer.putDouble(t.price);
				writeBuffer.putInt(t.size);
				writeBuffer.put((byte)t.side);
				writeBuffer.put((byte)t.ticker.length()); 
				writeBuffer.put(t.ticker.getBytes()); 
				
//		      writeBuffer[0] = (byte)(t.exchTime.nanoseconds >>> 56);
//		      writeBuffer[1] = (byte)(t.exchTime.nanoseconds >>> 48);
//		      writeBuffer[2] = (byte)(t.exchTime.nanoseconds >>> 40);
//		      writeBuffer[3] = (byte)(t.exchTime.nanoseconds >>> 32);
//		      writeBuffer[4] = (byte)(t.exchTime.nanoseconds >>> 24);
//		      writeBuffer[5] = (byte)(t.exchTime.nanoseconds >>> 16);
//		      writeBuffer[6] = (byte)(t.exchTime.nanoseconds >>>  8);
//		      writeBuffer[7] = (byte)(t.exchTime.nanoseconds >>>  0);
//		      writeBuffer[8] = (byte)(t.time.nanoseconds >>> 56);
//		      writeBuffer[9] = (byte)(t.time.nanoseconds >>> 48);
//		      writeBuffer[10] = (byte)(t.time.nanoseconds >>> 40);
//		      writeBuffer[11] = (byte)(t.time.nanoseconds >>> 32);
//		      writeBuffer[12] = (byte)(t.time.nanoseconds >>> 24);
//		      writeBuffer[13] = (byte)(t.time.nanoseconds >>> 16);
//		      writeBuffer[14] = (byte)(t.time.nanoseconds >>>  8);
//		      writeBuffer[15] = (byte)(t.time.nanoseconds >>>  0);
//		      price=Double.doubleToLongBits(t.price);
//		      writeBuffer[16] = (byte)(price >>> 56);
//		      writeBuffer[17] = (byte)(price >>> 48);
//		      writeBuffer[18] = (byte)(price >>> 40);
//		      writeBuffer[19] = (byte)(price >>> 32);
//		      writeBuffer[20] = (byte)(price >>> 24);
//		      writeBuffer[21] = (byte)(price >>> 16);
//		      writeBuffer[22] = (byte)(price >>>  8);
//		      writeBuffer[23] = (byte)(price >>>  0);
//		      //TODO price Double.toLongBits ???
//		      writeBuffer[24]=(byte)((t.size >>> 24) & 0xFF);
//		      writeBuffer[25]=(byte)((t.size >>> 16) & 0xFF);
//		      writeBuffer[26]=(byte)((t.size >>> 8) & 0xFF);
//		      writeBuffer[27]=(byte)((t.size >>> 0) & 0xFF);
//		      writeBuffer[28]=(byte)t.side;
//		      writeBuffer[29]=(byte)t.ticker.length();
//				writeBuffer.put(t.ticker.getBytes()); 
		      server.write(writeBuffer.flip());
		      writeBuffer.clear();
			}
			Maps9.endClient=System.nanoTime();
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
interface fi{
	String m();
}
class Serverr extends Thread{
//	ArrayList<Trade>trades=new ArrayList<>(Maps.numMsgs);;
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	@SuppressWarnings("static-access") public void run() {
		try{
			ServerSocketChannel listenSock = ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000));
//			@SuppressWarnings("resource") ServerSocket listenSock=new ServerSocket(3000);
			Maps9.startServer=System.nanoTime();
			SocketChannel client=listenSock.accept();
			final ByteBuffer b=ByteBuffer.allocate(40);
//			Socket clientSock=listenSock.accept();
//			DataInputStream client=new DataInputStream(clientSock.getInputStream());
//			byte[]b=new byte[1024];
			final byte[]ticker=new byte[10]; //new String will do an Arrays.copyof so we can have this one final
			long exchTimeLong,timeLong;
			double priceDouble;
			int sizeInt;
			char sideChar;
			byte tickerLengthByte;
			String tickerString;
			for(int i=0;i<Maps9.numMsgs;i++) {
				client.read(b); //TODO what if we don't have the whole message?
				b.flip();
				
//				exchTimeLong=b.getLong();
//				timeLong=b.getLong();
//				priceDouble=b.getDouble();
//				sizeInt=b.getInt();
//				sideChar=(char)b.get();
//				tickerLengthByte=b.get();
//				b.get(ticker,0,(int)tickerLengthByte);
//				tickerString=new String(ticker,0,tickerLengthByte);
				
				new Trade(
						new DateTime(b.getLong()),
						new DateTime(b.getLong()),
						b.getDouble(),
						b.getInt(),
						(char)b.get(),
						(new fi(){public String m(){byte tickerLengthByte=b.get();b.get(ticker,0,(int)tickerLengthByte);return new String(ticker,0,(int)tickerLengthByte);}}).m());
				b.compact();
			}
			Maps9.endServer=System.nanoTime();
			client.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}