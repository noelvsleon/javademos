package optimization.maps03;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Comparator;
public class Maps3{
	static int numMsgs=33000; //For 1.0s
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* We removed the gc call.
		* Created our own datetime class
		* Now we can do 33000 messages per second instead of 10000
		 */
		s.join();
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Server "+(endServer-startServer)/toSeconds);
		System.out.println("Client "+(endClient-startClient)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps3.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		for(int i=0;i<Maps3.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps3.startClient=System.nanoTime();
			ObjectOutputStream server=new ObjectOutputStream(serverSock.getOutputStream());
			for(int i=0;i<Maps3.numMsgs;i++) {
				server.writeObject(trades.get(i));
			}
			server.flush();
			Maps3.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			ServerSocket listenSock=new ServerSocket(3000);
			Maps3.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			ObjectInputStream client=new ObjectInputStream(clientSock.getInputStream());
			for(int i=0;i<Maps3.numMsgs;i++) {
				client.readObject();
			}
			Maps3.endServer=System.nanoTime();
			listenSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
}