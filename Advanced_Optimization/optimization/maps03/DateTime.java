package optimization.maps03;

import java.io.Serializable;
import java.time.Clock;
import java.time.Instant;

@SuppressWarnings("serial") public class DateTime implements Serializable{
	public long nanoseconds;
	static final long toSeconds=1000000000;
	static final long toMinutes=60*toSeconds;
	static final long toHours=60*toMinutes;
	static final long toDays=24*toHours;
	static final long toYears=365*toDays;
	static final Clock clock=Clock.systemDefaultZone();
	public DateTime(long nanoseconds){
		this.nanoseconds=nanoseconds;
	}
	public int compareTo(DateTime time){
		return nanoseconds>time.nanoseconds?1:nanoseconds<time.nanoseconds?-1:0;
	}
	public String toString() {
		return 
				(nanoseconds/toYears)
				+" years "+((nanoseconds%toYears)/toDays) //days
				+" days "+((nanoseconds%toDays)/toHours) //hours
				+":"+((nanoseconds%toHours)/toMinutes) //minutes
				+":"+((nanoseconds%toMinutes)/toSeconds) //seconds
				+"."+(nanoseconds%toSeconds); //nanos
	}
	public static DateTime now() {
		Instant now=clock.instant();
		return new DateTime((1000000000*now.getEpochSecond())+now.getNano());
	}
	public DateTime plus(long nanos) {
//		return new DateTime(nanoseconds+nanos); //if we wanted immutability
		nanoseconds+=nanos;
		return this;
	}
}