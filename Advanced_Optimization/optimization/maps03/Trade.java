package optimization.maps03;

import java.io.Serializable;

@SuppressWarnings("serial") public class Trade implements Comparable<Trade>,Serializable{
	public DateTime exchTime;
	public DateTime time;
	public String ticker;
	public double price;
	public int size;
	public char side;
	public Trade(DateTime exchTime,DateTime time, String ticker, double price, int size, char side){
		this.exchTime=exchTime;
		this.time=time;
		this.ticker=ticker;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public int compareTo(Trade t) {
		return time.compareTo(t.time);
	}
}