package optimization;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Loops{
	static int numTrades1=2000000;
	static final int numTrades2=2000000;
	static ArrayList<TradeC>tradeAL3=new ArrayList<TradeC>(numTrades2);
	static final ArrayList<TradeC>tradeAL5=new ArrayList<TradeC>(numTrades2);
	public static void main(String[] args){
		final int numTrades3=2000000;
		int numTrades4=2000000;
		ArrayList<TradeC>tradeAL=new ArrayList<TradeC>(numTrades2);
		ArrayList<TradeC>tradeAL2=new ArrayList<TradeC>(numTrades2);
		final ArrayList<TradeC>tradeAL4=new ArrayList<TradeC>(numTrades2);
		final ArrayList<TradeC>tradeAL6=new ArrayList<TradeC>(numTrades2);
		final ArrayList<TradeC>tradeAL7=new ArrayList<TradeC>(numTrades2);
		final ArrayList<TradeC>tradeAL8=new ArrayList<TradeC>(numTrades2);
		
		//inside loop
		long before,after;
		before=System.nanoTime();
		for(int i=0;i<numTrades4;i++) {
			tradeAL.add(new TradeC(LocalDateTime.now().plusSeconds(i),
					LocalDateTime.now().plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		long inloop=after-before;
		System.out.printf("Time inside  loop                   %11d\n",inloop);
		//pop AL2
		long before1=System.nanoTime();
		LocalDateTime now=LocalDateTime.now();
		before=System.nanoTime();
		for(int i=0;i<numTrades4;i++) {
			tradeAL2.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		long outloop=after-before1;
		System.out.printf("Time outside loop                   %11d excluding now %11d\n",outloop,after-before);
		System.out.printf("in/out                              %11f\n",inloop/(double)outloop);
		//static vs local collection
		before=System.nanoTime();
		for(int i=0;i<numTrades4;i++) {
			tradeAL3.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		long duration=after-before;
		System.out.printf("static vs local collection          %11d\n",duration);
		//static final collection
		before=System.nanoTime();
		for(int i=0;i<numTrades4;i++) {
			tradeAL5.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("static final collection             %11d\n",duration);
		//final collection
		before=System.nanoTime();
		for(int i=0;i<numTrades4;i++) {
			tradeAL4.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("final collection                    %11d\n",duration);
		//final collection final limit
		before=System.nanoTime();
		for(int i=0;i<numTrades3;i++) {
			tradeAL6.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("final collection final limit        %11d\n",duration);
		//final collection final limit
		before=System.nanoTime();
		for(int i=0;i<numTrades3;i++) {
			tradeAL8.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("final collection final limit        %11d\n",duration);
		//final collection static final limit
		before=System.nanoTime();
		for(int i=0;i<numTrades2;i++) {
			tradeAL7.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("final collection static final limit %11d\n",duration);
		/////////////////////sum
		//Iterate final limit
		long size=0;
		before=System.nanoTime();
		for(int i=0;i<numTrades2;i++) {
			size+=tradeAL7.get(i).size;
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("Iterate final limit                 %11d\n",duration);
		//Iterate method limit
		size=0;
		before=System.nanoTime();
		for(int i=0;i<tradeAL7.size();i++) {
			size+=tradeAL7.get(i).size;
		}
		after=System.nanoTime();
		long duration2=after-before;
		System.out.printf("Iterate method limit                %11d\n",duration2);
		System.out.printf("Iterate method/final                %11f\n",duration2/(double)duration);
	}
}
