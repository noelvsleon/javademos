package optimization.maps01;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import optimization.maps00.Trade;
public class Maps1{
	static int numMsgs=1200000; //Increase for VisualVM
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		//Debug(VisualVM)
		//????
		/*
		 * Sampler>CPU>Hotspots, Choose Threads main
		 * Resume
		 */
		
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* Sampler>CPU>Hotspots, Choose Threads main, Server, Client
		 * At the bottom, can see that the readObject, writeObject and because it is waiting main are the 3 methods that consume the most CPU time.
		 * java.time.LocalDateTime.now consumes 300ms on startup and the Client constructor <init> therefore takes 434ms
		 * similarly plusSeconds() takes 131ms
		 * Next we'll look at Maps2.java which moves the now() method call out of the loop.
		 * 
		 * Sampler>Memory>Per thread allocations>right click>filter>main,Client,Serverr>Enter
		 * we'll see Serverr and Client grow, when get to ~400MB each, click perform GC, no real effect
		 * switch to Heap histogram, top objects are:
		 * 	LocalDateTime 4.5M objects consuming 100MB similar for LocalDate, LocalTime
		 * 	Object[] for 289K for 89MB
		 * 	Trade for 2.2M for 89MB
		 * 	int[] 1189 for 56MB
		 * 	ObjectInputStream$HandleTable$HandleList[] 89 for 34MB
		 * 	byte[] 686K for 26M
		 */
		s.join();
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Server "+(endServer-startServer)/toSeconds);
		System.out.println("Client "+(endClient-startClient)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps1.numMsgs);;
	Client(){
		this.setName("Client");
		for(int i=0;i<Maps1.numMsgs;i++) {
			trades.add(new Trade(
				LocalDateTime.now().plusSeconds(i),
				LocalDateTime.now().plusSeconds(i+60),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps1.startClient=System.nanoTime();
			ObjectOutputStream server=new ObjectOutputStream(serverSock.getOutputStream());
			for(int i=0;i<Maps1.numMsgs;i++) {
				server.writeObject(trades.get(i));
			}
			server.flush();
			Maps1.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
//	ArrayList<Trade>trades=new ArrayList<>(Maps.numMsgs);;
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			ServerSocket listenSock=new ServerSocket(3000);
			Maps1.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			ObjectInputStream client=new ObjectInputStream(clientSock.getInputStream());
			for(int i=0;i<Maps1.numMsgs;i++) {
				client.readObject();
			}
			Maps1.endServer=System.nanoTime();
			listenSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
}