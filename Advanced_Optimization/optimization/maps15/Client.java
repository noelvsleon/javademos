package optimization.maps15;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import optimization.maps03.DateTime;
import optimization.maps13.TradeC;

class Client extends Thread{
	static String[]exchangeNames=new String[] {"NYSE","XLON","XETRA"};
	byte id;
	static final ArrayList<TradeC>trades=new ArrayList<>(Maps15.numMsgs);
	static final ArrayList<FillC>fills=new ArrayList<>(Maps15.numMsgs);
	static {
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		Random rand=new Random();
		for(int i=0;i<Maps15.numMsgs;i++) {
			
			trades.add(new TradeC(
				now.plus(1),
				now2.plus(1),
				123.45+rand.nextInt(20),
				1000+i,
				0==i/2?'B':'S',
				(byte)rand.nextInt(RefData.syms.length)));
			if((i%20)==0) {
				fills.add(i,new FillC(trades.get(i),(short)rand.nextInt(15)));
				++Maps15.numMsgsIncFills;
			}else {
				fills.add(null);
			}
		}
	}
	Client(int id){
		this.name=exchangeNames[id];
		this.id=(byte)id;
		this.setName("Client "+name);
	}
	long startClient=0;
	long endClient;
	String name;
	public void run() {
		try{
			ServerSocketChannel listenSock = ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000+id));
			startClient=System.nanoTime();
			SocketChannel server=listenSock.accept();
			server.configureBlocking(false);

			byte[]writeBufferA=new byte[40];
			ByteBuffer writeBuffer=ByteBuffer.wrap(writeBufferA);
			long timesClientBufferFull=0;
			long price;
			for(int i=Maps15.numMsgs-1;i>=0;i--) {
				TradeC t=trades.get(i);
				writeBufferA[0] = 0x00; //msgtype 0=Trade, 1=Fill
		      writeBufferA[1] = (byte)(t.exchTime.nanoseconds >>> 56);
		      writeBufferA[2] = (byte)(t.exchTime.nanoseconds >>> 48);
		      writeBufferA[3] = (byte)(t.exchTime.nanoseconds >>> 40);
		      writeBufferA[4] = (byte)(t.exchTime.nanoseconds >>> 32);
		      writeBufferA[5] = (byte)(t.exchTime.nanoseconds >>> 24);
		      writeBufferA[6] = (byte)(t.exchTime.nanoseconds >>> 16);
		      writeBufferA[7] = (byte)(t.exchTime.nanoseconds >>>  8);
		      writeBufferA[8] = (byte)(t.exchTime.nanoseconds >>>  0);
		      writeBufferA[9] = (byte)(t.time.nanoseconds >>> 56);
		      writeBufferA[10] = (byte)(t.time.nanoseconds >>> 48);
		      writeBufferA[11] = (byte)(t.time.nanoseconds >>> 40);
		      writeBufferA[12] = (byte)(t.time.nanoseconds >>> 32);
		      writeBufferA[13] = (byte)(t.time.nanoseconds >>> 24);
		      writeBufferA[14] = (byte)(t.time.nanoseconds >>> 16);
		      writeBufferA[15] = (byte)(t.time.nanoseconds >>>  8);
		      writeBufferA[16] = (byte)(t.time.nanoseconds >>>  0);
		      price=Double.doubleToLongBits(t.price);
		      writeBufferA[17] = (byte)(price >>> 56);
		      writeBufferA[18] = (byte)(price >>> 48);
		      writeBufferA[19] = (byte)(price >>> 40);
		      writeBufferA[20] = (byte)(price >>> 32);
		      writeBufferA[21] = (byte)(price >>> 24);
		      writeBufferA[22] = (byte)(price >>> 16);
		      writeBufferA[23] = (byte)(price >>>  8);
		      writeBufferA[24] = (byte)(price >>>  0);
		      writeBufferA[25]=(byte)((t.size >>> 24) & 0xFF);
		      writeBufferA[26]=(byte)((t.size >>> 16) & 0xFF);
		      writeBufferA[27]=(byte)((t.size >>> 8) & 0xFF);
		      writeBufferA[28]=(byte)((t.size >>> 0) & 0xFF);
		      writeBufferA[29]=(byte)t.side;
		      writeBufferA[30]=t.ticker;
		      writeBuffer.limit(31); //some annoying checks, could subclass it and break the encapsulation
		      server.write(writeBuffer);
		      while(0!=writeBuffer.remaining()) {
		      	++timesClientBufferFull;
		      	server.write(writeBuffer);
		      }
		      if(fills.get(i)!=null) {
			      writeBuffer.position(0);
			      FillC f=fills.get(i);
			      writeBufferA[0] = 0x01; //msgtype 0=Trade, 1=Fill
			      writeBufferA[1] =(byte)(f.exchTime.nanoseconds >>> 56);
			      writeBufferA[2] =(byte)(f.exchTime.nanoseconds >>> 48);
			      writeBufferA[3] =(byte)(f.exchTime.nanoseconds >>> 40);
			      writeBufferA[4] =(byte)(f.exchTime.nanoseconds >>> 32);
			      writeBufferA[5] =(byte)(f.exchTime.nanoseconds >>> 24);
			      writeBufferA[6] =(byte)(f.exchTime.nanoseconds >>> 16);
			      writeBufferA[7] =(byte)(f.exchTime.nanoseconds >>>  8);
			      writeBufferA[8] =(byte)(f.exchTime.nanoseconds >>>  0);
			      writeBufferA[9] =(byte)(f.time.nanoseconds >>> 56);
			      writeBufferA[10] =(byte)(f.time.nanoseconds >>> 48);
			      writeBufferA[11]=(byte)(f.time.nanoseconds >>> 40);
			      writeBufferA[12]=(byte)(f.time.nanoseconds >>> 32);
			      writeBufferA[13]=(byte)(f.time.nanoseconds >>> 24);
			      writeBufferA[14]=(byte)(f.time.nanoseconds >>> 16);
			      writeBufferA[15]=(byte)(f.time.nanoseconds >>>  8);
			      writeBufferA[16]=(byte)(f.time.nanoseconds >>>  0);
			      price=Double.doubleToLongBits(f.price);
			      writeBufferA[17]=(byte)(price >>> 56);
			      writeBufferA[18]=(byte)(price >>> 48);
			      writeBufferA[19]=(byte)(price >>> 40);
			      writeBufferA[20]=(byte)(price >>> 32);
			      writeBufferA[21]=(byte)(price >>> 24);
			      writeBufferA[22]=(byte)(price >>> 16);
			      writeBufferA[23]=(byte)(price >>>  8);
			      writeBufferA[24]=(byte)(price >>>  0);
			      writeBufferA[25]=(byte)((f.size >>> 24) & 0xFF);
			      writeBufferA[26]=(byte)((f.size >>> 16) & 0xFF);
			      writeBufferA[27]=(byte)((f.size >>> 8) & 0xFF);
			      writeBufferA[28]=(byte)((f.size >>> 0) & 0xFF);
			      writeBufferA[29]=(byte)f.side;
			      writeBufferA[30]=f.ticker;
			      writeBufferA[31]=(byte)((f.cp >>> 8) & 0xFF);
			      writeBufferA[32]=(byte)((f.cp >>> 0) & 0xFF);
			      writeBuffer.limit(33); //some annoying checks, could subclass it and break the encapsulation
			      server.write(writeBuffer);
			      while(0!=writeBuffer.remaining()) {
			      	++timesClientBufferFull;
			      	server.write(writeBuffer);
			      }
		      }		      
		      writeBuffer.clear();
			}
			endClient=System.nanoTime();
			System.out.println(Now.n()+"Client "+name+" buffer full "+timesClientBufferFull);
			System.out.println(Now.n()+"Client "+name+" time "+(endClient-startClient)/1000000000.);
			server.configureBlocking(true);
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}