package optimization.maps15;

import java.util.ArrayList;

import optimization.maps03.DateTime;
import optimization.maps13.Trade;
import optimization.maps13.TradeC;

public class DataStructures{

}
class ExchangeTrade{
	Trade t;
	int exchange;
	ExchangeTrade(Trade t,int exchange){
		this.t=t;
		this.exchange=exchange;
	}
}
class PumpAndDumps{
        final int exchId;
        final int startFillId;
        final int endFillId;
	PumpAndDumps(int exchId,int startFillId,int endFillId){
		this.exchId=exchId;
		this.startFillId=startFillId;
		this.endFillId=endFillId;
	}
}
class MarketData{
	@SuppressWarnings("unchecked") final ArrayList<Trade>[]trades=new ArrayList[Maps15.numClients];
	@SuppressWarnings("unchecked") final ArrayList<Fill>[]fills=new ArrayList[Maps15.numClients];
	{
		for(int i=0;i<trades.length;++i) {
			trades[i]=new ArrayList<Trade>();
			fills[i]=new ArrayList<Fill>();
		}
	}
	final ExchangeTrade high;
	final ExchangeTrade low;
	int[] cumShares=new int[Maps15.numClients];
	double[] cumNotional=new double[Maps15.numClients];
	final ExchangeTrade largest;
	final ArrayList<PumpAndDumps>pumpAndDumps=new ArrayList<>();
	MarketData(Trade t,int exchId){
		trades[exchId].add(t);
		fills[exchId].add(null);
		high=new ExchangeTrade(t,exchId);
		low=new ExchangeTrade(t,exchId);
		largest=new ExchangeTrade(t,exchId);
	}
	void newTrade(Trade t,int exchId) {
		fills[exchId].add(null); //Keep fills and trades in sync, this is the easy way, TODO optimise later
		trades[exchId].add(t);
		if(t.price>high.t.price) {
			high.t=t;
			high.exchange=exchId;
		}else if (t.price<low.t.price) {
			low.t=t;
			low.exchange=exchId;
		}
		cumShares[exchId]+=t.size;
		cumNotional[exchId]+=t.size*t.price;
		if(largest.t==null||(largest.t.size*largest.t.price)<(t.size*t.price)) {
			largest.t=t;
			largest.exchange=exchId;
		}
	}
	void pumpAndDumpCheck(int exchId) { //Only doing it per exchange
		int currentFillIndex=fills[exchId].size()-1;
		int indexOfLastOppositeSide=currentFillIndex;
		Fill currentFill=fills[exchId].get(currentFillIndex);
//		char oppositeSide=currentFill.side=='B'?'S':'B';
		int sequenceLength=0;
		while(indexOfLastOppositeSide>0) {
			Fill f=fills[exchId].get(--indexOfLastOppositeSide);
			if(f!=null) {
				if(f.side==currentFill.side) {
					++sequenceLength;
				}else {
					break;
				}
			}
		}
		if(sequenceLength>=10) {
			pumpAndDumps.add(new PumpAndDumps(exchId,indexOfLastOppositeSide,currentFillIndex));
		}
	}
	void newFill(Fill f,int exchId) {
		fills[exchId].set(trades[exchId].size()-1,f);
		pumpAndDumpCheck(exchId);
	}
}
class FillC extends TradeC{
	final short cp;
	//Not sure how exchanges would do counterparties, so we'll use shorts similar to how they do syms
	FillC(DateTime exchTime,DateTime time, double price, int size, char side,byte ticker,short cp){
		super(exchTime,time,price,size,side,ticker);
		this.cp=cp;
	}
	FillC(TradeC t,short cp){
		super(t.exchTime,t.time,t.price,t.size,t.side,t.ticker);
		this.cp=cp;
	}
}
class Fill extends Trade{
	short cp;
	//Not sure how exchanges would do counterparties, so we'll use shorts similar to how they do syms
	Fill(DateTime exchTime,DateTime time, double price, int size, char side,String ticker,short cp){
		super(exchTime,time,price,size,side,ticker);
		this.cp=cp;
	}
}