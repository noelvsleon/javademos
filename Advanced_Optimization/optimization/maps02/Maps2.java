package optimization.maps02;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import optimization.maps00.Trade;
public class Maps2{
	static int numMsgs=1200000; //Increase for VisualVM
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		//Debug(VisualVM)
		//????
		/*
		 * Sampler>CPU>Hotspots, Choose Threads main
		 * Resume
		 */
		
		Client c=new Client();
		Serverr s=new Serverr();
		
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* Sampler>CPU>Hotspots, Choose Threads main, Serverr, Client
		 * At the bottom, can see that the readObject, writeObject and because it is waiting main are the 3 methods that consume the most CPU time.
		 * We moved the now() method call out of the loop, and it no longer appears in the hotspot list
		 * plusSeconds() takes a similar time to before
		 * Client constructor <init> is much quicker
		 * 
		 * We added a call to System.gc() which helps the Per thread allocations stays under 1MB, but the Monitor chart has the CPU and GC go nuts, and it doesn't help with the java.time stuff
		 * Sampler>Memory>Per thread allocations>right click>filter>main,Client,Serverr>Enter
		 * 
		 * Maps3 will remove the gc call.
		 */
		s.join();
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Server "+(endServer-startServer)/toSeconds);
		System.out.println("Client "+(endClient-startClient)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps2.numMsgs);;
	Client(){
		this.setName("Client");
		LocalDateTime now=LocalDateTime.now();
		for(int i=0;i<Maps2.numMsgs;i++) {
			trades.add(new Trade(
				now.plusSeconds(i),
				now.plusSeconds(i+60),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			Maps2.startClient=System.nanoTime();
			ObjectOutputStream server=new ObjectOutputStream(serverSock.getOutputStream());
			for(int i=0;i<Maps2.numMsgs;i++) {
				server.writeObject(trades.get(i));
			}
			server.flush();
			Maps2.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Serverr extends Thread{
//	ArrayList<Trade>trades=new ArrayList<>(Maps.numMsgs);;
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			ServerSocket listenSock=new ServerSocket(3000);
			Maps2.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			ObjectInputStream client=new ObjectInputStream(clientSock.getInputStream());
			for(int i=0;i<Maps2.numMsgs;i++) {
				client.readObject();
				System.gc();
			}
			Maps2.endServer=System.nanoTime();
			listenSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
}