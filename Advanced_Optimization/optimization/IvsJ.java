package optimization;

public class IvsJ{
	static int maxi,maxj;
	static{maxi=maxj=20000;}
	static int[][]a=new int[maxi][maxj];
	static int sum1(int maxi,int maxj) {
		int total=0;
		for(int i=0;i<maxi;++i) {
			for(int j=0;j<maxj;++j) {
				total+=a[i][j];
			}
		}
		return total;
	}
	static int sum2(int maxi,int maxj) {
		int total=0;
		for(int i=0;i<maxi;++i) {
			for(int j=0;j<maxj;++j) {
				total+=a[j][i];
			}
		}
		return total;
	}
	public static void main(String[] args){
		System.out.println("\n\n===================================================\n===================================================\n===================================================\n\n");

		for(int i=0;i<maxi;++i) {
			for(int j=0;j<maxj;++j) {
				a[i][j]=i*j;
			}
		}
		long before,after;
		
		for(int max:new int[]{10,100,1000,10000,20000}) {
			System.out.println("######Max "+max);
			before=System.nanoTime();
			int res=sum1(max,max);
			after=System.nanoTime();
			long duration=after-before;
			System.out.println("######"+res+" "+duration);
	
			before=System.nanoTime();
			res=sum2(max,max);
			after=System.nanoTime();
			System.out.println("######"+res+" "+(after-before));
			System.out.println("######"+(after-before)/(double)duration);
		}
	}
}
