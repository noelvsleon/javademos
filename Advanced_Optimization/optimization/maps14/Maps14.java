package optimization.maps14;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;

import optimization.maps03.DateTime;
import optimization.maps13.Trade;
import optimization.maps13.TradeC;

public class Maps14{
	static int numMsgs=4000000; //4000000; for 51s //120000
	static int numClients=3;
	static int numServerMsgs=numClients*numMsgs;
	static Long startServer,endServer;
	public static void main(String[] args) throws InterruptedException{
		Client[] c=new Client[numClients];
		Server s=new Server();
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		System.out.println(Now.n()+"Choose Thread Server, Client too");
		for(int i=0;i<c.length;++i) {
			c[i]=new Client(i);
			c[i].start();
		}
		/*
		 * Prev: non blocking server
			 * For 8000000 now a bit slower 59s vs 48s. Buffer issues C/S 0/47785195
			 * Seems 1% faster for 120000, the obvious discrepancy being we are keeping more data now.
		 *This:
			 * Multiple clients and switch focus to server optimisation
			 * Server now handles variable(but not arbitrary) number of clients
			 * Both Clients and Servers now have Buffer issues, so we can now look at optimising the Server
			 * System/Server now handles 3/1.5x more messages in the same 1s
			 * For large inputs we run out of Memory, but if we change it from 8M to 4M*3*2 it takes 50s which is a 3x increase for 18% less time
			 * Added timestamps to logging
			 * 
			 * Profile -> show CPU, if you expand the threads they show hotspots per thread, whereas bottom box is aggregate
			 * Sever reading dominates, newTrade only accounts for 37ms outof 44659ms
			 * Clients write is the only hotspot
			 * Client run method took 4.6s itself whereas total time including children was 142829
			 * Similarly server.run took 3.2s out of 53
			 * DateTime 24M objects consuming 576MB, there are 12M Trade objects and 4M TradeC. Each DateTime is 16byte header+8bytes long=24bytes
			 * 
		 * Next times
		 	 * The processing logic is still dominated by the read/write, so get the server to do more work
		 	 * Have server keep track of the cumulative number of shares traded, cumulative notional, the largest trade, 
		 	 * Rename the clients to be NYSE, LSE, XETRA, add Fill messages too(Trades with a counterparty column, which are a subset(5%) of the Trades)
		 	 * Server then needs to keep track of both Fills and Trades, and store the NYSE, LSE, XETRA seperately, all the stats now need to keep track of which exchange had the highest price etc, and cumulatives per exchange
		 	 * Server detect pump and dump keep track of sequential buy Fills followed by a sell where the Trade price moved progressively higher(min increase of 5%) (70% of Fills at a price higher than the average of the last 3), the sell is then for at least 70% of the sum of the buys, and the Trades show the price deflating over the next 10 trades to at least 2.5% of its peak   
		 	 * 
		 	 * Remove DateTime objects and store only the longs, then modify Trade toString method to use DateTime toString 
		 	 * If we have 8,000,000 trades and ~100 syms that thats roughly 80K trades per sym. So we could presize the Server ArrayLists at 100K as a reasonable guess, but it can always grow.
		 	 * We could switch the order of the if else branches in server
		 	 * We could remove the if else by prepopulating trades with MarketData objects containing fake high and low Trades with 0/MAX_VALUE prices
		 	 * We could drop the ticker component of Servers Trade class as it could be added to the MarketData class(1 instead of N). Still need to recieve it, and use it, just not store it. 
		 	 * We could play with the ByteBuffer size, and the TCP buffer sizes.
		 	 * We could change Serverrs Trade class to have ArrayLists of values instead of having a List of Trades.	
		 	 * Selector
		 	 * ...
		 	 * UDP vs TCP
			 * 
			 * ???Multiple instruments, HashMap, then EnumMap
			 * OrderBook
		 * 
		 */
		for(int i=0;i<c.length;++i) {
			c[i].join();
		}
		s.join();
		double toSeconds=1000000000.;
		System.out.println(Now.n()+"Server "+(endServer-startServer)/toSeconds);
	}
}

class RefData{
	static String[]syms=new String[]{"AAL","ABF","ADM","AHT","ANTO","AV.","AZN","BA.","BARC","BATS","BDEV","BKG","BLND","BLT","BNZL","BP.","BRBY","BT.A","CCH","CCL","CNA","CPG","CRDA","CRH","DCC","DGE","DLG","EVR","EXPN","EZJ","FERG","FRES","GLEN","GSK","GVC","HL.","HLMA","HSBA","IAG","IHG","III","IMB","INF","ITRK","ITV","JE.","JMAT","KGF","LAND","LGEN","LLOY","LSE","MCRO","MKS","MNDI","MRO","MRW","NG.","NMC","NXT","OCDO","PPB","PRU","PSN","PSON","RB.","RBS","RDSA","RDSB","REL","RIO","RMG","RMV","RR.","RRS","RSA","RTO","SBRY","SDR","SGE","SGRO","SHP","SKG","SKY","SLA","SMDS","SMIN","SMT","SN.","SSE","STAN","STJ","SVT","TSCO","TUI","TW.","ULVR","UU.","VOD","WPP","WTB"};
}

class Client extends Thread{
	static ArrayList<TradeC>trades=new ArrayList<>(Maps14.numMsgs);
	static {
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		Random rand=new Random();
		for(int i=0;i<Maps14.numMsgs;i++) {
			trades.add(new TradeC(
				now.plus(1),
				now2.plus(1),
				123.45+rand.nextInt(20),
				1000+i,
				0==i/2?'B':'S',
				(byte)rand.nextInt(RefData.syms.length)));
		}
	}
	Client(int i){
		this.id=i;
		this.setName("Client "+i);
	}
	long startClient;
	long endClient;
	int id;
	public void run() {
		try{
			SocketAddress a=new InetSocketAddress("localhost",3000);
			SocketChannel server=SocketChannel.open();
			server.connect(a);
			startClient=System.nanoTime();
			server.configureBlocking(false);
			byte[]writeBufferA=new byte[40];
			ByteBuffer writeBuffer=ByteBuffer.wrap(writeBufferA);
			long timesClientBufferFull=0;
			long price;
			for(int i=Maps14.numMsgs-1;i>=0;i--) {
				TradeC t=trades.get(i);
		      writeBufferA[0] = (byte)(t.exchTime.nanoseconds >>> 56);
		      writeBufferA[1] = (byte)(t.exchTime.nanoseconds >>> 48);
		      writeBufferA[2] = (byte)(t.exchTime.nanoseconds >>> 40);
		      writeBufferA[3] = (byte)(t.exchTime.nanoseconds >>> 32);
		      writeBufferA[4] = (byte)(t.exchTime.nanoseconds >>> 24);
		      writeBufferA[5] = (byte)(t.exchTime.nanoseconds >>> 16);
		      writeBufferA[6] = (byte)(t.exchTime.nanoseconds >>>  8);
		      writeBufferA[7] = (byte)(t.exchTime.nanoseconds >>>  0);
		      writeBufferA[8] = (byte)(t.time.nanoseconds >>> 56);
		      writeBufferA[9] = (byte)(t.time.nanoseconds >>> 48);
		      writeBufferA[10] = (byte)(t.time.nanoseconds >>> 40);
		      writeBufferA[11] = (byte)(t.time.nanoseconds >>> 32);
		      writeBufferA[12] = (byte)(t.time.nanoseconds >>> 24);
		      writeBufferA[13] = (byte)(t.time.nanoseconds >>> 16);
		      writeBufferA[14] = (byte)(t.time.nanoseconds >>>  8);
		      writeBufferA[15] = (byte)(t.time.nanoseconds >>>  0);
		      price=Double.doubleToLongBits(t.price);
		      writeBufferA[16] = (byte)(price >>> 56);
		      writeBufferA[17] = (byte)(price >>> 48);
		      writeBufferA[18] = (byte)(price >>> 40);
		      writeBufferA[19] = (byte)(price >>> 32);
		      writeBufferA[20] = (byte)(price >>> 24);
		      writeBufferA[21] = (byte)(price >>> 16);
		      writeBufferA[22] = (byte)(price >>>  8);
		      writeBufferA[23] = (byte)(price >>>  0);
		      writeBufferA[24]=(byte)((t.size >>> 24) & 0xFF);
		      writeBufferA[25]=(byte)((t.size >>> 16) & 0xFF);
		      writeBufferA[26]=(byte)((t.size >>> 8) & 0xFF);
		      writeBufferA[27]=(byte)((t.size >>> 0) & 0xFF);
		      writeBufferA[28]=(byte)t.side;
		      writeBufferA[29]=t.ticker;
		      writeBuffer.limit(30); //some annoying checks, could subclass it and break the encapsulation
		      server.write(writeBuffer);
		      while(0!=writeBuffer.remaining()) {
		      	++timesClientBufferFull;
		      	server.write(writeBuffer);
		      }
		      writeBuffer.clear();
			}
			endClient=System.nanoTime();
			System.out.println(Now.n()+"Client "+id+" buffer full "+timesClientBufferFull);
			System.out.println(Now.n()+"Client "+id+" time "+(endClient-startClient)/1000000000.);
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
interface fi{
	byte[] m();
}
class MarketData{
	final ArrayList<Trade> trades=new ArrayList<>();
	Trade high;
	Trade low;
	MarketData(Trade t){
		trades.add(t);
		high=t;
		low=t;
	}
	void newTrade(Trade t) {
		trades.add(t);
		if(t.price>high.price) {
			high=t;
		}else if (t.price<low.price) {
			low=t;
		}
	}
}
class ExchangeConnection{
	final ByteBuffer b=ByteBuffer.allocate(30); //TODO benchmark a bigger buffer
	int numBytesRead=0;
	byte tickerByte;
	Trade t;
	final SocketChannel client;
	ExchangeConnection(SocketChannel client){
		this.client=client;
	}
	void receiveMsg() throws IOException{
			numBytesRead+=client.read(b);
			if(numBytesRead<30) { //messages are fixed size of 30
				++Server.timesServerBufferEmpty;
				return;
			}
			b.flip();
			
			t=new Trade(
					new DateTime(b.getLong()),
					new DateTime(b.getLong()),
					b.getDouble(),
					b.getInt(),
					(char)b.get(),
					RefData.syms[tickerByte=b.get()]);
			if(Server.trades[tickerByte]==null) { //TODO test branch ordering
				Server.trades[tickerByte]=new MarketData(t);
			}else {
				Server.trades[tickerByte].newTrade(t);
			}
			numBytesRead=0;
			b.clear();
			++Server.numMsgsRecv;
	}
}
class Server extends Thread{
	static final MarketData[]trades=new MarketData[RefData.syms.length];
	static long timesServerBufferEmpty=0;
	static int numMsgsRecv=0;
	Server(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			ServerSocketChannel listenSock = ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000));
			Maps14.startServer=System.nanoTime();
			SocketChannel[]client=new SocketChannel[Maps14.numClients];
			ExchangeConnection[]b=new ExchangeConnection[Maps14.numClients];
			for(int i=0;i<Maps14.numClients;++i) {
				client[i]=listenSock.accept();
				client[i].configureBlocking(false);
				b[i]=new ExchangeConnection(client[i]);
			}
			while(numMsgsRecv<Maps14.numServerMsgs){
				for(ExchangeConnection e:b) {
					e.receiveMsg();
				}
			}
			Maps14.endServer=System.nanoTime();
			System.out.println(Now.n()+"Server buffer empty "+timesServerBufferEmpty+" Num trades "+trades.length);
			for(int i=0;i<trades.length;++i) {
				MarketData md=trades[i];
				if(md==null) {
					System.out.println(Now.n()+RefData.syms[i]+" No trades");
				}else {
//					System.out.printf("%s Count %6d Last %91s High %91s Low %91s\n",RefData.syms[i],md.trades.size(),md.trades.get(md.trades.size()-1),md.high,md.low);
				}
			}
			for(SocketChannel s:client) {
				s.close();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Now{
	static String n() {
		return LocalDateTime.now()+" ";
	}
}