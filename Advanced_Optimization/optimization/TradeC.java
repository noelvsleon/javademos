package optimization;

import java.time.LocalDateTime;

class TradeC implements Comparable<TradeC>,Cloneable{
	LocalDateTime exchTime;
	LocalDateTime time;
	String ticker;
	double price;
	int size;
	char side;
	TradeC(LocalDateTime exchTime,LocalDateTime time, String ticker, double price, int size, char side){
		this.exchTime=exchTime;
		this.time=time;
		this.ticker=ticker;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public int compareTo(TradeC t) {
		return time.compareTo(t.time);
	}
	public boolean equals(Object t) {
		return this.time.equals((LocalDateTime)((TradeC)t).time);
	}
   protected Object clone() throws CloneNotSupportedException {
      return super.clone();
  }
}