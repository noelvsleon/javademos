package optimization;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.TreeSet;

public class ALvsLLvsTS{
	static int numTrades=1000000;
	static ArrayList<TradeC>tradeAL=new ArrayList<TradeC>(numTrades);
	static ArrayList<TradeC>tradeAL2=new ArrayList<TradeC>(numTrades);
	static LinkedList<TradeC>tradeLL=new LinkedList<TradeC>();
	static LinkedList<TradeC>tradeLL2=new LinkedList<TradeC>();
	static TreeSet<TradeC>tradeTS=new TreeSet<TradeC>();
	static long before,after;
	static LocalDateTime now=LocalDateTime.now();
	static int nearStart,nearEnd,nearMiddle,offset,numInserts;
	static TradeC trade;
	public static void populate() {
		before=System.nanoTime();
		for(int i=0;i<numTrades;i++) {
			tradeAL.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		long alpop=after-before;
		System.out.printf("AL 1M trades pop %11d\n",alpop);
		
		//pop LL
		before=System.nanoTime();
		for(int i=0;i<numTrades;i++) {
			tradeLL.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		long llpop=after-before;
		System.out.printf("LL 1M trades pop %11d\n",llpop);

		before=System.nanoTime();
		long duration=0,before2,after2;
		for(int i=0;i<numTrades;i++) {
			TradeC t=new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B');
			before2=System.nanoTime();
			tradeAL2.add(t);
			after2=System.nanoTime();
			duration+=after2-before2;
		}
		after=System.nanoTime();
		System.out.printf("AL duration      %11d\n",duration);
		alpop=after-before;
		System.out.printf("AL 1M trades pop %11d\n",alpop);
		
		long duration2=0;
		before=System.nanoTime();
		for(int i=0;i<numTrades;i++) {
			TradeC t=new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B');
			before2=System.nanoTime();
			tradeLL2.add(t);
			after2=System.nanoTime();
			duration2+=after2-before2;
		}
		after=System.nanoTime();
		llpop=after-before;
		System.out.printf("LL duration      %11d\n",duration2);
		System.out.printf("LL/AL duration   %11f\n",duration2/(double)duration);
		System.out.printf("LL 1M trades pop %11d\n",llpop);

		System.out.printf("LL/AL %11f\n",llpop/(double)alpop);
		
		before=System.nanoTime();
		for(int i=0;i<numTrades;i++) {
			tradeTS.add(new TradeC(now.plusSeconds(i),
					now.plusSeconds(i+60),
					"vod",
					123.45+i,
					1000+i,
					'B'));
		}
		after=System.nanoTime();
		long tspop=after-before;
		System.out.printf("TS 1M trades pop %11d\n",tspop);
		System.out.printf("TS/LL %11f\n",tspop/(double)llpop);
	}
	public static void insert() throws CloneNotSupportedException {
		offset=10;
		nearStart=offset;
		nearEnd=numTrades-offset;
		nearMiddle=(numTrades/2)-offset;
		numInserts=100;
		trade=new TradeC(now.plusSeconds(1000000),
				now.plusSeconds(1000000+60),
				"vod",
				123.45+1000000,
				1000+1000000,
				'B');
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeAL.add(nearEnd+i,trade);
		}
		after=System.nanoTime();
		long alinsertNE=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeAL.add(nearMiddle+i,trade);
		}
		after=System.nanoTime();
		long alinsertNM=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeAL.add(nearStart+i,trade);
		}
		after=System.nanoTime();
		long alinsertNS=after-before;
		
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeLL.add(nearEnd+i,trade);
		}
		after=System.nanoTime();
		long llinsertNE=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeLL.add(nearMiddle+i,trade);
		}
		after=System.nanoTime();
		long llinsertNM=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeLL.add(nearStart+i,trade);
		}
		after=System.nanoTime();
		long llinsertNS=after-before;
		System.out.printf("AL Insert Near Start %11d Middle %11d End %11d\n",alinsertNS,alinsertNM,alinsertNE);
		System.out.printf("LL Insert Near Start %11d Middle %11d End %11d\n",llinsertNS,llinsertNM,llinsertNE);
		System.out.printf("Start AL/LL %11f Middle LL/AL %11f End LL/AL %11f\n",alinsertNS/(double)llinsertNS,llinsertNM/(double)alinsertNM,alinsertNE/(double)llinsertNE);
		
		TradeC t=tradeAL.get(nearStart);
		TradeC newt;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeTS.add(newt);
		}
		after=System.nanoTime();
		long tsinsertNSm=after-before; //m for mutate
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeLL.add(newt);
		}
		after=System.nanoTime();
		long llinsertNSm=after-before; //m for mutate
		System.out.printf("LL 1M trades insert NS %11d\n",llinsertNSm);
		System.out.printf("TS 1M trades insert NS %11d\n",tsinsertNSm);
		System.out.printf("TS/LL %11f\n",tsinsertNSm/(double)llinsertNSm);
		
		t=tradeAL.get(nearMiddle);
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeTS.add(newt);
		}
		after=System.nanoTime();
		long tsinsertNMm=after-before; //m for mutate
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeLL.add(newt);
		}
		after=System.nanoTime();
		long llinsertNMm=after-before; //m for mutate
		System.out.printf("LL 1M trades insert NM %11d\n",llinsertNMm);
		System.out.printf("TS 1M trades insert NM %11d\n",tsinsertNMm);
		System.out.printf("TS/LL %11f\n",tsinsertNMm/(double)llinsertNMm);

		t=tradeAL.get(nearEnd);
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeTS.add(newt);
		}
		after=System.nanoTime();
		long tsinsertNEm=after-before; //m for mutate
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeLL.add(newt);
		}
		after=System.nanoTime();
		long llinsertNEm=after-before; //m for mutate
		System.out.printf("LL 1M trades insert NE %11d\n",llinsertNEm);
		System.out.printf("TS 1M trades insert NE %11d\n",tsinsertNEm);
		System.out.printf("TS/LL %11f\n",tsinsertNEm/(double)llinsertNEm);
	}
	public static void remove() throws CloneNotSupportedException {
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeAL.remove(nearEnd);
		}
		after=System.nanoTime();
		long alinsertNE=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeAL.remove(nearMiddle);
		}
		after=System.nanoTime();
		long alinsertNM=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeAL.remove(nearStart);
		}
		after=System.nanoTime();
		long alinsertNS=after-before;
		
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeLL.remove(nearEnd);
		}
		after=System.nanoTime();
		long llinsertNE=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeLL.remove(nearMiddle);
		}
		after=System.nanoTime();
		long llinsertNM=after-before;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			tradeLL.remove(nearStart);
		}
		after=System.nanoTime();
		long llinsertNS=after-before;
		System.out.printf("AL Remove Near Start %11d Middle %11d End %11d\n",alinsertNS,alinsertNM,alinsertNE);
		System.out.printf("LL Remove Near Start %11d Middle %11d End %11d\n",llinsertNS,llinsertNM,llinsertNE);
		System.out.printf("Start AL/LL %11f Middle LL/AL %11f End LL/AL %11f\n",alinsertNS/(double)llinsertNS,llinsertNM/(double)alinsertNM,llinsertNE/(double)alinsertNE);
		
		TradeC t=tradeAL.get(nearStart);
		TradeC newt;
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeTS.remove(newt);
		}
		after=System.nanoTime();
		long tsinsertNSm=after-before; //m for mutate
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeLL.remove(newt);
		}
		after=System.nanoTime();
		long llinsertNSm=after-before; //m for mutate
		System.out.printf("LL 1M trades remove NS %11d\n",llinsertNSm);
		System.out.printf("TS 1M trades remove NS %11d\n",tsinsertNSm);
		System.out.printf("LL/TS %11f\n",llinsertNSm/(double)tsinsertNSm);
		
		t=tradeAL.get(nearMiddle);
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeTS.remove(newt);
		}
		after=System.nanoTime();
		long tsinsertNMm=after-before; //m for mutate
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeLL.remove(newt);
		}
		after=System.nanoTime();
		long llinsertNMm=after-before; //m for mutate
		System.out.printf("LL 1M trades remove NM %11d\n",llinsertNMm);
		System.out.printf("TS 1M trades remove NM %11d\n",tsinsertNMm);
		System.out.printf("LL/TS %11f\n",llinsertNMm/(double)tsinsertNMm);

		t=tradeAL.get(nearEnd);
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeTS.add(newt);
		}
		after=System.nanoTime();
		long tsinsertNEm=after-before; //m for mutate
		before=System.nanoTime();
		for(int i=0;i<numInserts;i++) {
			newt=(TradeC)t.clone();
			newt.time=newt.time.plusNanos(i);
			tradeLL.remove(newt);
		}
		after=System.nanoTime();
		long llinsertNEm=after-before; //m for mutate
		System.out.printf("LL 1M trades remove NE %11d\n",llinsertNEm);
		System.out.printf("TS 1M trades remove NE %11d\n",tsinsertNEm);
		System.out.printf("LL/TS %11f\n",llinsertNEm/(double)tsinsertNEm);
	}
	public static void binarySearch() {
		TradeC NStrade=tradeAL.get(nearStart);
		TradeC NMtrade=tradeAL.get(nearMiddle);
		TradeC NEtrade=tradeAL.get(nearEnd);
		//TradeC needs to implement Comparable<TradeC> and override compareTo to use these
		before=System.nanoTime();
		Collections.binarySearch(tradeAL,NStrade); //for AL log(n) for LL (n)
		after=System.nanoTime();
		long alns=after-before;
		before=System.nanoTime();
		Collections.binarySearch(tradeAL,NMtrade); //for AL log(n) for LL (n)
		after=System.nanoTime();
		long alnm=after-before;
		before=System.nanoTime();
		Collections.binarySearch(tradeAL,NEtrade); //for AL log(n) for LL (n)
		after=System.nanoTime();
		long alne=after-before;
		before=System.nanoTime();
		Collections.binarySearch(tradeLL,NStrade); //for AL log(n) for LL (n)
		after=System.nanoTime();
		long llns=after-before;
		before=System.nanoTime();
		Collections.binarySearch(tradeLL,NMtrade); //for AL log(n) for LL (n)
		after=System.nanoTime();
		long llnm=after-before;
		before=System.nanoTime();
		Collections.binarySearch(tradeLL,NEtrade); //for AL log(n) for LL (n)
		after=System.nanoTime();
		long llne=after-before;
		
		System.out.printf("Binary Search AL NS %11d NM %11d NE %11d\n",alns,alnm,alne);
		System.out.printf("Binary Search LL NS %11d NM %11d NE %11d\n",llns,llnm,llne);
		System.out.printf("Binary Search LL/AL NS %11f NM %11f NE %11f\n",llns/(double)alns,llnm/(double)alnm,llne/(double)alne);
		
		before=System.nanoTime();
		tradeTS.contains(NStrade);
		after=System.nanoTime();
		long containsNS=after-before;
		before=System.nanoTime();
		tradeTS.contains(NMtrade);
		after=System.nanoTime();
		long containsNM=after-before;
		before=System.nanoTime();
		tradeTS.contains(NEtrade);
		after=System.nanoTime();
		long containsNE=after-before;
		System.out.printf("Binary Search LL/TS NS %11f AL/TS NS %11f\n",llns/(double)containsNS,alns/(double)containsNS);
		System.out.printf("Binary Search LL/TS NM %11f AL/TS NM %11f\n",llnm/(double)containsNM,alnm/(double)containsNM);
		System.out.printf("Binary Search LL/TS NE %11f AL/TS NE %11f\n",llne/(double)containsNE,alne/(double)containsNE);
	}
	public static void main(String[] args) throws CloneNotSupportedException{
		System.out.println("=========Populate=======");
		populate();
		System.out.println("=========Insert=======");
		insert();
		System.out.println("=========Remove=======");
		remove();
		System.out.println("=========Search=======");
		binarySearch();
	}
}