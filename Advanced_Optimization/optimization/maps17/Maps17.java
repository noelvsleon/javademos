package optimization.maps17;

import java.time.LocalDateTime;

public class Maps17{
	static int numMsgs=2500000; //2500000; for 64s //120000 for 2s
	static int numMsgsIncFills=numMsgs; //2625000 //
	static int numClients=3;
	static int numServerMsgs=numClients*numMsgs;
	static Long startServer,endServer;
	public static void main(String[] args) throws InterruptedException{
		System.out.println(numMsgsIncFills);
		Client[] c=new Client[numClients];
		Serverr s=new Serverr();
		s.start();
		while(startServer==null) {
			Thread.sleep(1);
		}
		System.out.println(Now.n()+"Choose Thread Server, Client too");
		for(int i=0;i<c.length;++i){
			c[i]=new Client(i);
			c[i].start();
		}
		System.out.println(numMsgsIncFills);
		/*
	 	* This Time
	 		* Now use message ids per sym/exchange	but this adds 4 bytes to the message sizes
	 		* Server is doing manual conversion of types as the default UDP class doesn't provide nice methods, and didn't want to look for a wrapper(as they've proven to be slow everywhere else)
	 		* UDP vs TCP
	 		* Made Client id final as it doesn't change and we use it a lot
	 		* We now lose data
	 		* Made 1 exchange buggy and deliberately lose message towards end of day
	 		* Added logoff message from clients to server, so server doesn't rely on message counts to know when to stop, which also takes the first step to allowing arbitrary number of clients
	 		* Server now detects gaps in trades(but not fills) per sym/exchange
	 		* 
		 * Next times
		 	 * How to manage data loss -> rabbitmq
		 	 * Optimisatoin of the UDP code
		 	 * 
		 	 * We could change Serverrs Trade class to have ArrayLists of values instead of having a List of Trades. We'd want to diagram this in Java due to autoboxing, before trying it out
		 	 * Change how Serverr keeps track of Fills, currently stores lots of nulls.
		 	 * We could play with the ByteBuffer size, and the TCP buffer sizes.
		 	 * Server detect pump and dump keep track of sequential buy Fills followed by a sell where the Trade price moved progressively higher(min increase of 5%) (70% of Fills at a price higher than the average of the last 3), the sell is then for at least 70% of the sum of the buys, and the Trades show the price deflating over the next 10 trades to at least 2.5% of its peak   
		 	 * We could switch the order of the if else branches in server
		 	 *  	
		 	 * Server now handles arbitrary number of clients
		 	 * Selector
		 	 * Server now handles reconnection
		 	 * 
		 	 * ...
			 * 
			 * HashMap, then EnumMap
			 * OrderBook
		 */
		for(int i=0;i<c.length;++i) {
			c[i].join();
		}
		s.join();
		double toSeconds=1000000000.;
		System.out.println(Now.n()+"Server "+(endServer-startServer)/toSeconds);
	}
}

class RefData{
	static String[]syms=new String[]{"AAL","ABF","ADM","AHT","ANTO","AV.","AZN","BA.","BARC","BATS","BDEV","BKG","BLND","BLT","BNZL","BP.","BRBY","BT.A","CCH","CCL","CNA","CPG","CRDA","CRH","DCC","DGE","DLG","EVR","EXPN","EZJ","FERG","FRES","GLEN","GSK","GVC","HL.","HLMA","HSBA","IAG","IHG","III","IMB","INF","ITRK","ITV","JE.","JMAT","KGF","LAND","LGEN","LLOY","LSE","MCRO","MKS","MNDI","MRO","MRW","NG.","NMC","NXT","OCDO","PPB","PRU","PSN","PSON","RB.","RBS","RDSA","RDSB","REL","RIO","RMG","RMV","RR.","RRS","RSA","RTO","SBRY","SDR","SGE","SGRO","SHP","SKG","SKY","SLA","SMDS","SMIN","SMT","SN.","SSE","STAN","STJ","SVT","TSCO","TUI","TW.","ULVR","UU.","VOD","WPP","WTB"};
}

interface fi{
	byte[] m();
}


class Now{
	static String n() {
		return LocalDateTime.now()+" ";
	}
}
