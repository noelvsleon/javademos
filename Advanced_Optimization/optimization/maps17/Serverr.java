package optimization.maps17;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
@SuppressWarnings("serial") class LogoffException extends Exception{
	
}
class ExchangeConnection{
	ByteBuffer[] ba=new ByteBuffer[] {ByteBuffer.allocate(34),ByteBuffer.allocate(36)}; //TODO benchmark a bigger buffer
	ByteBuffer bm=ByteBuffer.allocate(1); //TODO benchmark a bigger buffer
	int numBytesRead=0;
	byte tickerByte;
	String ticker;
	Trade t;
	Fill f;
	byte msgType=0x03;
	int[]msgSizes=new int[] {35,37}; //we added 1 to optimise code later
	Map<Byte,DatagramSocket>exchanges=new ConcurrentHashMap<>(); //avoid pesky ConcurrentModificatoinExceptions
	byte[] buf=new byte[256];
	void addExchange(DatagramSocket exch,byte id){
		exchanges.put(id,exch);
	}
	void receiveMsg(DatagramSocket client,byte exchId) throws IOException, LogoffException{
		DatagramPacket packet = new DatagramPacket(buf, buf.length); //TODO can we reuse? //TODO will this get less than buf.length? or do we need 1 byte buf etc
		client.receive(packet); //This will block, so we should have used the nonblocking version for the server
//		String received=new String(packet.getData(), 0, packet.getLength());
		msgType=buf[0];
//			if(msgType==0x02) {
//				numBytesRead=client.read(bm);
//				if(numBytesRead==0) {
//					++Serverr.timesServerBufferEmpty;
//					return;
//				}else {
//					msgType=bm.array()[0];
//					bm.clear();
//				}
//			}
//			numBytesRead+=client.read(ba[msgType]);
//			if(numBytesRead<msgSizes[msgType]) {
//				++Serverr.timesServerBufferEmpty;
//				return;
//			}
//			ByteBuffer b=ba[msgType];
//			b.flip();
			if(msgType==0x01) {
				t=new Trade(
						(buf[1]<<24)+(buf[2]<<16)+(buf[3]<<8)+(buf[4]<<0),
						((long)buf[5]<<56)+((long)buf[6]<<48)+((long)buf[7]<<40)+((long)buf[8]<<32)+((long)buf[9]<<24)+((long)buf[10]<<16)+((long)buf[11]<<8)+((long)buf[12]<<0),
						((long)buf[13]<<56)+((long)buf[14]<<48)+((long)buf[15]<<40)+((long)buf[16]<<32)+((long)buf[17]<<24)+((long)buf[18]<<16)+((long)buf[19]<<8)+((long)buf[20]<<0),
						Double.longBitsToDouble(((long)buf[21]<<56)+((long)buf[22]<<48)+((long)buf[23]<<40)+((long)buf[24]<<32)+((long)buf[25]<<24)+((long)buf[26]<<16)+((long)buf[27]<<8)+((long)buf[28]<<0)),
						(buf[29]<<24)+(buf[30]<<16)+(buf[31]<<8)+(buf[32]<<0),
						(char)buf[33]);
				tickerByte=buf[34];
				Serverr.trades[tickerByte].newTrade(t,exchId);
			}else if(msgType==0x02){
				f=new Fill(
						(buf[1]<<24)+(buf[2]<<16)+(buf[3]<<8)+(buf[4]<<0),
						((long)buf[5]<<56)+((long)buf[6]<<48)+((long)buf[7]<<40)+((long)buf[8]<<32)+((long)buf[9]<<24)+((long)buf[10]<<16)+((long)buf[11]<<8)+((long)buf[12]<<0),
						((long)buf[13]<<56)+((long)buf[14]<<48)+((long)buf[15]<<40)+((long)buf[16]<<32)+((long)buf[17]<<24)+((long)buf[18]<<16)+((long)buf[19]<<8)+((long)buf[20]<<0),
						Double.longBitsToDouble(((long)buf[21]<<56)+((long)buf[22]<<48)+((long)buf[23]<<40)+((long)buf[24]<<32)+((long)buf[25]<<24)+((long)buf[26]<<16)+((long)buf[27]<<8)+((long)buf[28]<<0)),
						(buf[1]<<29)+(buf[2]<<30)+(buf[31]<<8)+(buf[32]<<0),
						(char)buf[33],
						(short)((buf[34]<<8)+(buf[35]<<0)));
				tickerByte=buf[36]; //This fixes a bug from the past few versions
				Serverr.trades[tickerByte].newFill(f,exchId);
			}else {
				throw new LogoffException();
			}
			numBytesRead=0;
			msgType=0x02;
//			b.clear();
			++Serverr.numMsgsRecv;
	}
	void receive() {
		final int numMsgs=Maps17.numMsgsIncFills*Maps17.numClients;
		while(true){
			for(Entry<Byte,DatagramSocket>e:exchanges.entrySet()){
				try{
					receiveMsg(e.getValue(),e.getKey());
				}catch(IOException err){
					err.printStackTrace();
				}catch(LogoffException err) {
					System.out.println("Exchange logoff "+e.getKey());
					exchanges.remove(e.getKey());
					if(exchanges.size()==0) {
						return; //TODO benchmark this against changing the condition
					}
				}
			}
		}
	}

}
class Serverr extends Thread{
 	static MarketData[]trades=new MarketData[RefData.syms.length];
 	static {
 		for(int i=0;i<trades.length;++i) {
 			trades[i]=new MarketData(RefData.syms[i]);
 		}
 	}
	static long timesServerBufferEmpty=0;
	static int numMsgsRecv=0;
	Serverr(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			Maps17.startServer=System.nanoTime();
			DatagramSocket[]client=new DatagramSocket[Maps17.numClients];
			ExchangeConnection exs=new ExchangeConnection();
			for(int i=0;i<Maps17.numClients;++i) {
				client[i]=new DatagramSocket(3000+i);
				exs.addExchange(client[i],(byte)i);
			}
			exs.receive();
			Maps17.endServer=System.nanoTime();
			System.out.println(Now.n()+"Server buffer empty "+timesServerBufferEmpty+" Num trades "+trades.length);
			for(int i=0;i<5;++i) {
				MarketData md=trades[i];
				for(int j=0;j<Maps17.numClients;++j) {
					if(md.trades[j].size()==0) {
						System.out.println(Now.n()+" "+RefData.syms[i]+" No trades");
					}else {
						System.out.printf("%s: %s Count %6d Last %91s High %91s Low %91s\n",Client.exchangeNames[j],RefData.syms[i],md.trades[j].size(),md.trades[j].get(md.trades[j].size()-1),md.high,md.low);
					}
				}
			}
			System.out.println("Missing Trades\n------");
			for(int i=0;i<trades.length;++i) {
				for(int j=0;j<trades[i].missingTrades.length;++j) {
					if(trades[i].missingTrades[j].size()>0) {
						System.out.printf("Missing Trades %s %s %d\n",Client.exchangeNames[j],RefData.syms[i],trades[i].missingTrades[j].size());
					}
					if(trades[i].missingFills[j].size()>0) {
						System.out.printf("Missing Fills %s %s %d\n",Client.exchangeNames[j],RefData.syms[i],trades[i].missingFills[j].size());
					}
				}
			}
			for(DatagramSocket s:client) {
				s.close();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}