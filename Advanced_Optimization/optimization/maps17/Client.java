package optimization.maps17;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Random;

import optimization.maps16.DateTime;
import optimization.maps16.TradeC;

class Client extends Thread{
	static String[]exchangeNames=new String[] {"NYSE","XLON","XETRA"};
	final byte id;
	static ArrayList<TradeC>trades=new ArrayList<>(Maps17.numMsgs);
	static ArrayList<FillC>fills=new ArrayList<>(Maps17.numMsgs);
	static {
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		Random rand=new Random();
		for(int i=0;i<Maps17.numMsgs;i++) {
			
			trades.add(new TradeC(
				now.plus(1),
				now2.plus(1),
				123.45+rand.nextInt(20),
				1000+i,
				0==i/2?'B':'S',
				(byte)rand.nextInt(RefData.syms.length)));
			if((i%20)==0) {
				fills.add(i,new FillC(trades.get(i),(short)rand.nextInt(15)));
				++Maps17.numMsgsIncFills;
			}else {
				fills.add(null);
			}
		}
	}
	Client(int id){
		this.name=exchangeNames[id];
		this.id=(byte)id;
		this.setName("Client "+name);
	}
	long startClient=0;
	long endClient;
	String name;
	public void run() {
		try{
			DatagramSocket server=new DatagramSocket();
			
			DatagramPacket packet;
			InetAddress address=InetAddress.getByName("localhost");
			
//			ServerSocketChannel listenSock = ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000+id));
			startClient=System.nanoTime();
//			SocketChannel server=listenSock.accept();
//			server.configureBlocking(false);

			byte[]writeBufferA=new byte[40];
//			ByteBuffer writeBuffer=ByteBuffer.wrap(writeBufferA);
			long timesClientBufferFull=0;
			long price;
			int[]msgIdsT=new int[RefData.syms.length];
			int[]msgIdsF=new int[RefData.syms.length];
			int msgId=0;
			for(int i=Maps17.numMsgs-1;i>=0;i--) {
				TradeC t=trades.get(i);
				writeBufferA[0] = 0x01; //msgtype 0=Trade, 1=Fill
				if((id==0) && (msgId>Maps17.numMsgs-10) && (0==msgId%2)) {
					msgIdsT[t.ticker]+=3;
				}
				msgId=msgIdsT[t.ticker]++;
				writeBufferA[1]=(byte)((msgId >>> 24) & 0xFF); //TODO why is this different from the longs?
				writeBufferA[2]=(byte)((msgId >>> 16) & 0xFF);
				writeBufferA[3]=(byte)((msgId >>> 8) & 0xFF);
				writeBufferA[4]=(byte)((msgId >>> 0) & 0xFF);
		      writeBufferA[5] = (byte)(t.exchTime.nanoseconds >>> 56);
		      writeBufferA[6] = (byte)(t.exchTime.nanoseconds >>> 48);
		      writeBufferA[7] = (byte)(t.exchTime.nanoseconds >>> 40);
		      writeBufferA[8] = (byte)(t.exchTime.nanoseconds >>> 32);
		      writeBufferA[9] = (byte)(t.exchTime.nanoseconds >>> 24);
		      writeBufferA[10] = (byte)(t.exchTime.nanoseconds >>> 16);
		      writeBufferA[11] = (byte)(t.exchTime.nanoseconds >>>  8);
		      writeBufferA[12] = (byte)(t.exchTime.nanoseconds >>>  0);
		      writeBufferA[13] = (byte)(t.time.nanoseconds >>> 56);
		      writeBufferA[14] = (byte)(t.time.nanoseconds >>> 48);
		      writeBufferA[15] = (byte)(t.time.nanoseconds >>> 40);
		      writeBufferA[16] = (byte)(t.time.nanoseconds >>> 32);
		      writeBufferA[17] = (byte)(t.time.nanoseconds >>> 24);
		      writeBufferA[18] = (byte)(t.time.nanoseconds >>> 16);
		      writeBufferA[19] = (byte)(t.time.nanoseconds >>>  8);
		      writeBufferA[20] = (byte)(t.time.nanoseconds >>>  0);
		      price=Double.doubleToLongBits(t.price);
		      writeBufferA[21] = (byte)(price >>> 56);
		      writeBufferA[22] = (byte)(price >>> 48);
		      writeBufferA[23] = (byte)(price >>> 40);
		      writeBufferA[24] = (byte)(price >>> 32);
		      writeBufferA[25] = (byte)(price >>> 24);
		      writeBufferA[26] = (byte)(price >>> 16);
		      writeBufferA[27] = (byte)(price >>>  8);
		      writeBufferA[28] = (byte)(price >>>  0);
		      writeBufferA[29]=(byte)((t.size >>> 24) & 0xFF); //TODO why is this different from the longs?
		      writeBufferA[30]=(byte)((t.size >>> 16) & 0xFF);
		      writeBufferA[31]=(byte)((t.size >>> 8) & 0xFF);
		      writeBufferA[32]=(byte)((t.size >>> 0) & 0xFF);
		      writeBufferA[33]=(byte)t.side;
		      writeBufferA[34]=t.ticker;
//		      writeBuffer.limit(35); //some annoying checks, could subclass it and break the encapsulation
//		      server.write(writeBuffer);
				server.send(new DatagramPacket(writeBufferA, 35, address, 3000+id)); //TODO can we reuse?

//		      while(0!=writeBuffer.remaining()) {
//		      	++timesClientBufferFull;
//		      	server.write(writeBuffer);
//		      }
		      if(fills.get(i)!=null) {
//			      writeBuffer.position(0);
			      FillC f=fills.get(i);
			      writeBufferA[0] = 0x02; //msgtype 0=EOD, 1=Trade, 2=Fill
					msgId=msgIdsF[f.ticker]++;
					writeBufferA[1]=(byte)((msgId >>> 24) & 0xFF); //TODO why is this different from the longs?
					writeBufferA[2]=(byte)((msgId >>> 16) & 0xFF);
					writeBufferA[3]=(byte)((msgId >>> 8) & 0xFF);
					writeBufferA[4]=(byte)((msgId >>> 0) & 0xFF);
			      writeBufferA[5] =(byte)(f.exchTime.nanoseconds >>> 56);
			      writeBufferA[6] =(byte)(f.exchTime.nanoseconds >>> 48);
			      writeBufferA[7] =(byte)(f.exchTime.nanoseconds >>> 40);
			      writeBufferA[8] =(byte)(f.exchTime.nanoseconds >>> 32);
			      writeBufferA[9] =(byte)(f.exchTime.nanoseconds >>> 24);
			      writeBufferA[10] =(byte)(f.exchTime.nanoseconds >>> 16);
			      writeBufferA[11] =(byte)(f.exchTime.nanoseconds >>>  8);
			      writeBufferA[12] =(byte)(f.exchTime.nanoseconds >>>  0);
			      writeBufferA[13] =(byte)(f.time.nanoseconds >>> 56);
			      writeBufferA[14] =(byte)(f.time.nanoseconds >>> 48);
			      writeBufferA[15]=(byte)(f.time.nanoseconds >>> 40);
			      writeBufferA[16]=(byte)(f.time.nanoseconds >>> 32);
			      writeBufferA[17]=(byte)(f.time.nanoseconds >>> 24);
			      writeBufferA[18]=(byte)(f.time.nanoseconds >>> 16);
			      writeBufferA[19]=(byte)(f.time.nanoseconds >>>  8);
			      writeBufferA[20]=(byte)(f.time.nanoseconds >>>  0);
			      price=Double.doubleToLongBits(f.price);
			      writeBufferA[21]=(byte)(price >>> 56);
			      writeBufferA[22]=(byte)(price >>> 48);
			      writeBufferA[23]=(byte)(price >>> 40);
			      writeBufferA[24]=(byte)(price >>> 32);
			      writeBufferA[25]=(byte)(price >>> 24);
			      writeBufferA[26]=(byte)(price >>> 16);
			      writeBufferA[27]=(byte)(price >>>  8);
			      writeBufferA[28]=(byte)(price >>>  0);
			      writeBufferA[29]=(byte)((f.size >>> 24) & 0xFF);
			      writeBufferA[30]=(byte)((f.size >>> 16) & 0xFF);
			      writeBufferA[31]=(byte)((f.size >>> 8) & 0xFF);
			      writeBufferA[32]=(byte)((f.size >>> 0) & 0xFF);
			      writeBufferA[33]=(byte)f.side;
			      writeBufferA[34]=(byte)((f.cp >>> 8) & 0xFF);
			      writeBufferA[35]=(byte)((f.cp >>> 0) & 0xFF);
			      writeBufferA[36]=f.ticker;
//			      writeBuffer.limit(37); //some annoying checks, could subclass it and break the encapsulation
//			      server.write(writeBuffer);
			      server.send(new DatagramPacket(writeBufferA, 37, address, 3000+id)); //TODO can we reuse?
//			      while(0!=writeBuffer.remaining()) {
//			      	++timesClientBufferFull;
//			      	server.write(writeBuffer);
//			      }
		      }		      
//		      writeBuffer.clear();
			}
			endClient=System.nanoTime();
			System.out.println(Now.n()+"Client "+name+" time "+(endClient-startClient)/1000000000.);
//			server.configureBlocking(true);
			writeBufferA[0]=0x00;
			server.send(new DatagramPacket(writeBufferA,1,address,3000+id));
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}