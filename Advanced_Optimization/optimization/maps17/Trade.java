package optimization.maps17;

import optimization.maps16.DateTime;

class Trade{
	int id;
	long exchTime;
	long time;
	double price;
	int size;
	char side;
	Trade(int id,long exchTime,long time, double price, int size, char side){
		this.id=id;
		this.exchTime=exchTime;
		this.time=time;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public String toString(){
		return DateTime.toString(exchTime)+","+DateTime.toString(time)+","+price+","+size+","+side;
	}
}