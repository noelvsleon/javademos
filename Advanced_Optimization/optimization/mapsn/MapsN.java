package optimization.mapsn;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class MapsN{
	static int numMsgs=1200000; //10000 for 1s
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		//LinkedHashMap with ordering of Keys
		//HashMap no order of keys
		//ConcurrentSkipListMap sorted keys
		//trades
		
		
//		Enummap of tickers to array lists of trades 
//		Or last price
//		Msg will have ticker as number or byte array
//		Bench different ways of tranformin
//		To in
//		To string
//		To enum
//		Have byte array as key in hashmap
//		Might need networking to defeat compiler
// Map of order book trees
// 1 structure for B another for S

		
		//Debug(VisualVM)
		//????
		/*
		 * Sampler>CPU>Hotspots, Choose Threads main
		 * Resume
		 */
		
		Client c=new Client();
		Server s=new Server();
		
		//new Object->print->GC
//		s.receiveObjects();
//		c.sendObjects();
		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/* Sampler>CPU>Hotspots, Choose Threads main
		 * At the bottom, can see that the readObject, writeObject and because it is waiting main are the 3 methods that consume the most CPU time.
		 * java.time.LocalDateTime.now consumes 300ms on startup and the Client constructor <init> therefore takes 434ms
		 * similarly plusSeconds() takes 131ms
		 * Next we'll look at Maps2.java which moves the now() method call out of the loop.
		 * 
		 * Sampler>Memory>Per thread allocations>right click>filter>main,Client,Serverr>Enter
		 * we'll see Serverr and Client grow, when get to ~400MB each, click perform GC, no real effect
		 * switch to Heap histogram, top objects are:
		 * 	LocalDateTime 4.5M objects consuming 100MB similar for LocalDate, LocalTime
		 * 	Object[] for 289K for 89MB
		 * 	Trade for 2.2M for 89MB
		 * 	int[] 1189 for 56MB
		 * 	ObjectInputStream$HandleTable$HandleList[] 89 for 34MB
		 * 	byte[] 686K for 26M
		 */
		s.join();
		c.join();
		double toSeconds=1000000000.;
		System.out.println("Server "+(endServer-startServer)/toSeconds);
		System.out.println("Client "+(endClient-startClient)/toSeconds);

//		s.receiveFIX();
//		c.sendFIX();
//		
//		s.receiveBytes();
//		c.sendBytes();
//		
//		s.receiveBytesUDP();
//		c.sendBytesUDP();
//		
//		//reuse Object->print //last prices
//		s.receiveObjects();
//		c.sendObjects();
//		//experiment with different ways of storing objects ...., 
//		s.receiveObjects();
//		c.sendObjects();
//		
//		s.receiveFIX();
//		c.sendFIX();
//		
//		s.receiveBytes();
//		c.sendBytes();
//		
//		s.receiveBytesUDP();
//		c.sendBytesUDP();
// TODO pipes
	}

}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(MapsN.numMsgs);;
	Client(){
		this.setName("Client");
		for(int i=0;i<MapsN.numMsgs;i++) {
			trades.add(new Trade(
				LocalDateTime.now().plusSeconds(i),
				LocalDateTime.now().plusSeconds(i+60),
				"vod",
				123.45+i,
				1000+i,
				0==i/2?'B':'S'));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			Socket serverSock=new Socket("localhost",3000);
			MapsN.startClient=System.nanoTime();
			ObjectOutputStream server=new ObjectOutputStream(serverSock.getOutputStream());
			for(int i=0;i<MapsN.numMsgs;i++) {
				server.writeObject(trades.get(i));
			}
			server.flush();
			MapsN.endClient=System.nanoTime();
			serverSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
class Server extends Thread{
//	ArrayList<Trade>trades=new ArrayList<>(Maps.numMsgs);;
	Server(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	public void run() {
		try{
			ServerSocket listenSock=new ServerSocket(3000);
			MapsN.startServer=System.nanoTime();
			Socket clientSock=listenSock.accept();
			ObjectInputStream client=new ObjectInputStream(clientSock.getInputStream());
			for(int i=0;i<MapsN.numMsgs;i++) {
				client.readObject();
			}
			MapsN.endServer=System.nanoTime();
			listenSock.close();
		}catch(IOException e){
			e.printStackTrace();
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
}
@SuppressWarnings("serial") class Trade implements Comparable<Trade>,Serializable{
	LocalDateTime exchTime;
	LocalDateTime time;
	String ticker;
	double price;
	int size;
	char side;
	Trade(LocalDateTime exchTime,LocalDateTime time, String ticker, double price, int size, char side){
		this.exchTime=exchTime;
		this.time=time;
		this.ticker=ticker;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public int compareTo(Trade t) {
		return time.compareTo(t.time);
	}
}