package optimization;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;


enum TradeE{exchTime,time,ticker,price,size,side}
public class TODOHashMapVSMap{
	
	public static void enumMaps() {
		//hashmap vs enummap vs 2 [] vs object 
		//enummaps have fixed keyset where you get all the values
		//do object too
		//both allow naming the values
		//alternatively just use an array with named indices
		
		final byte exchTime=0,timee=1,ticker=2,price=3,size=4,side=5;
		Object[]v=new Object[] {LocalDateTime.now(),
				LocalDateTime.now(),
				"vod",
				123.45,
				1000,
				"B"
		};
		
		HashMap<String,Object>hs=new HashMap<>() {{
			put("exchTime",v[0]);
			put("time",v[1]);
			put("ticker",v[2]);
			put("price",v[3]);
			put("size",v[4]);
			put("side",v[5]);
		}};
		HashMap<TradeE,Object>he=new HashMap<>();
		EnumMap<TradeE,Object>e=new EnumMap<>(TradeE.class);
		TradeC o=new TradeC((LocalDateTime)v[0],(LocalDateTime)v[1],(String)v[2],(double)v[3],(int)v[4],(char)v[5]);
		
		
		
		//Benchmarks
			//TODO Creation??
			//Get
			//Set
		//TODO benchmark where values are lists, so representing a table, then operations like get row
		
//		
//		final String[]
		
		
		
		
		EnumMap<GFG, String> gfgMap = new 
            EnumMap<GFG, String>(GFG.class);
		gfgMap.put(GFG.CODE, "Start Coding with gfg"); 
      gfgMap.put(GFG.CONTRIBUTE, "Contribute for others"); 
      gfgMap.put(GFG.MCQ, "Test Speed with Mcqs"); 
      gfgMap.put(GFG.QUIZ, "Practice Quizes"); 
      gfgMap.put(GFG.QUIZ, "dave"); 
      
      //String vs char[]
      //but also the immutability of strings.
	}
	public static void charAvsString() {
		
	}
	public enum GFG 
   { 
       CODE, CONTRIBUTE, QUIZ, MCQ; 
   }

	public static void main(String[] args){
		
		
		//TODO enumMaps();
		//TODO charAvsString();
		
//		HashMap 
//			growth vs presize
//			performance at diff sizes S/M/L
//			key length
//			collisions
		
		//DON'T// this kind of question will be answered in the enumMaps section//array of ArrayLists
//TODO			array of HashMaps 
//TODO			vs HashMap of ArrayLists
//			vs ArrayList of Trade objects
//			vs LinkedList of Trade objects
		
		

//		Collections.fill(list,obj);
//		Collections.frequency(c,o);
//		Collections.sort(list);
//		Collections.binarySearch(list,key);
//		Collections.man, min, reverse, sort
		
//		Arrays.binarySearch(a,key);
//		Arrays.copyOf, copyOfRange, deepEquals
//		Arrays.parallel
//		Arrays.file, sort
		
		//TODO a table of the data structures - list versus key/values, verses duplicate keys
		
	}

}
