package optimization;

class FibonacciI{
	long x;
	long y;
	FibonacciI(){
		x=0;y=1;
	}
	long getN(short n) {
		if(n<0) {
			return -2;
		}
		x=0;y=1;
		switch(n) {
			case 0:return -1;
			case 1:return x;
			case 2:return y;
		}
		long res=0;
		while(n-->2) {
			res=x+y;
			x=y;
			y=res;
		}
		return res;
	}
}

class FibonacciR{
	long x=0;
	long y=1;
	long res;
	long getN(short n) {
		if(n<0) {
			return -2;
		}
		switch(n) {
			case 0:return -1;
			case 1:return 0;
			case 2:return 1;
		}
		res=x+y;
		if(n==3) {
			return res;
		}
		x=y;
		y=res;
		return getN(--n);
	}
}


public class Recursion{

	public static void main(String[] args){
		System.out.println("\n\n===================================================\n===================================================\n===================================================\n\n");
		long before,after;
		short[] maxs= {200,200,3000,3000,20000,20000,200};
		for(short max:maxs){
			System.out.println("-------------"+max);
			before=System.nanoTime();
			long res=0;
			for(short i=0;i<max;++i) {
					res+=new FibonacciR().getN(i);
			}
			after=System.nanoTime();
			long duration=after-before;
			System.out.println("########"+duration+"==="+res);
			res=0;
			before=System.nanoTime();
			for(short i=0;i<max;++i) {
				if(0==i%50)
					res+=new FibonacciI().getN(i);
			}
			after=System.nanoTime();
			System.out.println("########"+(after-before)+"==="+res);
			System.out.println("########"+duration/(double)(after-before));
		}
		
		
		//look at output, do again after compi
		
		/*
		 * 
		 * break
Sets a breakpoint when debugging the JVM to stop at the beginning of compilation of the specified method.


		 * 
		 * 
		 * 
		 */
	}

}
