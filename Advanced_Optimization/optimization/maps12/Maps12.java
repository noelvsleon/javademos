package optimization.maps12;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;

import optimization.maps03.DateTime;

public class Maps12{
	static int numMsgs=8000000; //8000000 for 48s //120000; //for 1.0s //4000000; //for 21s
	static Long startServer,endServer,startClient,endClient;
	public static void main(String[] args) throws InterruptedException{
		Client c=new Client();
		Server s=new Server();

		s.start();
		while(null==startServer) {
			Thread.sleep(1);
		}
		c.start();
		System.out.println("Choose Thread Server, Client too");
		/*
		 * This time: non blocking server
		 * For 1s seems the same speed, but for the large input 4000000 it now only takes 21s, so increase the large input
		 * Client and Server are equal with 20s, after doubling input to 8000000, it's 43s each with the only hotspots being read and write
		 * Server buffer was empty 36972768 times whereas Client buffer was never full
		 *  
		 * Next times
			 * All trades in server 
			 * Multiple clients and switch focus to server optimisation
			 * Last trade for vod
			 * Last trade table
			 * Increase size of server buffer
			 * Could explore TCP buffer size
			 * UDP vs TCP
			 * Keeping all trades on the server ArrayList of Objects, Object of ArrayLists
			 * Multiple instruments, HashMap, then EnumMap
			 * OrderBook
			 * ticker -> test grabbing the bytes once instead of letting it do it repeatedly
		 * 
		 */
		c.join();
		s.join();
		double toSeconds=1000000000.;
		System.out.println("Client "+(endClient-startClient)/toSeconds);
		System.out.println("Server "+(endServer-startServer)/toSeconds);
	}
}

class Client extends Thread{
	ArrayList<Trade>trades=new ArrayList<>(Maps12.numMsgs);
	Client(){
		this.setName("Client");
		DateTime now=DateTime.now();
		DateTime now2=new DateTime(now.nanoseconds+60);
		byte[]vod=new byte[] {'v','o','d'};
		for(int i=0;i<Maps12.numMsgs;i++) {
			trades.add(new Trade(
				now.plus(1),
				now2.plus(1),
				123.45+i,
				1000+i,
				0==i/2?'B':'S',
				vod));
		}
		System.out.println("Client ready");
	}
	public void run() {
		try{
			SocketAddress a=new InetSocketAddress("localhost",3000);
			SocketChannel server=SocketChannel.open();
			server.connect(a);
			Maps12.startClient=System.nanoTime();
			server.configureBlocking(false);
			final byte[]writeBufferA=new byte[40];
			final ByteBuffer writeBuffer=ByteBuffer.wrap(writeBufferA);
			long timesClientBufferFull=0;
			long price;
			for(int i=Maps12.numMsgs-1;i>=0;i--) {
				Trade t=trades.remove(i);
		      writeBufferA[0] = (byte)(t.exchTime.nanoseconds >>> 56);
		      writeBufferA[1] = (byte)(t.exchTime.nanoseconds >>> 48);
		      writeBufferA[2] = (byte)(t.exchTime.nanoseconds >>> 40);
		      writeBufferA[3] = (byte)(t.exchTime.nanoseconds >>> 32);
		      writeBufferA[4] = (byte)(t.exchTime.nanoseconds >>> 24);
		      writeBufferA[5] = (byte)(t.exchTime.nanoseconds >>> 16);
		      writeBufferA[6] = (byte)(t.exchTime.nanoseconds >>>  8);
		      writeBufferA[7] = (byte)(t.exchTime.nanoseconds >>>  0);
		      writeBufferA[8] = (byte)(t.time.nanoseconds >>> 56);
		      writeBufferA[9] = (byte)(t.time.nanoseconds >>> 48);
		      writeBufferA[10] = (byte)(t.time.nanoseconds >>> 40);
		      writeBufferA[11] = (byte)(t.time.nanoseconds >>> 32);
		      writeBufferA[12] = (byte)(t.time.nanoseconds >>> 24);
		      writeBufferA[13] = (byte)(t.time.nanoseconds >>> 16);
		      writeBufferA[14] = (byte)(t.time.nanoseconds >>>  8);
		      writeBufferA[15] = (byte)(t.time.nanoseconds >>>  0);
		      price=Double.doubleToLongBits(t.price);
		      writeBufferA[16] = (byte)(price >>> 56);
		      writeBufferA[17] = (byte)(price >>> 48);
		      writeBufferA[18] = (byte)(price >>> 40);
		      writeBufferA[19] = (byte)(price >>> 32);
		      writeBufferA[20] = (byte)(price >>> 24);
		      writeBufferA[21] = (byte)(price >>> 16);
		      writeBufferA[22] = (byte)(price >>>  8);
		      writeBufferA[23] = (byte)(price >>>  0);
		      writeBufferA[24]=(byte)((t.size >>> 24) & 0xFF);
		      writeBufferA[25]=(byte)((t.size >>> 16) & 0xFF);
		      writeBufferA[26]=(byte)((t.size >>> 8) & 0xFF);
		      writeBufferA[27]=(byte)((t.size >>> 0) & 0xFF);
		      writeBufferA[28]=(byte)t.side;
		      writeBufferA[29]=(byte)t.ticker.length;
		      writeBuffer.position(30); //some annoying checks, could subclass it and break the encapsulation
				writeBuffer.put(t.ticker); 
		      server.write(writeBuffer.flip());
		      while(0!=writeBuffer.remaining()) {
		      	++timesClientBufferFull;
		      	server.write(writeBuffer);
		      }
		      writeBuffer.clear();
			}
			Maps12.endClient=System.nanoTime();
			System.out.println("Client buffer full "+timesClientBufferFull);
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
interface fi{
	byte[] m();
}
class Server extends Thread{
//	ArrayList<Trade>trades=new ArrayList<>(Maps.numMsgs);;
	Server(){
		this.setName("Serverr"); //misspell so that VisualVM filter is easy
	}
	@SuppressWarnings("static-access") public void run() {
		try{
			ServerSocketChannel listenSock = ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000));
			Maps12.startServer=System.nanoTime();
			SocketChannel client=listenSock.accept();
			client.configureBlocking(false);
			final ByteBuffer b=ByteBuffer.allocate(40); //TODO benchmark a bigger buffer
//			byte[]ticker=new byte[10];
			int numMsgsRecv=0;
			int numBytesRead=0;
			byte tickerLengthByte;
			Trade t;
			long timesServerBufferEmpty=0;
			while(numMsgsRecv<Maps12.numMsgs) {
				numBytesRead+=client.read(b);
				while(numBytesRead<31) { //minimum message would be a trade with a 1 char ticker
					++timesServerBufferEmpty;
					numBytesRead+=client.read(b);
				}
				b.flip();
				
				t=new Trade(
						new DateTime(b.getLong()),
						new DateTime(b.getLong()),
						b.getDouble(),
						b.getInt(),
						(char)b.get());
				tickerLengthByte=b.get();
				numBytesRead-=30;
				if(numBytesRead<tickerLengthByte) {
					b.position(numBytesRead+1);
				}
				while(numBytesRead<tickerLengthByte) {
					++timesServerBufferEmpty;
					numBytesRead+=client.read(b);
				}
				t.ticker=Arrays.copyOfRange(b.array(),30,30+tickerLengthByte);
				b.position(30+tickerLengthByte);
				numBytesRead=b.remaining();
				++numMsgsRecv;
				b.compact();
			}
			Maps12.endServer=System.nanoTime();
			System.out.println("Server buffer empty "+timesServerBufferEmpty);
			client.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}