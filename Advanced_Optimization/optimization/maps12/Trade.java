package optimization.maps12;

import java.io.Serializable;

import optimization.maps03.DateTime;

@SuppressWarnings("serial") public class Trade implements Comparable<Trade>,Serializable{
	DateTime exchTime;
	DateTime time;
	byte[] ticker;
	double price;
	int size;
	char side;
	public Trade(DateTime exchTime,DateTime time, double price, int size, char side, byte[] ticker){
		this.exchTime=exchTime;
		this.time=time;
		this.price=price;
		this.size=size;
		this.side=side;
		this.ticker=ticker;
	}
	public Trade(DateTime exchTime,DateTime time, double price, int size, char side){
		this.exchTime=exchTime;
		this.time=time;
		this.price=price;
		this.size=size;
		this.side=side;
	}
	public int compareTo(Trade t) {
		return time.compareTo(t.time);
	}
}