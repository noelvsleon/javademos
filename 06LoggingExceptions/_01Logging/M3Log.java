package _01Logging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class M3Log {
/*HOWTO ECLIPSE
 * DOWNLOAD
 * 	https://logback.qos.ch/download.html
 * 	https://www.slf4j.org/download.html
 * ADD TO WINDOW>PREFERENCES>JAVA>BUILD PATH>USER LIBRARIES>NEW>Add external JAR
 * 	logback-core
 * 	logback-classic
 * 	slf4j-api
 * ADD TO PROJECT>PROPERTIES>JAVA BUILD PATH>LIBRARIES>Classpath>Add library>User Library>
 * 	logback-core
 * 	logback-classic
 * 	slf4j-api
 * RUN IT
 * 	Will get some funny symbols and [34m as these are the BASH/IntelliJ colours and we are running on windows
 * 	javademos/myApp.log will be created/appended to
 * MANUALS
 * 	https://www.slf4j.org/manual.html
 * 	https://logback.qos.ch/manual/introduction.html
 * 	https://logback.qos.ch/manual/configuration.html
 * 
 * */
/*TODO HOWTO ECLIPSE MAVEN*/
/*HOWTO INTELLIJ
 * File>Project Structure>Global Libraries>+>From Maven
 * 		ch.qos.logback:logback-classic:1.2.3 >ok>ok> also brinks back logback core and slf4j-api
 * Run
 * We use System.setProperty, but if we didn't we'd have to use: Run>Edit Configurations>VM Options>-Dlogback.configurationFile=src/logging/logback.xml
 **/
	static {System.setProperty("logback.configurationFile", "logback.xml");}
	public static final Logger lg=LoggerFactory.getLogger(M3Log.class); //Next step would be to put this in its own thread
	//TODO create another version which has a logging thread/maybe just do in project as part of the initial example
	public static void main(String[] args) {
	    lg.info("Hello World");  //Will print if log level is info/debug/trace
	    lg.debug("I am alive!"); //Will print if log level is debug/trace
	    lg.trace("I am human!"); //Will print if log level is trace
	}
}
