package _02Exceptions;

import java.io.FileReader;
import java.io.IOException;
import org.ietf.jgss.GSSException;
import _01Logging.M3Log;

public class main {
	static void print(Exception e){
		M3Log.lg.info("getMessage:"+e.getMessage());
		M3Log.lg.info("toString:"+e); //there might not be a message so toString is handy
		M3Log.lg.debug("printStackTrace:");
		for(var ste:e.getStackTrace()) {
			M3Log.lg.debug(ste.toString());
		}
	}
	public static void main(String[] args) {
		//DEBUG ME
		try{
			int[] a=new int[10];
			a[10]=0;
			M3Log.lg.info("first");
		}catch(ArrayIndexOutOfBoundsException e){
			print(e);
			//show can click on the stacktrace if in eclipse
			//otherwise use ctrl+shift+r
		}
		try{
			throw new MyException();
			//not allowed to have this code as it is unreachable, showcase this//print("second");
		}catch(MyException e){
			print(e);
		}
		try{
			throw new MyException2("custom error message");
		}catch(MyException2 e){print(e);}
		try{
			new AClass(42);
			M3Log.lg.info("never here");
		}catch(MyException2 e){
			print(e);
			//talk about investigating stack traces, often need to understand the error, and the context
			/*often the error is not diagnosable from the stack trace, for example somewhere earlier we forgot to initialise something and got a NPE, 
			 * or the object is incorrectly configured and it is these things that need to be fixed. 
			 * "Don't do that" vs "Handle the error" approaches. You need to make the program do what it is supposed to, not just make errors go away.
			 */
			/*many programs routinely error and print stacktraces when in fact they are not encountering issues, this is bad design, but is common. 
			The most common cases of this are "No config file found","Can't log","No admin port available", "Component A(which we aren't using) didn't start properly.
			*/
		}
		//Java 7 try with resources
		try(FileReader f=new FileReader("Z:\\dave.txt")){
			M3Log.lg.info("file");
			throw new MyException();
		}catch(Exception e){
			print(e);
		}
		//TODO other try with resources examples
		
		//JUMP TO file.java THEN JUMP BACK HERE
		
		for(int i=0;i<10;i++){
			try{
				M3Log.lg.info(i+" Start ");
				switch(i){
					case 0:break;
					case 1:continue;
					case 2:throw new IOException("io excep");
					case 3:throw new GSSException(42);
					case 4:throw new MyException();
					case 5:return;
				}
				M3Log.lg.info("Middle ");
			}catch(IOException | GSSException e){ //Java 7 | in catch
				M3Log.lg.info("Error "+e.getMessage()+" ");
			}catch(MyException e){
				M3Log.lg.info("Error 2 "+e.getMessage()+" ");
			}finally{
				M3Log.lg.info("End "+System.lineSeparator());
			}
		}
		//TODO try with resources have close method in autoclosable class throw an exception and show off e.getSuppressed();, pretty advanced and niche stuff
		//TODO example of re throw ing inside a catch
		//TODO show refactor interface method throws
	}
}

//No point defining the default constructor //MyException(){}
class MyException extends Exception{}

class MyException2 extends Exception{
	MyException2(String s){
		super(s);
	}
}

class AClass{
	private double answer=0;
	AClass(int constructorArg) throws MyException2{
		method1(constructorArg,4.2);
	}
	void method1(int arg1,double arg2) throws MyException2{
		//call another method that excepts
		answer=subMethod1(arg1+arg2);
	}
	//stupid code is allowed, this method never returns float
	float subMethod1(double arg2) throws MyException2{
		throw new MyException2("an error happened");
	}
}