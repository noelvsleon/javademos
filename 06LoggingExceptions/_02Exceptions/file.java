package _02Exceptions;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class file {

	public static void main(String[] args) throws IOException {
		String wkspc=System.getProperty("user.dir");
		String tmpDirPath=wkspc+"\\..\\tmpDir";
		File tmpDir=new File(tmpDirPath);
		tmpDir.mkdir();
		var files=new ArrayList<>(); //Run program, get the J10 error, then move this line down to line 22, and it will compile.
		for(int i=1;i<=10;i++){
			File f=new File(tmpDirPath+"\\"+i+".txt");
			f.createNewFile();
		}

		int j=1; //deliberate error
		for(int i=1;i<1000000;i++){
			System.out.println(i);
			FileReader fr=new FileReader(tmpDirPath+"\\"+j+".txt");
			fr.read(); //fr.sd.lock.lock.fd.(handle,err,in,out,closed)
			files.add(fr);
			j++;
			if(j==10){
				j=1;
			}
		}
		//on linux this should error due to too many files
		//on windows it will run out of heap space
		/*
		Linux:
		4093
		Exception in thread "main" java.io.FileNotFoundException: /tmp/cswks\..\tmpDir\7.txt (Too many open files)
		        at java.base/java.io.FileInputStream.open0(Native Method)
		        at java.base/java.io.FileInputStream.open(FileInputStream.java:220)
		        at java.base/java.io.FileInputStream.<init>(FileInputStream.java:158)
		        at java.base/java.io.FileInputStream.<init>(FileInputStream.java:113)
		        at java.base/java.io.FileReader.<init>(FileReader.java:58)
		        at file.main(file.java:24)

		Windows:
		230711
		Exception in thread "main" java.lang.OutOfMemoryError: Java heap space

		 */
	}

}
