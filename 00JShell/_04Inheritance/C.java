package _04Inheritance;

public class C extends A{
	  int m(){ //Override
		    return super.m()+1;
		  }
		  int m(int a1,int a2){ //Overload
		    return m()+a1*a2;
		  }
}