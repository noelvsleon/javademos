package _04Inheritance;

public class Main{
	public static void main(String[] args){
		System.out.println(new A().va);
		System.out.println(new B().va);
		System.out.println(new B().vb);
		System.out.println(new A().m());
		System.out.println(new B().m());
		C c=new C();
		System.out.println(c.va);
		System.out.println(c.m());
		System.out.println(c.m(5,6));
	}
}
