package _03Statics;

public class Shares{
	String ric;
	Trade[] trades;
	Shares(String ric,Trade[] trades){
		this.ric=ric;
		this.trades=trades;
	}
	Stats calcStats(){
		return new Stats(trades[0].price,trades[trades.length-1].price,Trade.vwap(trades));
	}
}
