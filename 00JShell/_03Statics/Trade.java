package _03Statics;

public class Trade{
	double price;
	int size;
	static double tax=0.005; // SDRT
	Trade(double price,int size){
		this.price=price;
		this.size=size;
	}
	static double vwap(Trade[] trades){
		double totalShares=0;
		double totalNotional=0;
		for(Trade ct:trades){ //ct=Current Trade
			totalShares+=ct.size;
			totalNotional+=ct.size*ct.price;
	   }
		return (1+tax)*totalNotional/totalShares;
	}
}
