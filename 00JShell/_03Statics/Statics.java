package _03Statics;

public class Statics{
	public static void main(String[] args){
		Trade[]trades=new Trade[3];
		trades[0]=new Trade(1.5,10);
		trades[1]=new Trade(1.6,100);
		trades[2]=new Trade(1.65,234);
		System.out.println(Trade.vwap(trades));
		System.out.println(Trade.tax);
		System.out.println(trades[0].price);
		Shares s=new Shares("VOD.L",trades);
		Stats st=s.calcStats();
		System.out.println(st.open);
		System.out.println(st.close);
		System.out.println(st.vwap);
	}
}
