package _05Scope;

public class C1{
	  int member=C2.staticProperty*2;
	  static int staticProperty=3;
	  void method(int parameter){
	    int local=4;
	    C2 c=new C2(local,parameter,this.member,staticProperty,C2.staticProperty);
	  }

}
