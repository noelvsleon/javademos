package _02Classes;

public class Classes{
	public static void main(String[] args) throws Exception{
		Shares vod=new Shares();
		System.out.println(vod.ric);
		System.out.println(vod.sharePrice);
		vod.ric="VOD.L";
		vod.sharePrice=1.38;
		Shares atrazeneca=new Shares();
		Shares2 vod2=new Shares2("VOD.L",1.38);
		System.out.println(vod2.ric);
		System.out.println(vod2.sharePrice);
		Shares2 bmw=new Shares2("BMW.DE",69.9);
		Shares3 bmw2=new Shares3("BMW.DE",69.9);
		Shares4 bmw3=new Shares4("BMW.DE",69.9);
		System.out.println(bmw2.toString());
	}
}

class Shares{
	String ric; // Property/Member/Attribute
	double sharePrice;
}

class Shares2{
	String ric;
	double sharePrice;
	Shares2(String ric,double sharePrice){
		this.ric=ric; // this. disambiguates between Properties and Arguments
		this.sharePrice=sharePrice;
	}
}

class Shares3{
	  String ric;
	  double sharePrice;
	  Shares3(String ric,double sharePrice){
	    this.ric=ric; 
	    this.sharePrice=sharePrice;
	  }
	  public String toString(){ //A method
	    return ric+"="+sharePrice;
	}
}

class Shares4{
	String ric;
	double sharePrice;
	Shares4(String ric,double sharePrice) throws Exception{
		if(ric==null) {
			throw new Exception("ric cannot be null");
		}
		this.ric=ric;
		this.sharePrice=sharePrice;
	}
	public String toString(){ // A method
		return ric+"="+sharePrice;
	}
}