package basics;

import _01Logging.M3Log;

//System Under Test
public class Student{
	private String name;
	public String getName(){
		return name;
	}

	private BrainPowerScale brainpower;
	public BrainPowerScale getBrainpower(){
		return brainpower;
	}

	private char grade='F';
	public char getGrade(){
		return grade;
	}

	/**
	 * 
	 * @param name
	 * @param brainpower
	 * @param grade - A-F
	 */
	public Student(String name,BrainPowerScale brainpower) {
		this.name=name;
		this.brainpower=brainpower;
	}
	
	public void study() {
		if(brainpower==BrainPowerScale.Peanut) {
			brainpower=BrainPowerScale.Average;
			M3Log.lg.info("Congrats, you are now smarter than a T-Rex");
		}else {
			M3Log.lg.info("Already at Average or Brain, and more study won't take you higher!");
		}
	}
	
	public void test(boolean pass) {
		grade=(char)((pass?-1:1)+(int)grade);
	}
}

enum BrainPowerScale{Peanut,Average,Brain};