package _01;

public class Branches {

	public static void main(String[] args) {
		if(3<4){
			System.out.print("What ");
		}
		if(7>6)System.out.print("is ");
		if(6>7){
			System.out.println("madness?");
		}else{
			System.out.print("the ");
		}
		if(21<2){
			System.out.println("insanity");
		}else if(21*1==42){
			System.out.println("crazyness");
		}else if (21*2==42) {
			System.out.print("meaning ");
		}
		String conditionalValue="of";
		switch(conditionalValue){ //java 7 strings in switches
			case "What":System.out.print("imperfection");
			case "is":System.out.print("imperfection");
			case "the":System.out.print("imperfection");
			case "meaning":System.out.print("imperfection");
			case "of":System.out.print("of ");
			case "life":System.out.print("life");
				break;
			case "?":System.out.println("?");
		}
		System.out.println("?");
		
		//TODO default:;
		//TODO ternary
		
	}

}
