package _01;

public class Looping{
	public static void main(String[] args){
		// DEBUG ME
		for(int i=0;i<11;++i){
			System.out.println(i);
		}
		int i;
		i=0;
		/* for(;;){ 
		 * 	//flooding is bad 
		 * 	i--; 
		 * }
		 */
		// uncomment below, see the unreachable error, comment out infinite loop
		int[] numbers={1,2,3,4,5,6,7,8,9,10};
		for(int item:numbers){
			System.out.println("item is: "+item);
		}
		boolean foundIt=false;
		int searchfor=4;
		for(i=0;i<numbers.length;++i){
			System.out.println("looking for "+searchfor+" found "+numbers[i]);
			if(numbers[i]==searchfor){
				foundIt=true;
				System.out.println("found it!");
				break;
			}
		}
	}
}
