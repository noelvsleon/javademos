package _01;

public class Loopingbreak {
	public static void main(String[] args) {
		// autoformat ctrl+shift+f; context>source>format
		// never write code like this, also eclipse won't step into loops written all on
		// one line in debug mode
		int searchfor = 6;
		boolean foundItFlag = false;
		int[][] arrayOfInts = { { 1, 2 }, { 6, 7 }, { 21, 42 } };
		// can label loops but will error if try to label variable declaration
		search: for (int i = 0; i < arrayOfInts.length; i++){for(int j=0;j<arrayOfInts[i].length; j++){if(arrayOfInts[i][j]==searchfor){foundItFlag = true;break search;}}}
		// DEBUG ME
		//Show Ctrl+shift+i(inspect) on the arrayOfInts[i] 
		//Show Ctrl+shift+i(inspect) on the arrayOfInts[i][j] 

		// undo changes using team revert....TODO
	}
}
