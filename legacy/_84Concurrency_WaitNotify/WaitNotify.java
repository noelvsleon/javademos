package _84Concurrency_WaitNotify;
public class WaitNotify{
	public static void main(String[] args) throws InterruptedException{
		MyRunnable1 run1=new MyRunnable1();
		Thread one=new Thread(run1);
		one.start();
		Thread.sleep(1000);
		synchronized(run1.lock) {
			run1.lock.notify();
		}
		Thread.sleep(1000);
		synchronized(run1.lock) {
			run1.lock.notify();
		}
	}
}

class MyRunnable1 implements Runnable{
	Object lock=new Object();
	public void run() {
		System.out.println("start");
			synchronized(lock) {
				try{
				lock.wait();
				}catch(InterruptedException e){
				}
			}
		System.out.println("middle");
			synchronized(lock) {
				try{
					lock.wait();
				}catch(InterruptedException e){
			}
		}
		System.out.println("finish");
	}
}

