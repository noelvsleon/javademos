package networking;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

//Server to accumulate integers sent from multiple clients
public class Server{
	public static void main(String[] args) throws IOException {
		System.out.println("test");
		long total=0;
		List<Client>clients=new ArrayList<>();
		//Listen on port 3000
		ServerSocketChannel ss=ServerSocketChannel.open().bind(new InetSocketAddress("localhost",3000));
		ss.configureBlocking(false);
		//TODOC
		while(true) {
			//Check for client connection
			SocketChannel newClient=ss.accept();
			if(newClient!=null) {
				newClient.configureBlocking(false);
				clients.add(new Client(newClient));
			}
			for(int i=0;i<clients.size();++i) {
				SocketChannel clientSocket=clients.get(i).s;
				ByteBuffer clientBuffer=clients.get(i).b;
				int numBytesRead=clientSocket.read(clientBuffer);
				//-1,0,N
				if(numBytesRead==-1) {
					clients.remove(i);
					--i;
				}else if(numBytesRead!=0) {
					clientBuffer.flip();
					while(4<=clientBuffer.remaining()) {
						total+=clientBuffer.getInt();
					}
					clientBuffer.flip();
					System.out.println("Total is now "+total+" because we got an int from "+clientSocket);
				}
				
			}
		}
	}
}

class Client {
	static final int defaultBuffersize=10000;
	SocketChannel s;
	ByteBuffer b;
	Client(SocketChannel s){
		this.s=s;
		b=ByteBuffer.allocate(defaultBuffersize);
	}
}