package javademos;

import java.time.LocalDateTime;
import java.util.concurrent.CopyOnWriteArrayList;

public class Scratch {

    String ticker = "VOD", exchange = "DAVE";

    String getRIC() {
	return ticker + "." + exchange;
    }

    public static void main(String[] args) {
	System.out.println(System.currentTimeMillis());
	System.out.println(System.nanoTime());
	LocalDateTime.now();
	int[] i = new int[5000000000L];
	System.out.println(i.getClass().getName());
	
	Integer a=new Integer(4);
	Integer a=Integer.valueOf(4);
	//
	// Coffee black=new Coffee();
	// Coffee black=Coffee.giveMeCoffeeFromFactory();
	// black.setMilk(true);
	// List<Integer>egArrayList=new ArrayList<>();
	// egArrayList.add(7);
	// System.out.println();
    }

}

class Coffee {
    private boolean milk;

    private Coffee() {
    }

    public static Coffee giveMeCoffeeFromFactory() {
	return new Coffee();
    }

    void setMilk(boolean milk) {
	this.milk = milk;
    }

    boolean getMilk() {
	return milk;
    }
}