package _01TCPSimple;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import _01Logging.M3Log;

public class Client{

	public static void main(String[] args){
		try{
			Socket server=new Socket();
			InetSocketAddress serverAddress=new InetSocketAddress(1234);
			M3Log.lg.info("Connecting to server on port "+serverAddress);
			server.connect(serverAddress);
			BufferedWriter out=new BufferedWriter(new OutputStreamWriter(server.getOutputStream()));
			String ourMsg="Hello, how are you?\n";
			M3Log.lg.info("Sending message to server:"+ourMsg);
			out.write(ourMsg);
			out.flush();
			BufferedReader in=new BufferedReader(new InputStreamReader(server.getInputStream()));
			M3Log.lg.info("Wait for message");
			String theirMsg=in.readLine();
			M3Log.lg.info("Received message from server:"+theirMsg);
			M3Log.lg.info("Disconnect");
			server.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
