package _01TCPSimple;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import _01Logging.M3Log;

public class Server{

	public static void main(String[] args){
		try(ServerSocket ss=new ServerSocket()){
			InetSocketAddress ourAddress=new InetSocketAddress(1234);
			M3Log.lg.info("Binding port "+ourAddress);
			ss.bind(ourAddress);
			while(true) {
				M3Log.lg.info("Wait for a client to connect");
				Socket currentClient=ss.accept();
				M3Log.lg.info("Client connected from port "+currentClient.getPort());
				BufferedReader in=new BufferedReader(new InputStreamReader(currentClient.getInputStream()));
				M3Log.lg.info("Wait for a message");
				String theirMsg=in.readLine();
				M3Log.lg.info("Received message from client:"+theirMsg);
				BufferedWriter out=new BufferedWriter(new OutputStreamWriter(currentClient.getOutputStream()));
				String ourMsg="Fine, thanks.\n";
				M3Log.lg.info("Sending message to client:"+ourMsg);
				out.write(ourMsg);
				out.flush();
				currentClient.close();
			}
		}catch(BindException e) {
			System.out.println("TODO retry loop around bind");
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}

	
}
