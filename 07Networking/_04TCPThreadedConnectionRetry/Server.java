package _04TCPThreadedConnectionRetry;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import _01Logging.M3Log;

public class Server{
	public static void main(String[] args){
		try(ServerSocket ss=new ServerSocket()){
			InetSocketAddress ourAddress=new InetSocketAddress(1234);
			M3Log.lg.info("Binding port "+ourAddress);
			ss.bind(ourAddress);
			while(true) {
				M3Log.lg.info("Wait for a client to connect");
				Socket currentClient=ss.accept();
				new ClientHandler(currentClient).start();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}

class ClientHandler extends Thread{
	static final int sym=1;
	static final int side=2;
	static final int size=3;
	static Map<String,Double> prices=Map.of(
			"VOD.L",1.38
			,"BT.L",2.38
			,"CNA.L",1.25);
	private Socket client;
	BufferedReader in;
	BufferedWriter out;
	boolean running=true;
	ClientHandler(Socket currentClient){
		this.client=currentClient;
	}
	@Override public void run() {
		int clientId=client.getPort();
		M3Log.lg.info("Client connected from port "+clientId);
		try{
			in=new BufferedReader(new InputStreamReader(client.getInputStream()));
			out=new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
			while(running) {
				M3Log.lg.info("Wait for a message");
				String theirMsg=in.readLine();
				if(theirMsg==null) {
					logout();
					continue;
				}
				M3Log.lg.info("Received message from client:"+clientId+" "+theirMsg);
				if(theirMsg.startsWith("Order")) {
					processOrder(theirMsg);
				}else if(theirMsg.equals("logout")){
					logout();
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	void processOrder(String order) throws IOException {
		String[]orderParts=order.split(",");

		String response="Fill,"+orderParts[sym]+","+orderParts[size]+","+(Integer.parseInt(orderParts[size])*prices.get(orderParts[sym]));
		M3Log.lg.info("Sending message to client:"+response);
		out.write(response+"\n");
		out.flush();
	}
	void logout() {
		try{
			client.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		running=false;
	}
}
	
/*
}catch(BindException e) {
System.out.println("TODO retry loop around bind");
*/