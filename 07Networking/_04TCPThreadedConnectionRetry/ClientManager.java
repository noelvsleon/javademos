package _04TCPThreadedConnectionRetry;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Random;
import _01Logging.M3Log;

public class ClientManager{
	public static void main(String[] args){
		for(int i=0;i<3+new Random().nextInt(10);++i){
			new Client().start();
		}
	}
}

class Client extends Thread{
	@Override public void run(){
		Random rand=new Random();
		String sym=new String[]{"VOD.L","BT.L","CNA.L"}[rand.nextInt(3)];
		String side=new String[]{"Buy","Sell"}[rand.nextInt(2)];
		InetSocketAddress serverAddress=new InetSocketAddress(1234);
		M3Log.lg.info("Connecting to server on port "+serverAddress);

		Socket server=new Socket();
		int timeout[]={1000,2000,5000,10000}; // exponential backoff
		int timeoutIndex=0;
		int counter=0;
		int maxRetries=10;
		while(counter<maxRetries&&!server.isConnected()){
			try{
				server.connect(serverAddress,timeout[timeoutIndex]);
			}catch(IOException e){
				e.printStackTrace();
			}
			if(timeoutIndex<3)
				timeoutIndex++;
			counter++;
		}
		if(!server.isConnected()){
			M3Log.lg.info("could not connect after "+maxRetries+". Oh dear!! Exiting.");
			try{
				server.close(); //Java is sometimes really dumb! Avoid impossible resource leak to keep compiler warning free.
			}catch(IOException e){
				e.printStackTrace();
			}
			return;
		}
		try{
			BufferedWriter out=new BufferedWriter(new OutputStreamWriter(server.getOutputStream()));
			BufferedReader in=new BufferedReader(new InputStreamReader(server.getInputStream()));
			for(int i=0;i<1+rand.nextInt(20);++i){
				String order="Order,"+sym+","+side+","+100*1+rand.nextInt(20); //TODO more dynamic BT.L, CNA.L, Buy vs Sell, random sizes,
				out.write(order+"\n");
				out.flush();
				M3Log.lg.info("Wait for message");
				String theirMsg=in.readLine(); //TODO do something with answer
				M3Log.lg.info("Received message from server:"+theirMsg);
			}
		}catch(IOException e){
			e.printStackTrace();
		}finally {
			try{
				server.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}
