package _07TCP_NIO_NonBlocking;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Random;
import _01Logging.M3Log;

public class NBClientManager{
	public static void main(String[] args){
		ArrayList<Client>clients=new ArrayList<Client>();
		int numClients=3+new Random().nextInt(10);
		for(int i=0;i<numClients;++i){
			clients.add(new Client(i));
		}
		while(clients.size()>0){
			for(int i=clients.size()-1;i>=0;--i){
				Client client=clients.get(i);
				if(client.connected){
					if(client.read){
						client.read();
					}else{
						client.write();
					}
				}else{
					try{
						client.connect();
					}catch(ClientConnectException_GaveUpTrying e){
						M3Log.lg.error(e.getMessage());
						clients.remove(i);
					}
				}
			}
		}
	}
}
@SuppressWarnings("serial") class ClientConnectException_GaveUpTrying extends Exception{}

class Client{
	static InetSocketAddress serverAddress=new InetSocketAddress(1234);
	static final Random rand=new Random();
	static final String[] syms=new String[]{"VOD.L","BT.L","CNA.L"};
	static final String[] sides=new String[]{"Buy","Sell"};
	static final int timeout[]={1000,2000,5000,10000}; // exponential backoff
	static final int maxRetries=10;
	String sym;
	String side;
	int timeoutIndex=-1;
	int counter=0;
	boolean connected=false;
	boolean read=false;
	boolean partialWrite=false;
	ByteBuffer bytesin=ByteBuffer.allocate(1024);
	ByteBuffer bytesout=ByteBuffer.allocate(1024);
	SocketChannel s;
	int id;
	Client(int id){
		this.id=id;
		sym=syms[rand.nextInt(3)];
		side=sides[rand.nextInt(2)];
	}
	void connect() throws ClientConnectException_GaveUpTrying{
		if(timeoutIndex<3)
			++timeoutIndex;
		counter++;
		M3Log.lg.info(id+"/Connecting to server on port "+serverAddress+" attempt #"+counter);

		try{
			s=SocketChannel.open();
			s.configureBlocking(false);
			s.connect(serverAddress);
			connected=s.finishConnect();
		}catch(ConnectException e){
		}catch(IOException e){
		}
		if(connected){
			return;
		}
		if(counter==maxRetries){
			M3Log.lg.error("could not connect after "+maxRetries+". Oh dear!! Exiting.");
			throw new ClientConnectException_GaveUpTrying();
		}
		//Explain second entry to this method and the deliberate error with not calling finishConnect but just opening a new connection
	}

	void write(){
		if(!partialWrite){
			String order="Order,"+sym+","+side+","+100*rand.nextInt(20);
			bytesout.put((order+"\n").getBytes());
			bytesout.flip();
		}
		try{
			int num=s.write(bytesout);
		}catch(IOException e){
			e.printStackTrace();
		}
		if(bytesout.hasRemaining()){
			partialWrite=true;
		}else{
			bytesout.clear();
			partialWrite=false;
			read=true;
		}
	}
	void read(){
		M3Log.lg.info("Wait for message");
		int num;
		try{
			num=s.read(bytesin);
			if(num==0){
				return;
			}
			bytesin.flip();
			String theirMsg="";
			byte currentByte=0x0; //Another impossible compilation error avoidance
			for(int i=0;i<num;++i){
				currentByte=bytesin.get();
				theirMsg+=(char)currentByte;
			}
			bytesin.clear();
			if(currentByte=='\n'){
				M3Log.lg.info("Received message from server:"+theirMsg);
				read=false;
			}else{
				M3Log.lg.info("Received partial message from server:"+theirMsg);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}

/*
 * //non blocking client public class NBClient { public static void
 * main(String[]args) throws InterruptedException{ SocketAddress a=new
 * InetSocketAddress("localhost",3000); try { SocketChannel s; do{
 * s=SocketChannel.open(); s.configureBlocking(false); s.connect(a); try{
 * while(!s.finishConnect()){ Thread.sleep(100); //waiting sleep }
 * }catch(ConnectException e){
 * System.out.println("Error connecting, going to retry"); e.printStackTrace();
 * Thread.sleep(3000); //don't spam->sleep } }while(!s.isConnected()); byte[]
 * b={1,2,6,7,21,42}; for(int i=0;i<b.length;i++){ b[i]+=48; //partial ascii
 * shift, for more helpful output on the server, 21 and 42 will translate to EZ
 * which is fine for our purposes } String myName="DavidHodgins"; ByteBuffer
 * bytes=ByteBuffer.allocate(42); bytes.put(b); bytes.put(myName.getBytes());
 * bytes.flip(); //prepare it for writing, set limit to position, position to 0
 * etc while(bytes.hasRemaining())s.write(bytes); s.close(); //if we don't close
 * the connection properly, the other side gets a forced close exception(when
 * this program finishs) and can't read the buffer without gymnastics } catch
 * (IOException e) { e.printStackTrace(); } } }
 */