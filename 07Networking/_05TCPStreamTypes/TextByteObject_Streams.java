package _05TCPStreamTypes;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import _01Logging.M3Log;
public class TextByteObject_Streams {
	public static void main(String[] args) {
		Server s=new Server();
		s.start();
		s.setName("server");
		Client c=new Client();
		c.start();
		c.setName("client");
	}
}

class Client extends Thread{
	@SuppressWarnings("resource")
	@Override public void run(){
		try {
			M3Log.lg.info("connecting to text");
			Socket text=new Socket("localhost",3000);
			M3Log.lg.info("connecting to bytes");
			Socket bytes=new Socket("localhost",3001);
			M3Log.lg.info("connecting to objects");
			Socket objects=new Socket("localhost",3002);

			M3Log.lg.info("send text");
			PrintWriter textout=new PrintWriter(text.getOutputStream(),true);
			textout.println("i am client");
			
			M3Log.lg.info("send bytes");
			DataOutputStream bytesout=new DataOutputStream(bytes.getOutputStream());
			for(int i=0;i<256;i++){
				bytesout.write(i);
			}
			M3Log.lg.info("close bytes");
			bytesout.close();
			
			M3Log.lg.info("send object");
			ObjectOutputStream objectsout=new ObjectOutputStream(objects.getOutputStream());
			objectsout.writeObject(new Fruit("apple"));
			objectsout.writeObject(new Fruit("orange"));
			
			M3Log.lg.info("read text");
 			BufferedReader textin=new BufferedReader(new InputStreamReader(text.getInputStream()));
			M3Log.lg.info("server sent:"+textin.readLine());

			M3Log.lg.info("read object");
			ObjectInputStream objectsin=new ObjectInputStream(objects.getInputStream());
			M3Log.lg.info("Dog is "+((Dog)objectsin.readObject()).size());
			M3Log.lg.info("Dog is "+((Dog)objectsin.readObject()).size());
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		M3Log.lg.info("done");
	}
}

class Server extends Thread{
	@SuppressWarnings("resource")
	@Override public void run(){
		M3Log.lg.info("server listening");
		try {
			Socket text=new ServerSocket(3000).accept();
			M3Log.lg.info("client connected to text");
			Socket bytes=new ServerSocket(3001).accept();
			M3Log.lg.info("client connected to bytes");
			Socket objects=new ServerSocket(3002).accept();
			M3Log.lg.info("client connected to objects");

			M3Log.lg.info("read text");
			BufferedReader in=new BufferedReader(new InputStreamReader(text.getInputStream()));
			M3Log.lg.info("client sent:"+in.readLine());
			
			M3Log.lg.info("read bytes");
			DataInputStream bytesin=new DataInputStream(bytes.getInputStream());
			int current;
			int i=0;
			while(-1!=(current=bytesin.read())){
				System.out.print(current+" is "+(char)current+"\t");
				if(i==10){
					i=0;
					System.out.println();
				}
				i++;
			}
			M3Log.lg.info("received end of byte stream, got -1");
			
			M3Log.lg.info("read object");
			ObjectInputStream objectsin=new ObjectInputStream(objects.getInputStream());
			M3Log.lg.info(((Fruit)objectsin.readObject()).toString());
			M3Log.lg.info(((Fruit)objectsin.readObject()).toString());

			M3Log.lg.info("send text");
			PrintWriter textout=new PrintWriter(text.getOutputStream(),true);
			textout.println("i am server");
			
			M3Log.lg.info("try to send bytes");
			DataOutputStream bytesout=new DataOutputStream(bytes.getOutputStream());
			try{
				bytesout.writeBytes("The meaning of life, the universe and everything is:");
				bytesout.write(42);
				bytesout.writeChar('\n');
			}catch(SocketException e){
				M3Log.lg.info("got error, as expected as should be closed");
				e.printStackTrace();
			}
			
			M3Log.lg.info("send object");
			ObjectOutputStream objectsout=new ObjectOutputStream(objects.getOutputStream());
			objectsout.writeObject(new Dog("German"));
			objectsout.writeObject(new Dog("Yorkshire"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
class Fruit implements Serializable{
	private String type;
	private boolean hasStem;
	Fruit(String type){
		this.type=type;
		if(type=="apple"){
			hasStem=true;
		}
	}
	public String toString(){
		String result=super.toString()+"\n"
				+"\tClass:"+this.getClass().getName()+"\n"
				+"\tParent:"+this.getClass().getSuperclass().getName()+"\n"
				+"\tFields\n"
				+"\t\tString type=\""+type+"\"\n"
				+"\t\tboolean hasStem="+hasStem+"\n\tReflection:\n";
		for(Field f:this.getClass().getDeclaredFields()){
			result+="\t"+f.getType().getName()+" "+f.getName()+"\n";
		}
		return result;
	}
}
class Dog implements Serializable{
	private String breed;
	Dog(String breed){
		this.breed=breed;
	}
	public String size(){
		if(breed=="German"){
			return "big";
		}else if(breed.equals("Alsation")){
			return "Large";
		}else{
			return "small";
		}
	}	
}
