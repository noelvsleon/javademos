package _08TCP_NIO_NonBlocking_Objects_Selector;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.Map;
import org.apache.commons.lang3.SerializationUtils;
import _01Logging.M3Log;
public class ServerObjectsSelector {
	static ServerSocketChannel ss;
	static java.nio.channels.Selector selector;
	static final Map<String,Double> prices=Map.of(
			"VOD",1.38
			,"BT",2.38
			,"CNA",1.25
			,"DSY",15193.);
	public static void main(String[]args){
		try{
			selector=java.nio.channels.Selector.open();
			ss = ServerSocketChannel.open().bind(new InetSocketAddress(1234));
			M3Log.lg.info("Bound to port 1234");
			ss.configureBlocking(false);
			ss.register(selector,SelectionKey.OP_ACCEPT);
		}catch(IOException e){ //TODO retry loop for bind, the others errors don't seem possible
			e.printStackTrace();
			System.exit(1);
		}
		messageHandler();
	}
	static void messageHandler() {
		while(true) {
			try{
				selector.select(); //blocks
			}catch(IOException e){
				e.printStackTrace();
				return;
			} 
			var keyIterator=selector.selectedKeys().iterator();
			while(keyIterator.hasNext()){
				SelectionKey key=keyIterator.next();
				keyIterator.remove();
				if(key.isAcceptable()) {
					SocketChannel client;
					try{
						client=ss.accept();
						client.configureBlocking(false);
					}catch(IOException e){ //doesn't seem possible
						e.printStackTrace();
						return;
					}
					try{
						client.register(selector,SelectionKey.OP_READ,new ClientState());
						M3Log.lg.info("Client connected from "+client.socket().getPort());
					}catch(ClosedChannelException e){ //doesn't seem possible
						e.printStackTrace();
						return;
					}
				}else if(key.isReadable()) {
					ClientState state=(ClientState)key.attachment();
					int num;
					SocketChannel s;
					try{
						state.numRead+=num=(s=(SocketChannel)key.channel()).read(state.rb);
					}catch(IOException e){
						e.printStackTrace();
						return;
					}
					if(num==-1) {
						key.cancel();
						try{
							key.channel().close();
						}catch(IOException e){
							e.printStackTrace();
						} 
						continue;
					}
					state.rb.flip();
					if(state.objectSize==0) {
						state.objectSize=state.rb.getInt();
					}
					int clientPort=s.socket().getPort();
					if(state.numRead==state.objectSize+Integer.BYTES) {
						Order o=SerializationUtils.deserialize(Arrays.copyOfRange(state.rb.array(), state.rb.position(), state.rb.position()+state.objectSize));
						state.rb.clear();
						state.objectSize=0;
						state.numRead=0;
						Fill f=new Fill(o,prices.get(o.sym));
						try{
							byte[]msg=SerializationUtils.serialize(f);
							M3Log.lg.info("Sending Fill "+f+" to client "+clientPort+" corresponding to order "+o);
							s.write(state.wb.putInt(msg.length).put(msg).flip());
							state.isWriting=state.wb.hasRemaining();
							if(state.isWriting) {
								M3Log.lg.warn("Buffer full for client "+clientPort+" will try to send the rest of the Fill later");
								s.register(selector,SelectionKey.OP_WRITE,state);
							}else {
								state.wb.clear();
							}
						}catch(IOException e){
							e.printStackTrace();
							return;
						}
					}else {
						M3Log.lg.warn("Read incomplete message for client "+clientPort+" will try to read the rest later");
						state.rb.flip(); //TODO test
					}
				}else if(key.isWritable()) {
					ClientState state=(ClientState)key.attachment();
					try{
						SocketChannel client=(SocketChannel)key.channel();
						client.write(state.wb);
						if(!state.wb.hasRemaining()) {
							M3Log.lg.info("Sent remaining message to client "+client.socket().getPort());
							state.wb.clear();
							state.isWriting=false;
							key.cancel();
						}
					}catch(IOException e){
						e.printStackTrace();
					}
				}
			}
		}
	}
}
class ClientState{
	int numRead=0; //num bytes read so far
	int objectSize=0;
	boolean isWriting=false;
	ByteBuffer wb=ByteBuffer.allocate(1024);  //somewhere to store data for writing
	ByteBuffer rb=ByteBuffer.allocate(1024);  //somewhere to store data for reading
}