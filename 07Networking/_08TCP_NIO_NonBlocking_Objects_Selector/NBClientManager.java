package _08TCP_NIO_NonBlocking_Objects_Selector;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import org.apache.commons.lang3.SerializationUtils;
import _01Logging.M3Log;

public class NBClientManager{
	public static void main(String[] args){
		ArrayList<Client>clients=new ArrayList<Client>();
		int numClients=3+new Random().nextInt(2);
		for(int i=0;i<numClients;++i){
			clients.add(new Client(i));
		}
		while(clients.size()>0){
			for(int i=clients.size()-1;i>=0;--i){
				Client client=clients.get(i);
				if(client.connected){
					if(client.read){
						try{
							client.read();
						}catch(IOException|UnexpectedChannelClose e){
							e.printStackTrace();
							clients.remove(i);
						}
					}else{
						client.write();
					}
				}else{
					try{
						client.connect();
					}catch(ClientConnectException_GaveUpTrying e){
						M3Log.lg.error(e.getMessage());
						clients.remove(i);
					}
				}
			}
		}
	}
}
@SuppressWarnings("serial") class ClientConnectException_GaveUpTrying extends Exception{}
@SuppressWarnings("serial") class UnexpectedChannelClose extends Exception{}

class Client{
	static InetSocketAddress serverAddress=new InetSocketAddress(1234);
	static final Random rand=new Random();
	static final String[] syms=new String[]{"DSY","VOD","BT","CNA","HSBA"};
	static final String[] exchanges=new String[] {"JSE","LSE","LSE","LSE","LSE"};
	static final int timeout[]={1000,2000,5000,10000}; // exponential backoff
	static final int maxRetries=10;
	String sym;
	String ex;
	boolean side;
	int timeoutIndex=-1;
	int counter=0;
	boolean connected=false;
	boolean read=false;
	boolean partialWrite=false;
	ByteBuffer bytesin=ByteBuffer.allocate(1024);
	ByteBuffer bytesout=ByteBuffer.allocate(1024);
	int numRead=0;
	int objectSize=0;
	SocketChannel s;
	{
		try{
			s=SocketChannel.open();
			s.configureBlocking(false);
		}catch(IOException e){}
	}
	int id;
	Client(int id){
		this.id=id;
		sym=syms[id];
		side=rand.nextBoolean();
		ex=exchanges[id];
	}
	void connect() throws ClientConnectException_GaveUpTrying{
		if(timeoutIndex<3)
			++timeoutIndex; //TODO use the timeout
		counter++;
		M3Log.lg.info(id+"/Connecting to server on port "+serverAddress);

		try{
			if(!s.isConnectionPending()) {
				s.connect(serverAddress);
			}
			connected=s.finishConnect();
		}catch(IOException e){
			try{
				s=SocketChannel.open();
				s.configureBlocking(false);
			}catch(IOException e1){
				e1.printStackTrace();
			}
		}
		if(connected){
			return;
		}
		if(counter==maxRetries){
			M3Log.lg.error("could not connect after "+maxRetries+". Oh dear!! Exiting.");
			throw new ClientConnectException_GaveUpTrying();
		}
	}

	void write(){
		if(!partialWrite){
			Order o=new Order(sym,ex,side,1000);		
			//Need to download "Apache Commons Lang" to use this.
			byte[]b=SerializationUtils.serialize(o);
//			bytesout.put(ByteBuffer.allocate(Integer.BYTES).putInt(b.length).array());
			bytesout.putInt(b.length);
			bytesout.put(b);
			bytesout.flip();
		}
		try{
			int num=s.write(bytesout);
		}catch(IOException e){
			e.printStackTrace();
		}
		if(bytesout.hasRemaining()){
			partialWrite=true;
		}else{
			bytesout.clear();
			partialWrite=false;
			read=true;
		}
	}
	boolean printWaiting=true;
	void read() throws IOException, UnexpectedChannelClose {
		if(printWaiting)M3Log.lg.info("Wait for message");
		int num;
		numRead+=num=s.read(bytesin);
		if(num==0){
			printWaiting=false;
			return;
		}
		if(num==-1) {
			s.close();
			throw new UnexpectedChannelClose();
		}
		printWaiting=true;
		bytesin.flip();
		if(objectSize==0) {
			if(bytesin.remaining()<Integer.BYTES){
				bytesin.flip();
				return;
			}
			objectSize=bytesin.getInt();
		}
		if(numRead==Integer.BYTES+objectSize) { //Assumes we read exactly 1 message at a time
			Fill f=SerializationUtils.deserialize(Arrays.copyOfRange(bytesin.array(), bytesin.position(), bytesin.position()+objectSize));
			M3Log.lg.info("Received Fill:"+f);
			objectSize=0;
			numRead=0;
			bytesin.clear();
			read=false;
		}
	}
}


/*
//non blocking client
public class NBClientManager {
	public static void main(String[]args) throws InterruptedException{
		try {
			SocketChannel s=SocketChannel.open();
			SelectableChannel sc=s.configureBlocking(false);
			InetSocketAddress a=new InetSocketAddress("localhost",3000);
			s.connect(a);
			while(!s.finishConnect()){
				System.out.println("Waiting to connect");
				Thread.sleep(1000);
			}
			//Need to download "Apache Commons Lang" to use this.
			byte[]b=SerializationUtils.serialize(new Dog("German"));
			ByteBuffer bytes=ByteBuffer.allocate(Integer.BYTES+b.length);
			/*get the length of b as an int, and convert to a byte[] but ensure the length is 4 bytes and not smaller
			 * b.length is an int
			 * put it in a byte buffer in order to then be able to call .array effectively casting an int to a byte[]
			 * if we allocate 4 bytes in a byte buffer we solve the problem of small values giving byte/short rather than 4 byte ints
			 * which helps the server to decode our message
			 * 
			 * eg:
			 * byte[] dog=new byte[20];
			 * int dogSize=dog.length; //20
			 * ByteBuffer.allocate(4).putInt(b.lenght).array() -> 4 (0 0 0) because 20 fits in a byte ,but i allocated 4, and .array() just grabs the array
			 * 
			 * Protocol: objectSize(4),object(objectSize) //assumes each dog is a different number of bytes
			 * 
			 * Alternatives: Protocol: sizeSize(1)size(1,2,4)object(size) //more complex :(
			 * Alternatives: same protocol as first one:
			 * byte[]size=new byte[4];
			 * if(dogSize<255){size[0]=dogSize;size[1]=0;size[2]=0;size[3]=0}..... //although 0 is the default value and maybe this would be nicer in practice
			 *//*
			bytes.put(ByteBuffer.allocate(Integer.BYTES).putInt(b.length).array());  
			bytes.put(b);
			bytes.flip();
			while(bytes.hasRemaining())s.write(bytes);
			s.close(); //if we don't close the connection properly, the other side gets a forced close exception(when this program finishs) and can't read the buffer without gymnastics 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}*/