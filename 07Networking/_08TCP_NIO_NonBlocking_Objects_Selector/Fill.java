package _08TCP_NIO_NonBlocking_Objects_Selector;

import java.io.Serializable;
import java.lang.reflect.Field;
//Exchange sends fills to the client
@SuppressWarnings("serial") public class Fill implements Serializable{
	String sym;
	String ex;
	boolean side;
	int size;
	double price;	
	
	public Fill(String sym,String ex,boolean side,int size,double price){
		this.sym=sym;
		this.ex=ex;
		this.side=side;
		this.size=size;
		this.price=price;
	}
	public Fill(Order o,double price) {
		this(o.sym,o.ex,o.side,o.size,price);
	}
	@Override public String toString() {
		String result="";
		result+=super.toString()+"\n"
				+"Class:"+this.getClass().getName()+"\n"
				+"Parent:"+this.getClass().getSuperclass().getName()+"\n"
				+"Fields:\n";
		for(Field f:this.getClass().getDeclaredFields()) {
			try{
				result+=f.getType().getName()+" "+f.getName()+"="+f.get(this);
			}catch(IllegalArgumentException e){
				e.printStackTrace();
			}catch(IllegalAccessException e){
				e.printStackTrace();
			}
		}
		return result;
	}
}