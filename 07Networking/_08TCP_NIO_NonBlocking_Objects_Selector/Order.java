package _08TCP_NIO_NonBlocking_Objects_Selector;

import java.io.Serializable;
//Clients send orders to the exchange, these are all market orders so no price field
@SuppressWarnings("serial") public class Order implements Serializable{
	String sym;
	String ex;
	boolean side;
	int size;
	public Order(String sym,String ex,boolean side,int size){
		this.sym=sym;
		this.ex=ex;
		this.side=side;
		this.size=size;
	}
	public boolean inEuropeanEconomicArea() {
		if(ex=="JSE") {
			return false;
		}else if(ex=="LSE") {
			return true;
		}else {
			return false;
		}
	}
}
