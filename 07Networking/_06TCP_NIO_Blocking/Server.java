package _06TCP_NIO_Blocking;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Map;
import _01Logging.M3Log;

public class Server{
	public static void main(String[] args){
		try{
			ServerSocketChannel ss=ServerSocketChannel.open();
			InetSocketAddress ourAddress=new InetSocketAddress(1234);
			M3Log.lg.info("Binding port "+ourAddress);
			ss.bind(ourAddress);
			while(true) {
				M3Log.lg.info("Wait for a client to connect");
				SocketChannel currentClient=ss.accept();
				new ClientHandler(currentClient).start();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}

class ClientHandler extends Thread{
	static final int sym=1;
	static final int side=2;
	static final int size=3;
	static Map<String,Double> prices=Map.of(
			"VOD.L",1.38
			,"BT.L",2.38
			,"CNA.L",1.25);
	private SocketChannel client;
	ByteBuffer bytes;
	boolean running=true;
	ClientHandler(SocketChannel currentClient){
		this.client=currentClient;
	}
	@Override public void run() {
		int clientId=client.socket().getPort();
		M3Log.lg.info("Client connected from port "+clientId);
		bytes=ByteBuffer.allocate(1024);
		try{
			while(running) {
				M3Log.lg.info("Wait for a message");
				int num=client.read(bytes);
				bytes.flip();
				String theirMsg="";
				byte currentByte;
				while('\n'!=(currentByte=bytes.get())){
					theirMsg+=(char)currentByte;
				}
				bytes.clear();
				M3Log.lg.info("Received message from client:"+clientId+" "+theirMsg);
				if(theirMsg.startsWith("Order")) {
					processOrder(theirMsg);
				}else if(theirMsg.equals("logout")){
					logout();
				}
			}
		}catch(IOException e){
			if(e.getMessage().equals("An existing connection was forcibly closed by the remote host")) {
				M3Log.lg.warn("My code smells, client closed connection but didn't send 'logoff message' TODO FIXME");
			}else {
				e.printStackTrace();
			}
		}
	}
	void processOrder(String order) throws IOException {
		String[]orderParts=order.split(",");

		String response="Fill,"+orderParts[sym]+","+orderParts[size]+","+(Integer.parseInt(orderParts[size])*prices.get(orderParts[sym]));
		M3Log.lg.info("Sending message to client:"+response);
		bytes.put((response+"\n").getBytes());
		bytes.flip();
		client.write(bytes);
		bytes.clear();
	}
	void logout() {
		try{
			client.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		running=false;
	}
}