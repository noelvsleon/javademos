package _06TCP_NIO_Blocking;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Random;
import _01Logging.M3Log;

public class ClientManager{
	public static void main(String[] args){
		for(int i=0;i<3+new Random().nextInt(10);++i){
			new Client().start();
		}
	}
}

class Client extends Thread{
	@Override public void run(){
		Random rand=new Random();
		String sym=new String[]{"VOD.L","BT.L","CNA.L"}[rand.nextInt(3)];
		String side=new String[]{"Buy","Sell"}[rand.nextInt(2)];
		InetSocketAddress serverAddress=new InetSocketAddress(1234);
		M3Log.lg.info("Connecting to server on port "+serverAddress);

		SocketChannel s=null; //.open() throws impossible exceptions so use null
		int timeout[]={1000,2000,5000,10000}; // exponential backoff
		int timeoutIndex=0;
		int counter=0;
		int maxRetries=10;
		// s will be in blocking mode by default
		// There seems to be no way to have a connection timeout in blocking mode, but
		// then, that's what non blocking mode is for
		while(counter<maxRetries&&(s==null||!s.isConnected())){ // short circuit or allows us to have s even though it is not initialised the																	// first time around
			try{
				s=SocketChannel.open(); // have to include new socket connection because, if the connect fails, it
												// closes the channel which can't be reopened
				s.connect(serverAddress);
			}catch(ConnectException e){
				System.out.println(e.toString());
				try{
					Thread.sleep(timeout[timeoutIndex]);
				}catch(InterruptedException e1){
				}
			}catch(IOException e){
				e.printStackTrace();
			}
			if(timeoutIndex<3)
				timeoutIndex++;
			counter++;
		}
		if(s==null||!s.isConnected()){
			M3Log.lg.info("could not connect after "+maxRetries+". Oh dear!! Exiting.");
			return;
		}
		try{
			ByteBuffer bytes=ByteBuffer.allocate(1024);
			for(int i=0;i<1+rand.nextInt(20);++i){
				String order="Order,"+sym+","+side+","+100*1+rand.nextInt(20);
				bytes.put((order+"\n").getBytes());
				bytes.flip(); // change bytes from write mode to read mode, so that channel.write can read it
									// and write the contents to the socket
				// flip->sets limit to position, position to 0
				int num=s.write(bytes); // blocks
				bytes.clear();
				M3Log.lg.info("Wait for message");
				num=s.read(bytes);
				bytes.flip();
				String theirMsg="";
				byte currentByte;
				while('\n'!=(currentByte=bytes.get())){
					theirMsg+=(char)currentByte;
				}
				bytes.clear();
				M3Log.lg.info("Received message from server:"+theirMsg);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}