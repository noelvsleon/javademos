package _03TCPThreaded;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import _01Logging.M3Log;

public class ClientManager{
	public static void main(String[] args){
		for(int i=0;i<3+new Random().nextInt(10);++i){
			new Client().start();
		}
	}
}

class Client extends Thread{
	@Override public void run(){
		Random rand=new Random();
		String sym=new String[]{"VOD.L","BT.L","CNA.L"}[rand.nextInt(3)];
		String side=new String[]{"Buy","Sell"}[rand.nextInt(2)];
		InetAddress serverAddress=null;
		try{serverAddress=InetAddress.getLocalHost();}catch(UnknownHostException e1){}
		M3Log.lg.info("Connecting to server on port "+serverAddress);
		try(Socket server=new Socket(serverAddress,1234)){
			BufferedWriter out=new BufferedWriter(new OutputStreamWriter(server.getOutputStream()));
			BufferedReader in=new BufferedReader(new InputStreamReader(server.getInputStream()));
			for(int i=0;i<1+rand.nextInt(20);++i){
				String order="Order,"+sym+","+side+","+100*1+rand.nextInt(20); //TODO more dynamic BT.L, CNA.L, Buy vs Sell, random sizes,
				out.write(order+"\n");
				out.flush();
				M3Log.lg.info("Wait for message");
				String theirMsg=in.readLine(); //TODO do something with answer
				M3Log.lg.info("Received message from server:"+theirMsg);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}