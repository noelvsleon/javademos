package _02ThreadBasics;

import java.time.LocalDateTime;

public class ThreadBasics{

	public static void main(String[] args) throws InterruptedException{
		MyThreads thread1=new MyThreads();
		MyThreads thread2=new MyThreads();
		MyThreads thread3=new MyThreads();
		System.out.println("Starting 3 threads");
		thread1.start();
		thread2.start();
		thread3.start();
		System.out.println("Waiting for threads to finish");
		thread1.join();
		thread2.join();
		thread3.join();
		System.out.println("Finished");
	}
}

class MyThreads extends Thread{
	@Override
	public void run() {
		for(int i=0;i<10;++i) {
			System.out.println(i+" "+getName()+" "+LocalDateTime.now());
			try{sleep(1);}catch(InterruptedException e){}
		}
	}
}