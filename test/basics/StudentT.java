package basics;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import LogTesting.LogSpy;

class StudentT{
	static Student david;
	@BeforeAll static void setUpBeforeClass() throws Exception{
		david=new Student("David",BrainPowerScale.Peanut);
	}

	@AfterAll static void tearDownAfterClass() throws Exception{
	}

	@BeforeEach void setUp() throws Exception{
	}

	@AfterEach void tearDown() throws Exception{
	}

	@Test void testStudent(){
		Student s=new Student("Fred",BrainPowerScale.Average);
		assert(s.getName().equals("Fred"));
		assert(s.getGrade()=='F');
		assert(s.getBrainpower()==BrainPowerScale.Average);
	}

	@Test void testStudy(){
		//prereq
		assert(david.getBrainpower()==BrainPowerScale.Peanut);
		david.study(); //exercise SUT
		System.out.println(LogSpy.events);
		assert(LogSpy.events.size()==1);
		assert(LogSpy.events.get(0).toString().contains("smarter"));
		assert(david.getBrainpower()==BrainPowerScale.Average);
	}

	@Test void testTest(){
		assert(david.getGrade()=='F');
		david.test(true);
		assert(david.getGrade()=='E');
		david.test(false);
		assert(david.getGrade()=='F');
	}

}
