package _02Classes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.Random;
import org.junit.jupiter.api.Test;

//Shares and Shares2 were just stepping stones to Shares3, so only test Shares3
//Shares3 is package private so important to put the test in the same package
class TestClasses {
	@Test void testConstructor() throws Exception{
		assertEquals(1,Shares4.class.getDeclaredConstructors().length); //"Shouldn't have default constructor"
		Shares4 shr=new Shares4("VOD.L",1.38);
		assert(shr.ric=="VOD.L"); //use == instead of .equals to check for memory leak
		assert(shr.sharePrice==1.38);
	}
	@Test void testToString() throws Exception {
		Shares4 shr=new Shares4("VOD.L",1.38);
		assert(shr.toString().equals("VOD.L=1.38"));
	}
	@Test void testNull(){ //generally testing edge cases is good
		//this is a bad test, I can't imagine a case where you would want to give null, 
		//so we shouldn't test things that can't be traced back to a user requirement
		assertThrows(Exception.class,()->new Shares4(null,1.38));
	}
}
