package inheritance;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import _04ProjectOrig.cat;
public class cattest {
	cat c;
	@Before
	public void setUp()  {
		c=new cat();
	}

	@After
	public void tearDown()  {
	}

	@Test
	public void testToString() {
		assert(c.toString().startsWith("inheritance.cat@"));
	}
	@Test
	public void multipleObjects() {
		cat c2=new cat();
		assert(c!=c2);
	}
	@Test
	public void testEquals() {
		cat c3=new cat();
		assert(c.equals(c3));
	}
}
