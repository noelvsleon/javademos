package inheritance;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import _04ProjectOrig.labrador;
public class labrador42 {
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testLabradorStringByte_1() {
		labrador l=new labrador("Blue", (byte)0);
		assertEquals("Check that the number of limbs was set to 0",0,l.limbs);
	}
	@Test
	public void testLabradorStringByte_2() {
		labrador l=new labrador("Blue", (byte)3);
		assertEquals("Check that the number of limbs was set to 3",3,l.limbs);
		assertEquals("Check that the colour was set to Blue","Blue",l.colour);
	}
	
	@Test
	public void testLabradorString() {
		fail("Not yet implemented");
	}

}
