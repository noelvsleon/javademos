package _07TCP_NIO_NonBlocking;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.jupiter.api.Test;
import LogTesting.LogSpy;
public class TestClient{
	public static String testString="TestString\n"; //This will be given as the answer to server.read() inside Client
	//TODO test constructor
	@Test void testRead() {
		//DEBUGME - step into read() and LogSpy
		//Setup
		Client sut=new Client(42); //System Under Test
		sut.s=new MockSocketChannel();
		sut.read=true;
		//Exercise
		sut.read();
		//Validate
		//Look at LogTester.events
		assertEquals(2,LogSpy.events.size());
		assert(LogSpy.events.get(0).toString().toLowerCase().contains("wait"));
		assert(LogSpy.events.get(1).toString().toLowerCase().contains("received message"));
		assert(LogSpy.events.get(1).toString().contains(testString)); //want both received and teststring to be logged, but not concerned how
		assertFalse("Check if switched to write mode",sut.read);
	}
	//TODO test other branch		LogTester.events.get(1).toString().contains("partial")
}
