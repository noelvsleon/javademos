package _01;

public class ReferenceVSvalue {
	int plusOne(int x){
		x++;
		return x;
	}
	//method overloading
	int plusOne(NumberClass x){
		return x.numberField++;
	}
	public static void main(String[] args) {
		//DEBUG ME
		//using the 'main' class as a real class, i think this is confusing and try to avoid it
		ReferenceVSvalue exampleObject=new ReferenceVSvalue();
		int testValue=0;
		testValue++;
		//STEP INTO
		int res=exampleObject.plusOne(testValue);
		//res and testValue now have different values due to pass by value on primitive
		
		NumberClass testObject=new NumberClass();
		int res2=exampleObject.plusOne(testObject);
		//res2 and testObject.numberField are different variables, but have the same value due to pass by value on object(pass by reference ~ sort of)
	}
}
class NumberClass{
	int numberField=0;
}