package datastructures;

import java.util.TreeSet;

public class TreeSet_{

	public static void main(String[] args){
		//TODO when would use this, all the other demos have real examples?? we could create trades and implement comparable but other structures seem better for this.
		TreeSet<Integer>t=new TreeSet<>();
		t.add(1);
		t.add(2);
		t.add(6);
		t.add(7);
		t.add(21);
		t.add(42);
		t.add(20);
	}
}
