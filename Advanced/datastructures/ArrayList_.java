package datastructures;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
public class ArrayList_{
	public static void main(String[] args){
		//DEBUG ME
		List<Integer>a=new ArrayList<>();
		//Show elementdata
		a.add(42);
		//Show elementdata
		try {
			int i=a.get(3);
		}
		catch(IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		a.size();
		//Only way to determine capacity is via reflection or extending
		System.out.println(getCapacity(a));
		System.out.println(getElement(a,3));
		System.out.println("Finished");
	}
	public static int getCapacity(List a) {
		Field field;
		try{
			field=ArrayList.class.getDeclaredField("elementData");
			field.setAccessible(true);
			return ((Object[])field.get(a)).length;
		}catch(NoSuchFieldException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(SecurityException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IllegalArgumentException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IllegalAccessException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	public static <T>T getElement(List<T> a,int index) {
		Field field;
		try{
			field=ArrayList.class.getDeclaredField("elementData");
			field.setAccessible(true);
			return (T)((Object[])field.get(a))[index];
		}catch(NoSuchFieldException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(SecurityException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IllegalArgumentException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IllegalAccessException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
