package datastructures;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;

public class CopyOnWriteArraySet_{

	public static void main(String[] args){
		CopyOnWriteArraySet<Integer>c=new CopyOnWriteArraySet<>();
		int numIterations=100000;
		double oneSecond=1000000000.;
		long before=System.nanoTime();
		for(int i=0;i<numIterations;++i) {
			c.add(i);
		}
		long after=System.nanoTime();
		long duration=after-before;
		System.out.printf("CopyOnWriteArraySet    populate: %11d %f\n",duration,duration/oneSecond);
		
		TreeSet<Integer>t=new TreeSet<>();
		before=System.nanoTime();
		for(int i=0;i<numIterations;++i) {
			t.add(i);
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("TreeSet                populate: %11d %f\n",duration,duration/oneSecond);
		
		ArrayList<Integer>a=new ArrayList<>();
		before=System.nanoTime();
		for(int i=0;i<numIterations;++i) {
			a.add(i);
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("ArrayList              populate: %11d %f\n",duration,duration/oneSecond);
		
		ArrayList<Integer>b=new ArrayList<>();
		before=System.nanoTime();
		for(int i=0;i<numIterations;++i) {
			synchronized(b){
				b.add(i);
			}
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("ArrayList synchronized populate:%11d %f\n",duration,duration/oneSecond);
		
		int ns=100,ne=numIterations-5000,nm=(numIterations/2)-100;
		int numRemoves=100;
		before=System.nanoTime();
		for(int i=ne;i<numRemoves+ne;++i) {
			c.remove(i);
		}
		for(int i=nm;i<numRemoves+nm;++i) {
			c.remove(i);
		}
		for(int i=ns;i<numRemoves+ns;++i) {
			c.remove(i);
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("CopyOnWriteArraySet   remove: %11d %f\n",duration,duration/oneSecond);

		before=System.nanoTime();
		for(int i=ne;i<numRemoves+ne;++i) {
			t.remove(i);
		}
		for(int i=nm;i<numRemoves+nm;++i) {
			t.remove(i);
		}
		for(int i=ns;i<numRemoves+ns;++i) {
			t.remove(i);
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("TreeSet               remove: %11d %f\n",duration,duration/oneSecond);
		
		before=System.nanoTime();
		for(int i=ne;i<numRemoves+ne;++i) {
			a.remove(i);
		}
		for(int i=nm;i<numRemoves+nm;++i) {
			a.remove(i);
		}
		for(int i=ns;i<numRemoves+ns;++i) {
			a.remove(i);
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("ArrayList             remove: %11d %f\n",duration,duration/oneSecond);
		
		before=System.nanoTime();
		for(int i=ne;i<numRemoves+ne;++i) {
			synchronized(b) {
				b.remove(i);
			}
		}
		for(int i=nm;i<numRemoves+nm;++i) {
			synchronized(b) {
				b.remove(i);
			}
		}
		for(int i=ns;i<numRemoves+ns;++i) {
			synchronized(b) {
				b.remove(i);
			}
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.printf("ArrayList synchronized remove:%11d %f\n",duration,duration/oneSecond);
	}
}
