package datastructures;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.IntStream;

public class TreeMap_{

	static TreeMap<Double,Level> orderBook=new TreeMap<>();
	public static void main(String[] args){
		newOrder(new Order3(LocalDateTime.now(),"Client1","VOD",1.58,2000,'B'));
		newOrder(new Order3(LocalDateTime.now(),"Client1","VOD",1.59,2001,'B'));
		newOrder(new Order3(LocalDateTime.now(),"Client1","VOD",1.57,2002,'B'));
		newOrder(new Order3(LocalDateTime.now(),"Client1","VOD",1.60,2003,'B'));
		newOrder(new Order3(LocalDateTime.now(),"Client1","VOD",1.56,2004,'B'));
		newOrder(new Order3(LocalDateTime.now(),"Client1","VOD",1.59,2005,'B'));
		newOrder(new Order3(LocalDateTime.now(),"Client1","VOD",1.61,2006,'B'));
		for(Entry<Double,Level>e:orderBook.entrySet()){
			System.out.println(e.getKey()+" "+e.getValue());
		}
		TreeMap<Double,String> m=new TreeMap<>();
		m.put(1.58,"V1");
		m.put(1.59,"V2");
		m.put(1.57,"V3");
		m.put(1.60,"V4");
		m.put(1.56,"V5");
		m.put(1.61,"V6");
	}
	static void newOrder(Order3 o) {
		Level c;
		if(null==(c=orderBook.get(o.price))) {
			orderBook.put(o.price,new Level(o));
		}else {
			c.newOrder(o);
		}
	}
}

class Level{
	ArrayList<Order3> o=new ArrayList<>();
	Level(Order3 o){
		this.o.add(o);
	}
	void newOrder(Order3 o) {
		this.o.add(o);
	}
	public String toString() {
		return o.size()+" "+o.stream().mapToInt(e->e.size).sum();
	}
}
class Order3{
	String client;
	String sym;
	double price;
	int size;
	char side;
	LocalDateTime recvtime;

	Order3(LocalDateTime recvtime,String client,String sym,double price,int size,char side){
		this.recvtime=recvtime;
		this.client=client;
		this.sym=sym;
		this.price=price;
		this.size=size;
		this.side=side;
		
	}
}