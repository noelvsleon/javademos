package datastructures;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
public class ConcurrentHashMap_{
	public static void main(String[] args){
		/*USE CASES:
			HashMap good for random access, a good example is managing order status, where different orders update multiple times in a non sequential fashion
			threading
			modify during iteration*/
		
		Map<UUID,Order2>m=new HashMap<>();
		Map<UUID,Order2>mc=new ConcurrentHashMap<>();
		for(int i=0;i<10;++i) {
			m.put(UUID.randomUUID(),new Order2("VOD",1000+i,1.50,'B',Order2.Status.UNACKNOWLEDGED));
			m.put(UUID.randomUUID(),new Order2("BP",2000+i,2.38,'S',Order2.Status.UNACKNOWLEDGED));
			mc.put(UUID.randomUUID(),new Order2("VOD",1000+i,1.50,'B',Order2.Status.UNACKNOWLEDGED));
			mc.put(UUID.randomUUID(),new Order2("BP",2000+i,2.38,'S',Order2.Status.UNACKNOWLEDGED));
		}
		
		//DEBUGME
		System.out.println("########## HashMap #############\n");
		mutation(m);
		System.out.println("\n\n########## ConcurrentHashMap #############\n");
		mutation(mc);
		
		System.out.println("########## Threading HashMap ##########");
		MultiThread t1=new MultiThread(m);
		MultiThread t2=new MultiThread(m);
		t1.start();
		t2.start();
		
		try{
			t1.join();
			t2.join();
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		System.out.println("########## Threading ConcurrentHashMap ##########");
		MultiThread t3=new MultiThread(mc);
		MultiThread t4=new MultiThread(mc);
		t3.start();
		t4.start();
		
		
		System.out.println("Finished");
	}
	
	static void mutation(Map<UUID,Order2> m) {
		try{
			for(Entry<UUID,Order2>o:m.entrySet()) {
				if(0==o.getValue().size%2) {
					System.out.println("Setting order "+o.getKey()+" status to Acknowledged");
					o.getValue().status=Order2.Status.ACKNOWLEDGED;
				}else if(0==o.getValue().size%1001){
					System.out.println("Removing order");
					m.remove(o.getKey());
				}else {
					System.out.println("Order "+o);
				}
			}
		}catch(ConcurrentModificationException e) {
			e.printStackTrace();
		}
	}
	
}
class Order2{
	static enum Status{UNACKNOWLEDGED, ACKNOWLEDGED, PARTIALLY_COMPLETED, COMPLETE, TIMEDOUT};
	String sym;
	int size;
	double price;
	char side;
	Status status;
	
	Order2(String sym, int size, double price, char side,Status status){
		this.sym=sym;
		this.size=size;
		this.price=price;
		this.side=side;
		this.status=status;
	}
	public String toString() {
		return sym+","+size+","+price+","+side+","+status;
	}
}

class MultiThread extends Thread{
	Map<UUID,Order2> m;
	UUID first;
	MultiThread(Map<UUID,Order2> m){
		this.m=m;
		first=(UUID)m.keySet().toArray()[0];
	}
	public void run() {
		System.out.println("In thread");
		//PAUSE HERE
		System.out.println(m.remove(first));
		//Briefly Compare with each other ConcurrentHashMap_ConcurrentHashMap.java ConcurrentHashMap_HashMap.java
		/*Key lines:
		 * 
		 * f = tabAt(tab, i = (n - 1) & hash))
		 * synchronized (f) {
		 * 
		 * tabAt isn't super readable so don't try
		 * See slide for how it works
		 */
		
	}
}
