package profiling;

import java.time.LocalDateTime;

public class ThreadTimeline{

	public static void main(String[] args){
		new Thread(new A(1000)).start();;
		new Thread(new A(2000)).start();
	}

}

class A implements Runnable{
	int sleepTime;
	A(int sleepTime){
		this.sleepTime=sleepTime;
	}
	public void run(){
		try{
			while(true){
				for(int i=0;i<100000000;i++) {
					System.out.println(LocalDateTime.now());
				}
				Thread.sleep(sleepTime);
			}
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
}
