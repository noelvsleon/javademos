package profiling;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.SerializationUtils;

@SuppressWarnings("serial") class C implements Serializable{
}
@SuppressWarnings("serial") class ClassNameOfSensibleSize implements Serializable{
}

public class SerializedObjects{

	@SuppressWarnings("unused") public static void main(String[] args){
		//DEBUG ME
		C o1=new C();
		ClassNameOfSensibleSize o2=new ClassNameOfSensibleSize();
		byte[] o1Bytes;
		//Java default serialization
		try{
			ByteArrayOutputStream bos=new ByteArrayOutputStream();
			ObjectOutput out=new ObjectOutputStream(bos);
			out.writeObject(o1);
			o1Bytes=bos.toByteArray();
		}catch(IOException e){
		}
		
		//Apache serialization - simpler code, but result is the same
		//Serialize ArrayList with unused space
		byte[]b1=SerializationUtils.serialize(o1);
		byte[]b2=SerializationUtils.serialize(o2);
		
		//Serialize array with unused space
		int[]ia=new int[4];
		ia[0]=7;
		List<Integer>al=new ArrayList<>();
		al.add(7);
		
		byte[]b3=SerializationUtils.serialize(ia);
		byte[]b4=SerializationUtils.serialize((ArrayList<Integer>)al); //List<Integer> isn't serializable, so need to be explicit
		al.add(8);
		al.add(9);
		al.add(10);
		byte[]b5=SerializationUtils.serialize((Serializable)al); //List<Integer> isn't serializable, so need to be explicit

		//Profile this bit
		int[]ia2=new int[1000];
		List<Integer>al2=new ArrayList<Integer>(1000);
		for(int i=0;i<1000;i++) {
			ia2[i]=i;
			al2.add(i);
		}
		System.out.println();
	}

}
