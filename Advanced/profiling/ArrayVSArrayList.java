package profiling;

import java.util.ArrayList;
import java.util.List;

public class ArrayVSArrayList{

	static long arrayListPop() {
		long before=System.nanoTime();
		List<Integer> l=new ArrayList<>(6200000);
		for(int i=10;i<5000010;i++) { //If we create too many values, eclipse and VisualVM break.
			l.add(i);
		}
		long after=System.nanoTime();
		long duration=after-before;
		System.out.printf("ArrayList Populate> nanos: %11d seconds:%11f\n",duration,duration/1000000000.);
		return duration;
	}
	static long arrayListRetrieve() {
		List<Integer> l=new ArrayList<>(6200000);
		for(int i=10;i<5000010;i++) { //If we create too many values, eclipse and VisualVM break.
			l.add(i);
		}
		int[]indices=new int[400000];
		for(int i=0;i<100000;i++) {
			indices[i]=i; //start
		}
		for(int i=100000;i<200000;i++) {
			indices[i]=100000+i; //later
		}
		for(int i=200000;i<400000;i++) {
			indices[i]=5000000-i; //end
		}
		int sum=0;
		long before=System.nanoTime();
		for(int i=0;i<indices.length;++i) {
			sum+=l.get(indices[i]);
		}
		long after=System.nanoTime();
		return after-before;
	}
	static long arrayPop() {
		long before=System.nanoTime();
		int[] l=new int[5000000];
		for(int i=10;i<5000010;i++) { //If we create too many values, eclipse and VisualVM break.
			l[i-10]=i;
		}
		long after=System.nanoTime();
		long duration=after-before;
		System.out.printf("[]        Retrieve> nanos: %11d seconds:%11f\n",duration,duration/1000000000.);
		return duration;
	}
	static long arrayRetrieve() {
		int[] l=new int[5000000];
		for(int i=10;i<5000010;i++) { //If we create too many values, eclipse and VisualVM break.
			l[i-10]=i;
		}
		
		//we don't do one operation a million times, we want to deal with large data structures, and many retrievals, then loop a thousand times
		int[]indices=new int[400000];
		for(int i=0;i<100000;i++) {
			indices[i]=i; //start
		}
		for(int i=100000;i<200000;i++) {
			indices[i]=100000+i; //later
		}
		for(int i=200000;i<400000;i++) {
			indices[i]=5000000-i; //end
		}
		int sum=0;
		long before=System.nanoTime();
		for(int i=0;i<indices.length;++i) {
			sum+=l[indices[i]];
		}
		long after=System.nanoTime();
		return after-before;
	}
	public static long alPop() {
		long[] al=new long[] {
				arrayListPop(),
				arrayListPop(),
				arrayListPop(),
				arrayListPop(),
				arrayListPop()
		};
		
		long sum=0;
		for(int i=0;i<al.length;i++) {
			sum+=al[i];
		}
		return sum;
	}
	public static long alRetrieve() {
		long[] al=new long[] {
				arrayListRetrieve(),
				arrayListRetrieve(),
				arrayListRetrieve(),
				arrayListRetrieve(),
				arrayListRetrieve()
		};
		
		long sum=0;
		for(int i=0;i<al.length;i++) {
			sum+=al[i];
		}
		return sum;
	}
	public static long aPop() {
		long[] a=new long[] {
				arrayPop(),
				arrayPop(),
				arrayPop(),
				arrayPop(),
				arrayPop()
		};
		long sum2=0;
		for(int i=0;i<a.length;i++) {
			System.out.printf("[]        Populate> nanos: %11d seconds: %10f\n",a[i],a[i]/1000000000.);
			sum2+=a[i];
		}
		return sum2;
	}
	public static long aRetrieve() {
		long[] a=new long[] {
				arrayRetrieve(),
				arrayRetrieve(),
				arrayRetrieve(),
				arrayRetrieve(),
				arrayRetrieve()
		};
		long sum2=0;
		for(int i=0;i<a.length;i++) {
			System.out.printf("[]        Retrieve> nanos: %11d seconds: %10f\n",a[i],a[i]/1000000000.);
			sum2+=a[i];
		}
		return sum2;
	}
	public static void main(String[] args){
		long alpop=alPop();
		long apop=aPop();
		System.out.println();
		long alret=alRetrieve();
		long aret=aRetrieve();
		//Rerun the program a few times, the retrieval varies between the 2 structures, so this test is unconvincing in terms of performance
		System.out.printf("\nArrayList Populate sum: %11d\n[]        Populate sum: %11d\n",alpop,apop);
		System.out.println(alpop/(double)apop);
		System.out.printf("\nArrayList Retrieve sum: %11d\n[]        Retrieve sum: %11d\n",alret,aret);
		System.out.println(alret/(double)aret);
		/**TODO, i rewrote the code, so does this affect below???
		 * Profile it and check the memory differences
			Debug>Heap Dump
			Play through to the second method
			Heap Dump
			Compare the two(but don't click the compare button)
				1)Thread view, elementData 50MB, but Integers in object view shows as 100MB
				2)int[] 20MB in Object view and in Thread viwe,
		Conclusion, arrays win in terms of RAM, and put(). arrays/arraylists the same with get()
		*/		
	}
}