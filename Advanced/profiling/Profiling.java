package profiling;

import java.util.ArrayList;
import java.util.List;

public class Profiling{
/**
 * Open Task Manager -> CTRL+SHIFT+ESC -> Java process running VisualVM often needs killing
 * Download+Install https://visualvm.github.io/download.html
 * https://visualvm.github.io/idesupport.html
 * 	Download+Extract
 * 	Help>Install New Software>Add>Find your folder>Tick VisualVM
 * 	Window>Preferences>"Visual">configure JDK and VisVM location
 * 	Run>Debug Configuration>Multiple Launchers>VisualVM
 * 	Debug
 * 	VisualVM on left, Eclipse on right
 */
	public static void main(String[] args){
/**
 * 	Eclipse>Debug>Down Array>Java>Show system threads
 * 		3x RMI ???
 * 		JMX ???
 * 		Common-Cleaner ???
 * 		Attach Listener  ???
 * 		Signal Dispatcher  ???
 * 		Finalizer  ???
 * 		Reference Handler  ???
 * 	VisualVM>Local/Profiling.profiling
 * 		Monitor
 * 			GC activity->The little peaks show when it runs and scans for new garbage. There won't be any after a while as the program is paused.
 * 			Heap>Should stabilize around 15MB of heap. Why does it go up and down? see later(Profiling)
 * 				Click the Perform GC button x3(only first 2 do anything) 
 * 				PermGen>We have to statics, so its 0bytes
 * 			Classes>2092 - Java is complex
 * 			Threads>Should stabilize around 10 ???Why does it fluctuate????
 * 		Threads>4 of the threads are waiting to be interrupted with input
 * 		Sampler
 * 			Click Memory
 * 				Per thread allocations>explain then click the deltas button>so the RMI(the profiler) is causing the heap to fluctuate. The RMI memory grows forever, from 15MB to 39MB and up. So eventually you'll need to kill the task in task manager and restart. VisualVM is free, the other Profiling tools cost ~$400 per user.
 * 				Heap histogram>byte[] is the biggest user of memory ~17k objects, consuming 1MB but this grows and shrinks as the process runs.
 * 			Click CPU(here we see the baseline, then we will resume the program and see it change)
 * 				Thread CPU time
 * 				CPU Samples
 * 					Right click 'main'>Show>Only this thread
 * 					Left click 'main'>click the 'show hotspots button' * 		
 */
/** Eclipse>Resume.*/
		List<Integer> l=new ArrayList<>();
		for(int i=10;i<5000010;i++) { //If we create too many values, eclipse and VisualVM break.
			l.add(i);
		}
		System.out.println("Created 5M values");
/** 	VisualVM
 * 		Sampler>Thread CPU time, CPU Samples
 * 			ArrayList.add takes 10 times longer than Integer.valueOf
 * 		DIRTY DIRTY
 * 		Heap histogram>now there are 44M Integers, 
 * 				and 3M Object[] ???
 * 		Monitor>
 * 			Used Heap grow quickly, then stabilize and slowly things are GCed down to a reasonable level
 *				CPU/GC activity spike 
 * 		Restart the app, play to line 52, and then click the Heap Dump button(want to avoid it being >700GB of Profiling info), and wait for it to load
 * 			Choose Threads and drill into main
 * 				String[] is the argument to main, and is empty so the container is only 24B
 * 				ArrayList>
 * 					Note size of variable is 24B but fields>elementData>is 49.2MB
 * 					Look at the first 2 elements [0] and [1]>value
 * 					We created 5M elements, the size is 5M, but the elementData says there are 6.1M, if we look in eclipse in the variables view(make sure "show logical structure" is not selected). 6.1M is the size of the array, so it has 1.1M free spaces.
 * 					Each Integer is 20B despite ints being 4.
 * 			Choose Objects
 * 				  There are 5000167 Integers (presumably the APIs have 167 Integers) consuming 100MB
 * 					In the next example we will do some benchmarking and Profiling.
 */
	}

}


//TODO PermGen demo. HeapDump References demo
//Notes on pausing the thread //run for 3 seconds, Pause, F7(all the way back to main),F6(so the variables view works)