package benchmarking;

public class MethodCallCost{
	static long j=1;
	public static void StaticMethodEmpty() {
		
	}
	public static long StaticMethodTrivial() {
		return 42;
	}
	public static long StaticMethodNoArgs() {
		return j*=2;
	}
	
	public static long StaticMethodArgs(long x,int y) {
		return j=x*y;
	}
	
	public static void main(String[] args){
		int numIterations=500000000;
		long before,after,duration,i;
		//Empty loop
		before=System.nanoTime();
		for(i=0;i<numIterations;i++){
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.println(i+" "+duration/(double)numIterations);
		
		//Empty method
		before=System.nanoTime();
		for(i=0;i<numIterations;i++){
			MethodCallCost.StaticMethodEmpty();
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.println(i+" "+duration/(double)numIterations);
		
		//Trivial method
		before=System.nanoTime();
		for(i=0;i<numIterations;i++){
			MethodCallCost.StaticMethodTrivial();
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.println(i+" "+duration/(double)numIterations);
		
		//Noargs method
		before=System.nanoTime();
		for(i=0;i<numIterations;i++){
			MethodCallCost.StaticMethodNoArgs();
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.println(i+" "+duration/(double)numIterations);
		
		//args method
		before=System.nanoTime();
		for(i=0;i<numIterations;i++){
			MethodCallCost.StaticMethodArgs(i,2);
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.println(i+" "+duration/(double)numIterations);
		
		//no method
		before=System.nanoTime();
		for(i=0;i<numIterations;i++){
			j=2*i;
		}
		after=System.nanoTime();
		duration=after-before;
		System.out.println(i+" "+j+" "+duration/(double)numIterations);
		
		
		
		//System.out.printf("ArrayList Populate> nanos: %11d seconds:%11f\n",duration,duration/1000000000.);

	}

}
