package benchmarking;

import java.util.ArrayList;
import java.util.List;

public class ArrayListPresize{
	static long arrayList() {
		long before=System.nanoTime(); //don't want the print to interfere with the timings
		List<Integer> l=new ArrayList<>();
		for(int i=10;i<5000010;i++) { //If we create too many values, eclipse and VisualVM break.
			l.add(i);
		}
		long after=System.nanoTime(); //don't want the print to interfere with the timings
		System.out.println("Growth > nanos:"+(after-before)+" seconds:"+((after-before)/1000000000.));
		return after-before;
	}
	static long arrayListPresize() {
		long before=System.nanoTime(); //don't want the print to interfere with the timings
		List<Integer> l=new ArrayList<>(6200000);
		for(int i=10;i<5000010;i++) { //If we create too many values, eclipse and VisualVM break.
			l.add(i);
		}
		long after=System.nanoTime(); //don't want the print to interfere with the timings
		System.out.println("Presize> nanos:"+(after-before)+" seconds:"+((after-before)/1000000000.));
		return after-before;
	}
	
	public static void main(String[] args){
		long[]al=new long[] {arrayList(),
				arrayList(),
				arrayList()};
		long[]alp=new long[] {arrayListPresize(),
			arrayListPresize(),
			arrayListPresize()};
		double oneSecond=1000000000.;
		System.out.printf("AL  %11d\nALP %11d\nALP%%AL %11f\nAL Seconds %f\nSeconds saved %f",
				al[0]+al[1]+al[2],
				alp[0]+alp[1]+alp[2],
				(al[0]+al[1]+al[2])/(double)(alp[0]+alp[1]+alp[2]),
				(al[0]+al[1]+al[2])/oneSecond,
				((al[0]+al[1]+al[2])-(alp[0]+alp[1]+alp[2]))/oneSecond);
		/**Reality: this is stupid, we don't benchmark:
		 * 	short lived programs
		 * 	the start of a programs life. We wait till it has gone through 10000 or so iterations, or been running for a few minutes, and then benchmark. We are not interested in the first few messages processed normally*/
	}

}


//TODO PermGen demo. HeapDump References demo
//Notes on pausing the thread //run for 3 seconds, Pause, F7(all the way back to main),F6(so the variables view works)