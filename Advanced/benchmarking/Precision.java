package benchmarking;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.ArrayUtils;

public class Precision{

	public static void main(String[] args){
		long[]deltas=new long[10000];
		long before;
		long after;
		for(int i=0;i<deltas.length;++i) {
			before=System.nanoTime();
			after=System.nanoTime();
			deltas[i]=after-before;
		}
		
		HashMap<Long,Integer>m=new HashMap<>();
		Arrays.stream(deltas).forEach(e->{long bucket=50*(e/50);m.put(bucket,1+m.getOrDefault(bucket,0));});
		m.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(e->System.out.println(e.getKey()+" "+e.getValue()));
		
		System.out.printf("\nMin %d, Real Min %d, Max %d, Avg %d, ",
				Collections.min(Arrays.asList(ArrayUtils.toObject(deltas))),
				Arrays.stream(deltas).filter(x->x>0).min().getAsLong(),
				Arrays.stream(deltas).max().getAsLong(),
				(long)Arrays.stream(deltas).average().getAsDouble()
			);
	}

}
