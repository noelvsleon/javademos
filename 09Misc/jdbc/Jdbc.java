package jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class Jdbc {
	public static void main(String[] args) throws ClassNotFoundException {
		SQLException lastError=null;
		//TODO make the ip more dynamic so it works offsite and onsite or just try both!!!!
		try(Connection con=DriverManager.getConnection("jdbc:oracle:thin:@10.20.40.53:1521:oradb1","delegate","pass")) {
			queryDatabase(con);
		} catch (SQLException e) {
			lastError=e;
		}  
		try(Connection con=DriverManager.getConnection("jdbc:oracle:thin:@88.211.122.42:1521:oradb1","delegate","pass")) {
			queryDatabase(con);
		} catch (SQLException e) {
			if(lastError!=null) {
				lastError.printStackTrace();
				e.printStackTrace();
			}
		}  
	}
	static void queryDatabase(Connection con) throws SQLException {
		Statement stmt=con.createStatement();  //Typical Java/RDBMS clunky API
		ResultSet rs=stmt.executeQuery("select * from RefData.fix");
		String[] colNames=new String[rs.getMetaData().getColumnCount()];
		for(int i=1;i<=rs.getMetaData().getColumnCount();++i){
			System.out.println("Col Name:"+rs.getMetaData().getColumnName(i)+" Col Type:"+rs.getMetaData().getColumnTypeName(i));
			colNames[i-1]=rs.getMetaData().getColumnName(i);
		}
		System.out.println();
		System.out.println(String.join(",", colNames));
		System.out.println("------------------------");
		int id=1,tag=2,name=3,value=4,descr=5; //Dodgy
		while(rs.next()){
			System.out.println(rs.getInt(id)+","+rs.getInt(tag)+","+rs.getString(name)+","+rs.getString(value)+","+rs.getString(descr));
		}
	}
}
