package commonsConfiguration;
import java.io.File;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
public class main {
	public static void main(String[] args) {
		Configurations configs=new Configurations();
		try {
			Configuration config = configs.properties(new File("09Misc\\commonsConfiguration\\config.properties"));
			System.out.println(
					"application.name:"+config.getString("application.name")
				+"\napplication.version:"+config.getDouble("application.version")
				+"\napplication.title:"+config.getString("application.title")
				+"\nlocation:"+config.getString("location")
				+"\nprimary:"+config.getBoolean("primary")
				+"\ndataprovider:"+config.getString("dataprovider")
				+"\nstrategy:"+config.getString("strategy")
				+"\ndatabase.host:"+config.getString("database.host")
				+"\ndatabase.port:"+config.getInt("database.port")
				+"\ndatabase.servicename:"+config.getString("database.servicename")
				+"\ndatabase.user:"+config.getString("database.user")
				+"\ndatabase.password:"+config.getString("database.password")
				+"\ndatabase.connection_timeout:"+config.getInt("database.connection_timeout")
				+"\ndatabase.query_timeout:"+config.getInt("database.query_timeout",300000) //default
				+"\ndatabase.maxRetries:"+config.getShort("database.maxRetries"));
			for(String allowedClient:config.getList(String.class,"allowedClients")){
				System.out.println("allowedClient: "+allowedClient);
			}
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
}